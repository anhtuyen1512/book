# Rework

```
Khác biệt để bứt phá
Jason Fried & David Heinemeier Hansson
✏️ Nguyễn Anh Tuyến
```

[Giới thiệu](#giới-thiệu)

[Chương 1: Khởi đầu](#chương-1-khởi-đầu)

[Chương 2: Hạ bệ](#chương-2-hạ-bệ)

[Chương 3: Tiến lên](#chương-3-tiến-lên)

[Chương 4: Tiến triển](#chương-4-tiến-triển)

[Chương 5: Năng suất](#chương-5-năng-suất)

[Chương 6: Đối thủ cạnh tranh](#chương-6-đối-thủ-cạnh-tranh)

[Chương 7: Sự tiến triển](#chương-7-sự-tiến-triển)

[Chương 8: Chiêu thị](#chương-8-chiêu-thị)

[Chương 9: Thuê mướn](#chương-9-thuê-mướn)

[Chương 10: Kiểm soát thiệt hại](#chương-10-kiểm-soát-thiệt-hại)

[Chương 11: Văn hóa](#chương-11-văn-hóa)

[Chương 12: Kết thúc](#chương-12-kết-thúc)

Bạn đừng làm theo luật của người khác, đừng để "luật" của đối thủ chi phối. Hãy tạo ra "pháp luật" của mình để chiến thắng bất cứ khách hàng, đối thủ, sân chơi nào.

Jason Fried

Khác biệt để bứt phá thực sự là một cuốn sách kinh tế khác biệt khi đặt ra những vấn đề trong kinh doanh kiểu như "Ít hơn là tốt" hay "Họp hành là độc dược". Chính nhờ điều đó mà Khác biệt để bứt phá không chỉ là một cuốn sách dạy người đọc về những "mánh khoé" kinh doanh. Cuốn sách cô đọng, súc tích và dễ hiểu cho bất kỳ ai muốn định vị mình trong cuộc sống dù có muốn kinh doanh hay không.

Không đặt những vấn đề to tát như những tập đoàn kinh tế, cũng không đưa những nguyên tắc kinh tế khuôn mẫu, cứng nhắc, Khác biệt để bứt phá truyền cho người đọc cảm hứng để sống, để làm việc, để bứt khỏi những thứ nhàm chán xung quanh và tự hào về những gì mình đã làm được.

Hầu hết các sách kinh doanh đều cho bạn cùng một lời khuyên. Đó là viết ra bản kế hoạch, nghiên cứu thị trường, tìm kiếm nhà đầu tư, vân vân và vân vân. Nếu bạn đang tìm một quyển sách như thế, tôi khuyên bạn hãy đặt cuốn sách này trở lại kệ.

Khác biệt để bứt phá chỉ cho bạn một phương thức vô cùng dễ dàng để bạn gặt hái thành công. Bạn sẽ biết vì sao mình không cần đến nguồn vốn bên ngoài và vì sao bạn nên phớt lờ sự cạnh tranh. Sự thật là bạn cần ít hơn bạn nghĩ. Bạn không cần phải làm việc cật lực, không cần phải hoang phí thời gian cho công việc giấy tờ hay họp hội. Bạn thậm chí còn không cần đến cả văn phòng nữa. Những thứ đó tất thảy đều dư thừa.

Cái bạn cần là đừng nói nữa, mà hãy bắt tay vào việc. Cuốn sách Khác biệt để bứt phá sẽ chỉ cho bạn khối điều hay ho. Bạn sẽ học cách làm việc hiệu quả hơn mà không mất quá nhiều thời gian.

Thẳng thắng, rõ ràng và dễ đọc, Khác biệt để bứt phá là cuốn sách thực tiễn tuyệt vời dành cho những ai đã và đang mơ ước đến việc tạo dựng một thứ gì đó bằng chính sức mình.


# Giới thiệu

Quyển sách này không dựa trên lý thuyết học thuật mà dựa trên kinh nghiệm thực tế.

Chúng tôi hoạt động kinh doanh hơn mười năm nay. Trên suốt chặng đường ấy, chúng tôi chứng kiến hai cuộc khủng hoảng tài chính, một vụ nổ bong bóng bất động sản, các cuộc chuyển đổi mô hình kinh doanh, bao dự đoán u ám, và chúng tôi vẫn duy trì lợi nhuận qua tất cả mọi biến cố.

Chúng tôi là một công ty nhỏ, chuyên sản xuất phần mềm hỗ trợ các doanh nghiệp và tổ chức eo hẹp về tài chính để giúp họ quản lý công việc một cách dễ dàng. Hiện có hơn ba triệu khách hàng trên thế giới sử dụng các sản phẩm của chúng tôi.

Vào năm 1999, chúng tôi sáng lập công ty tư vấn thiết kế trang web gồm ba thành viên.

Năm 2004, không hài lòng với phần mềm quản lý dự án đang sử dụng, chúng tôi tự tạo ra phần mềm của chính mình: Basecamp. Khi chúng tôi giới thiệu Basecamp với các khách hàng và đồng nghiệp thì tất cả họ đều nói: "Chúng tôi cũng cần thứ này cho công việc kinh doanh". Năm năm sau, Basecamp đã tạo ra hàng triệu đô-la lợi nhuận mỗi năm.

Giờ đây, chúng tôi còn bán những công cụ mạng khác như phần mềm quản lý khách hàng Highrise, mạng nội bộ đồng thời là công cụ chia sẻ kiến thức Backpack, công cụ thảo luận nhóm Campfire. Chúng tôi cũng sáng chế và cung cấp mã nguồn mở cho một phần mềm lập trình được gọi làRuby on Rails, hỗ trợ rất nhiều cho thế giới web 2.0.
Tuy nhiên, chúng tôi không phải là "gã khổng lồ" kinh doanh trong lĩnh vực Internet, mà chỉ là một tập thể nhỏ (vỏn vẹn mười sáu người khi quyển sách này được ấn hành), chi tiêu dè sẻn và có lợi nhuận.

Rất nhiều người bảo chúng tôi không thể làm được những điều mà chúng tôi đang làm. Họ gọi chúng tôi là kẻ "đắc thời đắc thế". Họ bảo những người khác hãy phớt lờ lời khuyên của chúng tôi. Một số người thậm chí còn gọi chúng tôi là những kẻ vô trách nhiệm, khinh suất, hấp tấp và xem chúng tôi là những kẻ nghiệp dư.

Họ không hiểu làm thế nào một công ty có thể loại bỏ việc bành trướng quy mô, các cuộc họp, nguồn ngân sách, ban giám đốc, việc quảng cáo, các nhân viên bán hàng... mà vẫn phát đạt. Đó là vấn đề của họ, không phải của chúng tôi. Họ không nghĩ bạn lại có đội ngũ nhân viên trải rộng khắp tám thành phố ở hai châu lục và hầu như chẳng bao giờ gặp nhau. Họ bảo bạn không thể thành công mà không có các dự án tài chính và các kế hoạch năm năm. Họ đã sai.

Họ bảo bạn cần phải có một công ty PR để xuất hiện trên các tờ Times, Business Week, Inc., Fast Company, New York Times, Financial Times, Chicago Tribune, Atlantic, Entrepreneur và Wired. Họ đã sai. Họ bảo bạn không thể chia sẻ bí quyết và tiết lộ các bí mật của mình mà vẫn trụ vững trước các đối thủ cạnh tranh. Họ lại sai nốt.

Họ bảo bạn không thể nào cạnh tranh với những đối thủ đáng gờm mà không cần đến nguồn ngân sách dồi dào cho việc quảng cáo và marketing. Họ bảo bạn không thể thành công nếu bạn tạo ra các sản phẩm ít tính năng hơn của đối thủ cạnh tranh. Họ bảo bạn không thể gầy dựng nên tất cả ngay khi vừa bắt đầu. Nhưng đó lại chính là những gì chúng tôi đã thực hiện.

Họ nói rất nhiều điều. Chúng tôi bảo họ sai. Chúng tôi đã chứng minh điều đó. Và chúng tôi viết quyển sách này để chỉ cho bạn cách chứng minh như thế nào.

Trước tiên, chúng tôi sẽ bắt đầu bằng việc mổ xẻ công việc kinh doanh. Chúng tôi sẽ phân tích và giải thích tại sao đã đến lúc vứt bỏ những khái niệm truyền thống về các điều kiện cần thiết để điều hành một doanh nghiệp. Tiếp đó, chúng tôi sẽ chỉ cho bạn cách điều hành công việc kinh doanh ấy theo lối tư duy mới. Bạn sẽ học được cách bắt đầu, hiểu được tại sao bạn lại cần ít hơn là bạn nghĩ, khi nào thì khai trương, làm sao để nói lên điều bạn muốn, nên thuê mướn ai, khi nào và làm sao để kiểm soát tất cả.
Giờ thì chúng ta hãy bắt tay vào việc.


# Chương 1: Khởi đầu

## Thực tế mới

Đây là một quyển sách kinh doanh khác biệt dành cho những người khác biệt, từ những người chưa bao giờ mơ đến việc mở công ty cho đến những người đã gầy dựng và điều hành doanh nghiệp của mình một cách thành công.

Quyển sách này dành cho những doanh nhân kỳ cựu, những người dám nghĩ dám làm của giới kinh doanh; những người biết mình sinh ra để đi tiên phong, lãnh đạo và chinh phục.

Quyển sách cũng dành cho những chủ doanh nghiệp nhỏ, những người xem công việc kinh doanh là niềm vui sống và những người đang tìm kiếm công cụ giúp họ tốn ít công sức hơn nhưng mang lại hiệu quả hơn.

Và quyển sách này cũng dành cho những người bị mắc kẹt trong công việc hàng ngày, những người luôn ước ao được làm việc của chính mình. Có lẽ họ thích những gì mình làm, nhưng họ không thích sếp hay đồng nghiệp; hoặc có lẽ họ quá chán ngán, muốn được làm công việc mà mình đam mê và được trả lương cho công việc đó.

Cuối cùng, quyển sách này dành cho tất cả những ai chưa từng nghĩ đến việc lập nghiệp. Có lẽ họ nghĩ mình sinh ra không phải để làm việc đó. Có lẽ họ nghĩ họ không có thời gian, tiền bạc và niềm tin để hình dung ra nó. Cũng có thể họ chỉ sợ mạo hiểm. Hoặc có lẽ họ cho rằng kinh doanh là một thế giới "bẩn". Bất luận lý do là gì thì quyển sách này cũng vẫn dành cho họ.

Có một thực tế mới: ngày nay, bất kỳ ai cũng có thể kinh doanh. Những công cụ trước đây nằm ngoài tầm tay thì giờ rất dễ có được. Công nghệ từng tiêu tốn hàng ngàn đô-la thì giờ chỉ vài đô-la hoặc thậm chí là miễn phí. Một người có thể đảm đương công việc của hai hay ba người, thậm chí, trong một vài trường hợp, còn có thể đảm đương công việc của cả phòng. Những việc bất khả hồi vài năm trước thì giờ trở nên thật đơn giản.

Bạn không cần phải đau khổ làm việc đến 60, 80, hay 100 giờ mỗi tuần để công việc diễn ra suôn sẻ và có hiệu quả. Chỉ 10 đến 40 giờ mỗi tuần đã là quá nhiều. Bạn không cần phải rút hết tiền tiết kiệm hay gồng gánh nhiều rủi ro. Bạn thậm chí chẳng cần có một văn phòng riêng. Giờ đây, bạn có thể làm việc ở nhà hoặc hợp tác với những người sống cách xa hàng ngàn dặm mà bạn chưa bao giờ gặp mặt.

Đã đến lúc thay đổi phương thức làm việc. Chúng ta hãy bắt đầu.


# Chương 2: Hạ bệ

## Phớt lờ thế giới thực

Những lúc hào hứng nói với mọi người về một ý tưởng mới, bạn thường nghe mọi người bảo: "Chẳng làm được đâu".
Thế giới thực có vẻ là một nơi cực kỳ buồn chán. Đó là nơi mà các ý tưởng mới, phương pháp lạ và những khái niệm ngoại lai luôn thất thế. Chỉ có những gì người ta đã biết và từng làm mới được chào đón và đánh giá cao, cho dù những điều đó có thiếu sót hoặc kém hiệu quả.

Hãy nhìn sâu hơn vào vấn đề và bạn sẽ phát hiện ra các cư dân của "thế giới thực" này đầy bi quan và tuyệt vọng. Họ mong chờ những khái niệm mới sẽ thất bại. Họ cho rằng xã hội chưa sẵn sàng hoặc không có khả năng thay đổi.
Tệ hại hơn, họ muốn kéo kẻ khác cùng xuống mồ. Nếu bạn tràn trề hy vọng và đầy chí cầu tiến, họ sẽ cố thuyết phục để bạn tin rằng các ý tưởng của mình là bất khả thi. Họ sẽ bảo bạn đang lãng phí thời giờ.

Đừng tin họ. Thế giới này có thể là thật đối với họ, nhưng điều đó không có nghĩa là bạn phải sống trong đó.
Chúng tôi biết vậy vì công ty của chúng tôi không thể đáp ứng được đòi hỏi của thế giới thực này theo bất cứ cách nào. Trong thế giới thực, bạn không thể có hơn một tá nhân viên trải rộng trên tám thành phố ở hai châu lục. Trong thế giới thực, bạn không thể thu hút hàng triệu khách hàng mà không cần bất cứ nhân viên bán hàng hay chiến dịch quảng cáo. Trong thế giới thực, bạn không thể để lộ bí quyết thành công của mình cho người khác biết. Tuy nhiên, chúng tôi đã làm tất cả những điều đó và vẫn phát đạt.

Thế giới thực không phải là một nơi chốn, mà là một lời biện hộ. Nó là một lời bào chữa để không cần phải nỗ lực vì bất cứ điều gì. Thế giới thực đó chẳng có liên quan gì đến bạn.

## Đừng tung hô việc học hỏi từ sai lầm

Trong thế giới kinh doanh, thất bại đã trở thành một nghi thức bất di bất dịch. Bạn nghe ra rả bên tai cứ mười doanh nghiệp thì có đến chín là thất bại. Bạn nghe các cơ hội kinh doanh của bạn quá mong manh. Bạn nghe thất bại tạo nên nghị lực. Người ta còn khuyên bạn: "Thất bại là mẹ thành công".

Với quá nhiều thất bại xung quanh mình, bạn không thể nào không sống cùng nó. Nhưng đừng để nó ăn sâu vào người bạn. Đừng để mình bị các số liệu thống kê đánh lừa. Người khác thất bại thì đó là thất bại của họ, không phải của bạn.

Nếu người khác không thể đưa được sản phẩm của họ ra thị trường thì nào có liên quan gì đến bạn. Nếu người khác không thể xây dựng một đội ngũ nhân viên thì cũng chẳng can hệ chi đến bạn. Nếu người khác không thể định giá đúng dịch vụ của họ thì điều đó cũng chẳng mảy may ảnh hưởng đến bạn. Nếu người khác chi tiêu vượt mức thu nhập của họ thì... xem nào, bạn biết chuyện gì sẽ xảy ra rồi chứ?

Một nhận thức sai lầm khác: bạn cần phải học hỏi từ thất bại của mình. Bạn thật sự học được gì từ thất bại? Đó là bạn có thể biết những điềukhông nên lặp lại, nhưng liệu điều đó có ích gì khi bạn vẫn không biết bạn nên làm gì vào lần sau?

Hãy làm ngược lại bằng cách học hỏi từ thành công của mình. Thành công cho bạn "đạn dược" thực sự. Khi thành công trong một việc nào đó, bạn biết được điều gì hiệu quả và bạn có thể tiếp tục thực hiện việc đó. Và lần sau, bạn sẽ có thể làm tốt hơn thế.

Thất bại không phải là điều kiện tiên quyết để thành công. Một nghiên cứu của Khoa Kinh tế, Đại học Harvard phát hiện ra các doanh nhân thành công có nhiều cơ hội tiếp tục thành công hơn (tỷ lệ thành công trong tương lai của họ là 34%). Còn những doanh nhân mà công ty của họ thất bại ngay từ ban đầu gần như có cùng một tỷ lệ thành công với những người mới khởi nghiệp: chỉ 23%). Như vậy, cơ hội thành công của những người thất bại ngang bằng với những ai mới bước chân vào con đường kinh doanh. Thành công mới chính là kinh nghiệm thực sự đáng giá.

Điều này có gì bất ngờ đâu. Đó chính là quy luật tự nhiên. Sự tiến hóa không nấn ná ở những thất bại trong quá khứ; nó luôn được xây dựng dựa trên những gì hiệu quả. Bạn cũng nên như thế.

## Lên kế hoạch là phán đoán

Trừ phi bạn là thầy bói, còn không thì việc lên kế hoạch kinh doanh dài hạn chỉ là một hoạt động tưởng tượng. Có quá nhiều yếu tố nằm ngoài tầm kiểm soát của bạn: tình hình thị trường, đối thủ cạnh tranh, khách hàng, nền kinh tế... Việc lên kế hoạch khiến bạn cảm thấy mình có thể kiểm soát được những thứ mà thực ra bạn không thể.

Tại sao chúng ta không gọi việc lên kế hoạch bằng cái tên đúng của nó: phán đoán. Hãy xem các kế hoạch kinh doanh của bạn là những phán đoán kinh doanh, kế hoạch tài chính của bạn là những phán đoán tài chính và kế hoạch chiến lược chính là những phán đoán chiến lược. Giờ thì bạn có thể thôi lo lắng quá nhiều về những kế hoạch nữa. Chúng chẳng đáng để bạn phải quá căng thẳng đâu.

Khi bạn biến các phán đoán thành kế hoạch, bạn tiến vào vùng nguy hiểm. Kế hoạch để cho quá khứ điều khiển tương lai. Chúng che mắt bạn. "Đây là hướng mà chúng ta sẽ đi bởi vì... xem nào, bởi vì đây là kế hoạch của chúng ta." Và đó chính là vấn đề: các kế hoạch mâu thuẫn với sự ứng biến.

Bạn phải có khả năng ứng biến. Bạn phải có khả năng nắm bắt khi cơ hội đến. Đôi khi bạn cần phải nói: "Chúng ta phải đi theo hướng mới vì điều đó phù hợp với tình hình hiện tại."

Việc tính toán thời gian của những kế hoạch dài hạn cũng hỏng bét. Bạn có được thông tin nhiều nhất khi bạn đang làm một việc gì đó, chứ không phải là trước khi bạn thực hiện. Thế nhưng bạn lên kế hoạch vào lúc nào? Thường là trước khi bạn bắt tay vào việc. Đó là thời điểm tồi tệ nhất để đưa ra những quyết định then chốt.

Ở đây chúng tôi không có ý khuyên bạn đừng nên nghĩ về tương lai hoặc suy tính phương thức dẹp bỏ các chướng ngại. Đó là những việc rất đáng làm. Chỉ có điều, bạn không cần phải viết nó ra hoặc để nó ám ảnh bạn. Nếu viết một kế hoạch vĩ đại, bạn sẽ gần như chẳng bao giờ nhìn đến nó. Những bản kế hoạch dài mấy trang giấy thường sẽ chỉ nằm im như hóa thạch trong ngăn hồ sơ.

Hãy bỏ bớt việc phán đoán. Hãy quyết định những gì bạn sẽ làm trong một tuần, chứ không phải một năm. Hãy tìm ra điều quan trọng nhất tiếp theo và thực hiện. Hãy ra quyết định ngay trước khi bạn làm một điều gì đó, chứ đừng ra quyết định quá sớm.

Làm việc không cần lên kế hoạch cũng tốt thôi. Chỉ cần lên máy bay và cất cánh. Bạn có thể mua một chiếc áo sơ mi đẹp, kem cạo râu và bàn chải đánh răng khi bạn tới nơi.

Làm việc không có kế hoạch có thể hơi đáng sợ. Nhưng mù quáng làm theo một kế hoạch chẳng có gì dính dáng đến thực tế thì còn kinh khủng hơn.


## Sao phải bành trướng quy mô?

Người ta hỏi bạn: "Công ty của anh lớn cỡ nào?". Đó chỉ là một câu hỏi nhỏ trong cuộc trò chuyện thân mật, nhưng họ lại không tìm kiếm câu trả lời nhỏ. Nếu con số càng lớn, bạn càng có vẻ quyền lực, chuyên nghiệp và ấn tượng. "Ôi, tuyệt!" - Họ sẽ thốt lên thế nếu bạn có trên một trăm nhân viên. Còn không, bạn sẽ nhận được câu: "Ồ... ổn thôi". Câu trước là một lời khen, còn câu sau chỉ vì lịch sự.

Tại sao lại như thế? Gia tăng quy mô và việc kinh doanh có liên hệ gì với nhau? Tại sao mở rộng luôn là mục tiêu? Sức lôi cuốn của doanh nghiệp lớn so với doanh nghiệp nhỏ là gì? Việc tìm được quy mô thích hợp và duy trì như thế thì có gì không ổn? Bạn sẽ cần một câu trả lời hay hơn, khác hơn so với "hiệu quả kinh tế quy mô lớn" đã ăn sâu vào đầu.

Liệu chúng ta có nhìn vào Harvard hay Oxford và bảo: "Nếu hai ngôi trường này mở rộng thêm nhiều chi nhánh và thuê thêm hàng ngàn giáo sư, vươn ra toàn cầu và mở trường ở khắp nơi trên thế giới... thì sẽ trở thành những ngôi trường vĩ đại"? Đương nhiên là không. Đó không phải là cách mà chúng ta đo lường giá trị của những tổ chức này. Thế thì tại sao chúng ta lại đo lường các doanh nghiệp bằng cách đó?

Có thể quy mô phù hợp cho công ty của bạn là năm người. Có thể là bốn mươi. Có thể là hai trăm. Hoặc cũng có thể chỉ cần bạn với chiếc máy tính xách tay là đủ. Đừng đưa ra giả định về quy mô doanh nghiệp của bạn. Hãy phát triển từ từ và xem cái gì là phù hợp. Việc thuê mướn nhân sự trước chính là nguyên nhân dẫn đến cái chết yểu của nhiều công ty. Và hãy tránh sự phát triển bộc phát. Việc đó có thể khiến bạn bỏ qua quy mô thích hợp của mình.

Quy mô nhỏ không phải là một chiếc bàn đạp. Quy mô nhỏ là một đích đến vĩ đại trong chính bản thân nó.

Bạn có bao giờ nhận thấy trong khi các doanh nghiệp nhỏ mong muốn trở nên lớn hơn thì các doanh nghiệp lớn lại ước gì họ nhỏ và linh động hơn? Và hãy nhớ, một khi bạn đã lớn lên thì việc thu gọn lại mà không cần phải sa thải nhân viên, làm tổn hại nhuệ khí và thay đổi hoàn toàn phương thức kinh doanh là cực kỳ khó khăn.

Bạn không cần phải lúc nào cũng nhắm đến việc mở rộng quy mô. Quy mô ở đây không chỉ là số lượng nhân viên mà còn là chi phí, tiền thuê trụ sở, hạ tầng công nghệ thông tin, nội thất... Việc mở rộng quy mô không tự nhiên xảy đến với bạn. Bạn chính là người quyết định. Nếu bạn muốn, tức là bạn chấp nhận thêm những chuyện gây đau đầu. Bạn sẽ gánh lấy nhiều chi phí và ép mình phải gầy dựng một doanh nghiệp cồng kềnh, một doanh nghiệp mà việc điều hành sẽ khiến bạn khó khăn và căng thẳng hơn rất nhiều.

Đừng cảm thấy bất an khi điều hành một doanh nghiệp nhỏ. Bất kỳ ai điều hành một doanh nghiệp có thể đứng vững và có thể sinh lợi thì dù nó nhỏ hay lớn, người tạo ra nó cũng nên lấy làm tự hào.

## Chứng nghiện việc

Chúng ta có khuynh hướng tán dương những người tham công tiếc việc, trầm trồ thán phục những người làm việc thâu đêm và ngủ lại văn phòng. Hình ảnh đó được xem là biểu hiện của vinh dự xả thân vì sự nghiệp. Đối với những người này, dường như công việc không bao giờ có giới hạn.

Tuy nhiên, chứng nghiện việc này chẳng những không cần thiết mà còn thật xuẩn ngốc. Làm việc nhiều hơn không có nghĩa là bạn quan tâm và hoàn tất được nhiều việc hơn mà nó chỉ đơn thuần là bạn làm việc nhiều hơn thôi.

Những người nghiện việc rốt cục sẽ tạo ra nhiều vấn đề hơn là những việc họ thực sự giải quyết được bởi cách làm việc như thế sẽ không thể tồn tại lâu qua thời gian, và đến khi kiệt sức (điều này chắc chắn sẽ xảy ra) thì những tổn hại mà họ phải nhận lãnh là rất lớn.

Những người tham công tiếc việc cũng không nắm được cốt lõi vấn đề. Họ cố gắng giải quyết rắc rối chỉ toàn bằng cách bỏ thêm thật nhiều thời gian. Họ cố gắng bù đắp cho sự lười biếng tư duy bằng lao động thể lực. Việc này sẽ đưa đến những giải pháp thiếu thông minh và khó ứng dụng.

Họ thậm chí còn tạo ra những cơn khủng hoảng. Họ không tìm cách để làm việc hiệu quả hơn bởi vì họ thực sự thích làm việc ngoài giờ. Họ tận hưởng cảm giác thấy mình là những vị anh hùng. Họ tạo ra rắc rối (thường là một cách vô thức) để có thể làm việc nhiều hơn.

Người tham công tiếc việc khiến cho những người không ở lại làm việc muộn cảm thấy mình không xứng đáng vì "chỉ" làm việc đủ giờ. Điều đó dẫn đến cảm giác tội lỗi và sụt giảm tinh thần ở mọi người. Thêm vào đó, nó dẫn đến tâm lý "dán chặt mông vào ghế" - mọi người ở lại muộn, thậm chí không biết để làm gì và năng suất công việc cũng chẳng cải thiện bao nhiêu.

Nếu trí não luôn phải làm việc, bạn sẽ khó mà có óc phán xét sáng suốt. Cách định giá trị và ra quyết định của bạn sẽ bị lệch lạc. Bạn không còn có khả năng quyết định việc gì là đáng cố gắng, việc gì không. Và rồi sau cùng, bạn chỉ chuốc lấy sự chán chường và mệt mỏi.

Chẳng ai có thể đưa ra những quyết định nhạy bén khi tinh thần đang xuống dốc cả.

Rốt cục, người tham công tiếc việc không thực sự gặt hái được nhiều hơn người làm việc bình thường. Họ có thể tuyên bố mình là người hoàn hảo, nhưng thật ra họ đang lãng phí thời gian chúi mũi vào những tiểu tiết vụn vặt thay vì chuyển sang công việc tiếp theo.

Người tham công tiếc việc không phải là anh hùng. Họ không tiết kiệm được thời gian; họ chỉ sử dụng thời gian cho kỳ hết. Vị anh hùng thực thụ đã về nhà từ lâu bởi anh ta tìm ra cách làm việc nhanh lẹ hơn.

## Đã quá đủ với từ "ông chủ"

Hãy quên đi thuật ngữ ông chủ [[1]](#1). Từ này đã lỗi thời và quá nặng nề, nghe cứ như một câu lạc bộ chỉ mở cửa cho những ai có thẻ thành viên.

Có một nhóm người mới ở ngoài kia đang gầy dựng cơ nghiệp. Họ tạo ra lợi nhuận, nhưng lại không bao giờ đặt mình lên trên. Nhiều người trong số họ thậm chí còn không nghĩ mình là những chủ doanh nghiệp. Họ chỉ đang làm những gì họ yêu thích theo cách riêng của mình và nhận được thù lao từ việc ấy.

Vì vậy, hãy thay thế cái từ "bề trên" ấy bằng một từ thực tế hơn. Thay vì ông chủ, hãy chỉ gọi họ là những người khởi nghiệp. Bất cứ ai vừa bắt đầu kinh doanh đều là người khởi nghiệp. Bạn không cần phải có bằng MBA (Master of Business Administration - Thạc sĩ Quản trị Kinh doanh), chứng chỉ hành nghề, bộ com-lê thật oách, chiếc cặp táp bóng bẩy, hay chút máu mạo hiểm trong người. Bạn chỉ cần có ý tưởng, sự tự tin và động lực để bước lên bệ phóng.


# Chương 3: TIẾN LÊN

## Tạo dấu ấn

Để làm việc lớn, bạn cần phải thấy mình đang tạo ra sự khác biệt, tạo ra một dấu ấn có ý nghĩa và thuộc về một điều gì đó rất quan trọng.

Điều này không có nghĩa là bạn phải tìm ra phương pháp chữa bệnh ung thư mà chỉ cần bạn cảm thấy nỗ lực của mình có giá trị. Chỉ cần khách hàng của bạn nói: "Cái này làm cho cuộc sống của tôi tốt đẹp hơn", hay chỉ cần nếu bạn ngưng những gì mình đang làm thì mọi người sẽ nhận ra ngay.

Bạn cũng nên biết rằng đây là việc khẩn cấp, vì bạn không có nhiều thời gian, và đây là công việc của đời bạn. Liệu bạn muốn tạo ra một sản phẩm hệt như những thứ sẵn có, hay bạn muốn tạo ra sự khác biệt? Những gì bạn làm chính là gia tài của bạn. Đừng chỉ ngồi yên một chỗ và chờ đợi người khác tạo ra sự thay đổi mà bạn muốn. Và cũng đừng nghĩ rằng cần phải có một đội ngũ nhân viên khổng lồ để tạo ra sự khác biệt ấy.

Craigslist [[2]](#2) là một ví dụ điển hình. Đây chính là công ty đã đánh đổ ngành kinh doanh rao vặt truyền thống. Chỉ với vài chục nhân viên, công ty đã tạo ra hàng chục triệu đô-la doanh thu và lũng đoạn toàn bộ ngành báo chí.

Drudge Report do Matt Drudge điều hành, chỉ đơn thuần là một trang web do một người điều hành, nhưng nó lại có tác động to lớn đối với ngành công nghiệp tin tức - những người sản xuất chương trình truyền hình, những người dẫn chương trình đối thoại trên radio và các phóng viên báo chí thường xem Drudge Report là một nơi để tìm kiếm các tin sốt dẻo.

Nếu bạn dự định làm việc gì, hãy làm những việc thực sự ra trò. Không ai biết những anh chàng bé nhỏ sẽ đột ngột xuất hiện lúc nào để tiêu diệt các mô hình cũ đã tồn tại hàng thập kỷ. Bạn hoàn toàn có thể làm điều tương tự trong lĩnh vực của mình.


## Gãi đúng chỗ ngứa

Cách dễ dàng nhất và nhanh chóng nhất để có được một sản phẩm hay dịch vụ tuyệt vời là làm ra những thứ mà bản thân bạn cũng muốn sử dụng. Điều đó sẽ giúp bạn thiết kế những gì bạn muốn và bạn sẽ ngay lập tức biết được liệu những gì bạn đang làm có ích hay không.

Tại 37signals, chúng tôi tạo ra những sản phẩm mà chúng tôi cần để điều hành công việc của chính mình. Ví dụ, chúng tôi muốn có một phương pháp lưu trữ thông tin những người mà chúng tôi tiếp xúc, những gì chúng tôi nói với họ và khi nào chúng tôi cần phải triển khai bước tiếp theo. Thế là phần mềm quản lý khách hàng Highrise ra đời. Không cần phải có các nhóm thăm dò ý kiến, các nghiên cứu thị trường hay người môi giới. Chỉ đơn giản là chúng tôi ngứa chỗ nào, chúng tôi gãi chỗ đó.

Khi xây dựng một sản phẩm hay dịch vụ, bạn phải đưa ra hàng trăm quyết định nhỏ mỗi ngày. Nếu bạn giải quyết vấn đề của người khác, tức là bạn không ngừng cố gắng nhắm trúng mục tiêu trong bóng tối. Khi bạn giải quyết vấn đề của chính mình, ánh sáng chan hòa khắp nơi. Bạn biết chính xác câu trả lời đúng là gì.

Nhà phát minh người Anh James Dyson đã "gãi đúng chỗ ngứa" của mình. Trong khi hút bụi ngôi nhà, ông nhận ra chiếc máy hút bụi dạng túi liên tục bị mất lực hút, bụi bẩn cứ bám vào những lỗ nhỏ trên chiếc túi và làm nghẽn đường lưu chuyển không khí. Đó không phải là vấn đề trong tưởng tượng của một ai khác mà là vấn đề thực sự ông trực tiếp trải nghiệm. James Dyson bèn giải quyết vấn đề bằng cách tạo ra chiếc máy hút bụi không túi có lực hút xoáy đầu tiên.

Vic Firth đã nảy ra ý tưởng chế tạo những chiếc dùi trống tốt hơn trong khi đang chơi bộ trống một mặt cho dàn nhạc giao hưởng Boston. Những chiếc dùi anh mua không đáp ứng được yêu cầu, vì vậy anh bắt đầu tự chế tạo những chiếc dùi trống trong tầng hầm ở nhà mình. Rồi một ngày nọ, anh đánh rơi một bó dùi xuống sàn và nghe được tất cả những âm thanh trầm bổng khác nhau. Đó là lúc anh bắt đầu kết hợp những chiếc dùi dựa trên trữ lượng hơi ẩm, cân nặng, độ dày và cao độ âm để chúng tạo thành những cặp dùi đồng nhất. Kết quả này đã trở thành khẩu hiệu cho sản phẩm của anh: "Cặp đôi hoàn hảo". Ngày nay, nhà máy của Vic Firth cho ra đời hơn 85.000 dùi trống mỗi ngày và chiếm 62% thị phần trên thị trường dùi trống ở Mỹ.

Huấn luyện viên điền kinh Bill Bowerman cho rằng đội tuyển của ông cần có những đôi giày chạy bộ nhẹ hơn, tốt hơn. Thế là ông đến xưởng và đổ cao su vào khuôn bánh quế của gia đình. Đó là cách mà đế giày waffle (bánh quế) nổi tiếng của Nike đã ra đời.

Những người này đã gãi chỗ ngứa của họ và mở ra một thị trường khổng lồ những người có cùng nhu cầu với mình.

Khi bạn tạo ra những gì bạn cần, bạn có thể ấn định chất lượng sản phẩm một cách nhanh chóng và trực tiếp, thay vì phải ủy quyền cho người khác.

Mary Kay Wagner, nhà sáng lập công ty mỹ phẩm Mary Kay, biết rằng các sản phẩm chăm sóc da của bà rất tuyệt bởi vì chính bà cũng sử dụng chúng. Lúc đầu, bà mua sản phẩm từ một bác sĩ thẩm mỹ ở địa phương. Khi vị bác sĩ ấy qua đời, Wagner đã mua lại các công thức làm đẹp từ gia đình của vị bác sĩ này. Bà không cần phải có các nhóm thăm dò ý kiến hay thực hiện các cuộc nghiên cứu để biết được những sản phẩm ấy có chất lượng hay không. Bà chỉ cần nhìn vào làn da của chính mình mà thôi.

Quan trọng hơn cả, phương pháp "giải quyết vấn đề của chính bạn" còn làm bạn yêu thích những gì mình tạo ra. Bạn biết rõ vấn đề là gì và hiểu giá trị của những giải pháp cho vấn đề đó một cách tường tận. Chẳng có gì thay thế được điều đó. Sau hết, bạn sẽ song hành cùng nó trong những năm sắp tới. Có lẽ là cả phần đời còn lại của bạn cũng nên. Làm một việc gì mà bạn thật sự quan tâm và yêu thích thì bao giờ cũng tốt hơn.


## Bắt đầu ngay thôi!

Bạn có bao giờ nghe bạn bè mình nói: "Tôi từng có ý tưởng về eBay. Giá như tôi mà tiến hành thì giờ tôi thành tỷ phú rồi!" hoặc những câu tương tự như thế? Kiểu lập luận ấy mới thảm hại và hoang đường làm sao. Có ý tưởng về eBay chẳng có liên quan gì đến việc thực sự tạo ra eBay cả. Những gì bạn làm mới thực sự quan trọng, chứ không phải những gì bạn nghĩ, nói, hoặc lên kế hoạch.

Bạn cho rằng ý tưởng của mình rất đáng giá? Thế thì hãy thử đem rao bán và xem liệu bạn sẽ nhận lại được gì. Không nhiều lắm có lẽ là câu trả lời mà bạn có được. Chừng nào bạn chưa thực sự bắt đầu tạo ra một cái gì đó, thì ý tưởng tuyệt vời của bạn cũng chỉ là ý tưởng mà thôi. Và bất cứ ai cũng có vài ý tưởng như vậy.

Vị đạo diễn người Mỹ lừng danh Stanley Kubrick từng khuyên những nhà làm phim đầy tham vọng: "Hãy cầm lấy máy quay, rồi quay một bộ phim bất cứ thể loại nào". Kubrick biết rằng khi bạn mới vào nghề, bạn cần phải bắt đầu sáng tạo. Điều quan trọng nhất là phải bắt tay vào làm.

Bạn có thể có rất nhiều ý tưởng, và chúng chẳng tốn kém gì cả. Ngay cả ý tưởng độc đáo nhất cũng chỉ đóng một vai trò rất nhỏ trong việc kinh doanh đến nỗi nó gần như không được tính đến. Vấn đề thực sự ở đây là bạn thực thi ý tưởng tốt đến mức nào.

## Đừng đổ lỗi cho thời gian

Lời biện hộ phổ biến nhất mà mọi người thường đưa ra là: "Không đủ thời gian". Họ tuyên bố họ thích mở công ty, học cách chơi một loại nhạc cụ, đưa ra thị trường một sản phẩm mới, viết sách, hay bất cứ việc gì... nhưng cuối cùng họ không thực hiện vì không có thời gian.

Thôi nào. Bạn sẽ luôn có đủ thời gian nếu bạn biết sử dụng thời gian đúng cách. Và cũng đừng nghĩ rằng bạn phải bỏ công việc hiện tại thì mới thực hiện được những dự định của mình. Hãy cứ làm việc bình thường và tranh thủ thực hiện kế hoạch của mình vào buổi tối.

Thay vì xem ti-vi hoặc chơi game, hãy dành thời gian cho ý tưởng của bạn. Thay vì đi ngủ lúc mười giờ, hãy đi ngủ lúc mười một giờ. Chúng tôi không khuyên bạn thức trắng đêm hay làm việc đến mười sáu giờ mỗi ngày. Điều chúng tôi muốn nói là hãy cố gắng dành ra vài giờ đồng hồ mỗi tuần để làm việc bạn yêu thích. Bao nhiêu đó thời gian cũng là tốt lắm rồi.

Một khi đã trải nghiệm, bạn sẽ nhận ra mối quan tâm của mình là đam mê thực thụ hay chỉ là ý thích nhất thời. Nếu bạn thấy những gì mình làm không hiệu quả, bạn chỉ cần trở lại với công việc hàng ngày của mình như trước đây. Bạn không mạo hiểm, cũng chẳng mất mát gì, chỉ là tốn một ít thời gian, chẳng có gì to tát cả.

Khi bạn thực sự đam mê một điều gì, bạn sẽ dành thời gian cho nó, bất chấp những nghĩa vụ khác của bạn. Sự thật là hầu hết mọi người không có đủ niềm đam mê đó. Thế là họ bảo vệ cái tôi của mình bằng cách đổ lỗi cho thời gian. Đừng nuông chiều bản thân bằng những lời biện hộ. Việc biến ước mơ của bạn thành hiện thực hoàn toàn là trách nhiệm của bạn.

Bên cạnh đó, thời điểm hoàn hảo không bao giờ đến cả, bởi bạn luôn luôn đưa ra bao nhiêu lý do: quá trẻ, quá già, quá bận rộn, quá túng thiếu, hoặc quá gì đấy. Nếu bạn cứ tiếp tục gặm nhấm việc hoạch định thời gian hoàn hảo cho mọi việc thì những việc đó sẽ chẳng bao giờ xảy ra đâu.

## Giữ vững lập trường

Khi đang tiến hành công việc, hãy luôn ghi nhớ tại sao bạn lại làm những việc đó. Các doanh nghiệp thành công luôn có quan điểm riêng, chứ không đơn thuần là tạo ra sản phẩm hay dịch vụ. Bạn phải có cái để mà tin tưởng. Bạn phải có chiếc cột sống vững chắc để tựa vào. Bạn cần phải biết mình sẵn sàng đấu tranh cho cái gì. Và rồi bạn cần phải thể hiện cho thế giới thấy.

Lập trường vững vàng chính là cách bạn thu hút những người hâm mộ cuồng nhiệt. Họ hướng đến bạn và bảo vệ bạn. Và giống như câu "hữu xạ tự nhiên hương", họ sẽ loan truyền tiếng thơm xa hơn, rộng hơn và nồng nhiệt hơn bất cứ loại hình quảng cáo nào.

Lập trường vững vàng không phải tự dưng mà có. Bạn sẽ phải hạ bệ một số người. Họ sẽ cáo buộc bạn là kẻ kiêu căng, hống hách. Đời là thế. Có người yêu mến bạn thì cũng có kẻ căm ghét bạn. Nếu chẳng có ai khó chịu về những gì bạn nói, thì có thể là bạn chưa đủ quyết liệt. (Và như thế cũng có nghĩa là bạn rất nhàm chán!).

Rất nhiều người chẳng ưa chúng tôi bởi vì sản phẩm của chúng tôi ít tính năng hơn của các đối thủ cạnh tranh. Họ cảm thấy bị xúc phạm khi chúng tôi từ chối đưa vào sản phẩm của mình các tính năng mà họ muốn cung cấp. Nhưng chúng tôi vẫn tự hào về những sản phẩm của chính mình, cũng giống như họ tự hào về những tính năng của chính họ.

Chúng tôi thiết kế các sản phẩm theo cách đơn giản bởi vì chúng tôi nghĩ rằng hầu hết các phần mềm đều quá phức tạp: quá nhiều tính năng, quá nhiều nút bấm, quá nhiều sự rối rắm. Thế nên chúng tôi đã tạo ra những phần mềm có đặc điểm trái ngược. Nếu những gì chúng tôi tạo ra không phù hợp với tất cả mọi người thì điều đó cũng chẳng sao. Chúng tôi sẵn lòng để mất một số khách hàng nếu những khách hàng khác yêu thích sản phẩm của chúng tôi một cách mãnh liệt. Đó chính là lập trường của chúng tôi.

Khi bạn không biết mình tin tưởng điều gì thì mọi thứ đều trở nên mâu thuẫn; mọi thứ đều có thể đem ra tranh luận. Nhưng khi bạn đứng về một điều gì đó, thì các quyết định sẽ rõ ràng hơn bao giờ hết.

Ví dụ, siêu thị Whole Foods chuyên bán các sản phẩm hữu cơ tự nhiên chất lượng cao nhất. Họ không lãng phí thời gian trong việc quyết định hết lần này đến lần khác xem cái gì là phù hợp. Chẳng ai hỏi: "Liệu chúng ta có nên bán sản phẩm có chất điều vị nhân tạo này không?". Không có tranh luận ở đây. Câu trả lời đã rõ. Đó là lý do tại sao bạn không thể mua một lon Coca, hay một gói bánh Snickers ở đó.

Hiển nhiên, thực phẩm ở Whole Foods cũng sẽ đắt hơn. Một vài kẻ ác ý còn gọi họ là "Whole Paycheck" (khoản tiền lớn) và chọc quê những người mua sắm tại đó. Nhưng thế thì sao nào? Whole Foods đang kinh doanh hết sức khấm khá.

Một ví dụ khác là Cửa hàng bánh mì kẹp Vinnie’s nằm ngay trên con đường gần văn phòng ở Chicago của chúng tôi. Họ cho dầu húng quế tự làm vào bánh mì để tăng thêm hương vị. Thế nên bạn đừng đến quá muộn. Hãy thử hỏi khi nào họ đóng cửa và người phụ nữ ngồi phía sau quầy sẽ trả lời: "Chúng tôi đóng cửa khi hết bánh mì".

Thật thế sao? "Vâng. Chúng tôi lấy bánh mì từ một lò bánh ở dưới phố vào sáng sớm, lúc những chiếc bánh vừa mới ra lò. Khi hết bánh (thường là vào tầm hai hoặc ba giờ chiều) thì chúng tôi đóng cửa. Chúng tôi có thể mua thêm bánh mì trong ngày, nhưng những ổ bánh ấy sẽ không ngon bằng loạt bánh mới ra lò lúc sáng. Nếu bánh mì không ngon thì bán thêm vài ổ để làm gì. Vài đô-la thì không thể nào bù đắp cho việc phải bán loại thức ăn mà chúng tôi không thấy tự hào về nó".
Hẳn là bạn cũng thích ăn ở một nơi như thế thay vì đến mấy cửa hàng bán sandwich thông thường chứ?

## Sứ mệnh bất khả thi

Có một sự khác biệt rất lớn giữa việc tuyên bố sứ mệnh và thực thi sứ mệnh. Bạn biết đó, những khẩu hiệu kiểu như "cung cấp dịch vụ tốt nhất" được tạo ra chỉ để đem dán lên tường. Những câu này nghe rất giả dối và xa rời thực tế.

Hãy tưởng tượng bạn đang đứng ở một văn phòng cho thuê xe hơi. Căn phòng lạnh lẽo. Thảm lót sàn dơ bẩn. Quầy phục vụ trống trơn. Rồi bạn nhìn thấy một mảnh giấy tả tơi với vài biểu tượng được gắn trên một tấm bảng. Đó là một bản tuyên bố sứ mệnh:

Sứ mệnh của chúng ta là đáp ứng các nhu cầu thuê xe ô tô, xe tải, buôn bán xe và các nhu cầu có liên quan của khách hàng. Khách hàng sẽ hài lòng về phong cách phục vụ chuyên nghiệp và dịch vụ chất lượng của chúng ta.

Chúng ta sẽ giành được sự tín nhiệm lâu dài của khách hàng bằng chất lượng dịch vụ chứ không phải là những lời hứa hẹn. Chúng ta sẽ thành thật, thẳng thắn và hết sức tận tụy nhằm mang lại dịch vụ tốt nhất cho khách hàng khi giao dịch với chúng ta.

Chúng ta phải tạo động lực cho các nhân viên của mình để họ phục vụ khách hàng ngày một tốt hơn bằng cách cho họ các cơ hội phát triển bản thân và đền đáp xứng đáng công sức đóng góp của họ...

Những dòng chữ cứ tiếp tục chạy dài như thế. Bạn ngồi đó, đọc cái thứ tào lao ấy và tự hỏi: "Họ xem mình là kẻ ngốc nào đây?". Những lời lẽ trên tờ giấy có dính dáng gì đến thực tế mà bạn đang trải nghiệm cả đâu.

Việc này cũng giống như khi bạn phải chờ máy và một giọng nói được ghi âm sẵn bảo với bạn là công ty của họ đánh giá cao khách hàng như thế nào. Thật thế à? Nếu vậy thì anh nên thuê người hỗ trợ để tôi không cần phải chờ đến nửa tiếng đồng hồ để được phục vụ.

Hoặc thà anh không nói gì, chứ đừng cho tôi nghe một đoạn ghi âm bảo với tôi là anh quan tâm tôi tới mức nào. Đó chỉ là một con rô-bốt thôi. Tôi biết sự khác biệt giữa tình cảm thực sự và một con rô-bốt được lên chương trình để nói những lời dễ nghe.

Ủng hộ một điều gì đó không chỉ là viết nó ra, mà bạn phải thực sự tin tưởng và thực thi nó.

## Huy động vốn từ nguồn lực bên ngoài là phương án cuối cùng

Một trong những thắc mắc đầu tiên có thể bạn sẽ hỏi là: vốn từ đâu mà ra? Thường thì nhiều người vẫn nghĩ rằng nên huy động vốn từ những người ngoài cuộc. Đúng là nếu bạn sắp xây nhà máy hay mở nhà hàng thì có thể bạn cần huy động nguồn vốn bên ngoài; nhưng có rất nhiều công ty không cần phải có cơ sở hạ tầng đắt đỏ như vậy, đặc biệt là trong bối cảnh hiện nay.

Giờ đây chúng ta đang ở trong nền kinh tế dịch vụ. Các doanh nghiệp kinh doanh dịch vụ (tư vấn, các công ty phần mềm, dịch vụ cưới hỏi, thiết kế ảnh và hàng trăm ngành nghề khác) không cần phải có nhiều vốn. Nếu bạn đang điều hành một doanh nghiệp như thế, hãy tránh sử dụng nguồn vốn từ bên ngoài.

Thật ra, bất luận bạn đang kinh doanh theo loại hình nào, hãy sử dụng càng ít nguồn vốn từ bên ngoài càng tốt. Tiêu tiền của người khác nghe có vẻ hay, nhưng có một chiếc thòng lọng kèm theo. Đây là lý do:

Bạn mất kiểm soát. Khi sử dụng vốn của người khác, tức là bạn phải giải trình trước họ. Thoạt đầu việc ấy cũng tốt thôi, khi mà tất cả mọi người đều đồng thuận với nhau. Nhưng rồi việc gì sẽ xảy ra? Liệu bạn có mở công ty để phục tùng người khác? Nếu phải thì bạn cứ huy động vốn như thế và đó chính xác sẽ là kết cục của bạn.

Sử dụng nguồn vốn từ bên ngoài bóp chết việc gầy dựng một doanh nghiệp chất lượng. Các nhà đầu tư luôn muốn thu hồi vốn càng nhanh càng tốt (thường là sau ba hoặc năm năm). Các dự án kinh doanh dài hạn sẽ khó lòng tồn tại khi những người có liên quan chỉ muốn nhanh chóng thu hồi vốn như thế.

Tiêu tiền của người khác sẽ gây nghiện. Chẳng có gì dễ dàng hơn là tiêu tiền của người khác. Nhưng rồi bạn sẽ sử dụng hết và lại muốn có thêm. Và mỗi lần bạn huy động
thêm như thế, họ sẽ nắm nhiều cổ phần hơn trong công ty của bạn.

Thỏa thuận bất lợi. Khi vừa mới bắt đầu, bạn chẳng có đòn bẩy gì cả. Đó là thời điểm kinh khủng khi bước vào bất cứ vụ giao dịch tài chính nào.

Khách hàng mới là quan trọng. Nếu sử dụng nguồn vốn từ bên ngoài, có thể cuối cùng bạn sẽ gầy dựng những gì mà các nhà đầu tư muốn, thay vì những gì mà khách hàng muốn.

Việc huy động vốn cực kỳ rắc rối. Tìm kiếm nguồn tài trợ rất khó khăn và làm bạn kiệt sức. Nó làm tốn hàng tháng trời hội họp căng thẳng, hoàn tất các thủ tục pháp lý, hợp đồng... Điều đó thực sự khiến bạn phân tâm trong khi bạn cần tập trung làm nên việc lớn.

Không đáng để làm như thế chút nào. Rất nhiều chủ doanh nghiệp đã đi theo con đường ấy và rồi hối hận. Họ thường kể với chúng ta về sự biến đổi của kịch bản huy động vốn như sau: Thoạt tiên, bạn nhận được khoản tiền đầu tư thật nhanh chóng. Nhưng sau đó, khi bắt đầu họp với các nhà đầu tư hoặc ban giám đốc, bạn sẽ tự nhủ: "Trời đất ơi! Tôi đã tự đẩy mình vào tình cảnh gì thế này?". Giờ thì một kẻ khác đã nắm quyền quyết định.

Trước khi bạn đưa đầu vào chiếc thòng lọng ấy, hãy tìm phương án khác.

## Bạn cần ít hơn bạn nghĩ

Bạn có thực sự cần mười người không, hay chỉ hai hoặc ba người là đủ cho thời điểm hiện tại?

Bạn có thực sự cần 500.000 đô-la không, hay chỉ 50.000 hoặc 5.000 đô-la là đủ vào lúc này?

Bạn có thực sự cần đến sáu tháng không, hay bạn có thể hoàn thành công việc chỉ trong hai tháng?

Bạn có thực sự cần một văn phòng lớn không, hay bạn có thể chia sẻ không gian làm việc (hoặc làm việc tại nhà) trong một thời gian?

Bạn có thực sự cần một nhà kho không, hay bạn có thể thuê một chỗ lưu trữ nhỏ hoặc mua ngoài hoàn toàn thay vì tự sản xuất?

Bạn có thực sự cần phải làm quảng cáo và thuê một công ty PR không, hay là vẫn có những cách khác để được chú ý?

Bạn có thực sự cần phải xây dựng nhà máy không, hay bạn có thể thuê một đơn vị khác sản xuất cho bạn?

Bạn có thực sự cần một viên kế toán không, hay bạn có thể sử dụng Quicken [[3]](#3) và tự làm lấy sổ sách?

Bạn có thực sự cần lập ra phòng Công nghệ thông tin không, hay bạn có thể thuê ngoài?

Bạn có thực sự cần một nhân viên lễ tân không, hay bạn có thể tự làm công việc giải đáp thắc mắc cho khách hàng?
Bạn có thực sự cần một cửa hàng bán lẻ không, hay bạn có thể bán sản phẩm qua mạng?

Bạn có thực sự cần những tấm danh thiếp thật oai và tờ quảng cáo không, hay bạn có thể bỏ qua những thứ ấy?

Giờ thì bạn thấy rồi đấy. Có lẽ dần dần bạn cần phải khuếch trương và chi tiêu nhiều hơn, nhưng không phải ngay bây giờ.

Việc chi tiêu dè sẻn chẳng có gì sai trái cả. Khi tung sản phẩm đầu tiên của mình ra thị trường, chúng tôi đã thực hiện với chi phí rất rẻ. Chúng tôi không xây văn phòng riêng mà chia sẻ không gian làm việc với một công ty khác. Chúng tôi không có nhiều trụ sở; chúng tôi chỉ có một mà thôi. Chúng tôi không làm quảng cáo mà chỉ quảng cáo bằng cách chia sẻ kinh nghiệm trên mạng. Chúng tôi không thuê người trả lời hộp thư khách hàng; chính nhà sáng lập công ty tự trả lời. Và mọi thứ đều diễn ra ổn thỏa.

Nhiều công ty lớn đã khởi sự trong ga-ra để xe. Công ty của bạn cũng có thể như vậy.

## Thành lập một công ty - không phải một công ty mới thành lập

À, công ty mới thành lập! Đó là loại công ty đặc biệt rất được chú ý (nhất là trong thế giới công nghệ).

Một công ty mới thành lập là một nơi đầy ma thuật. Đó là nơi mà chi phí là vấn đề của kẻ khác. Đó là nơi mà cái gọi là doanh thu chỉ là chuyện vặt. Đó là nơi mà bạn có thể tiêu tiền của người khác cho đến khi bạn tìm ra cách tự kiếm lấy tiền. Đó là nơi mà luật kinh doanh không được áp dụng.

Vấn đề của cái nơi đầy ma thuật này là nó chỉ có trong chuyện cổ tích. Sự thật là mọi doanh nghiệp, mới hay cũ, cũng đều chịu sự chi phối của các nguồn lực thị trường và các quy luật kinh tế. Doanh thu đi vào, chi phí đi ra. Hoặc là làm ăn có lời, hoặc là thua lỗ.

Các công ty mới thành lập cố gắng phớt lờ thực tế này. Những người điều hành đang cố gắng trì hoãn điều không thể tránh khỏi; đó là thời khắc mà doanh nhiệp của họ phải trưởng thành, làm ăn có lãi và trở thành một doanh nghiệp thực thụ, có khả năng chống chọi để tồn tại.

Khi bước vào con đường kinh doanh, bất kỳ ai có thái độ "chúng ta sẽ tìm ra cách kiếm lời trong tương lai" đều bị xem là lố bịch. Việc đó giống như là chế tạo tên lửa nhưng lại mở lời: "Hãy vờ như trọng lực không tồn tại." Kinh doanh mà không hướng đến lợi nhuận thì không phải là kinh doanh, đó chỉ là một thú vui.

Vì thế, đừng dùng ý tưởng về một công ty mới thành lập làm chỗ dựa. Thay vào đó, hãy thành lập một doanh nghiệp thực thụ. Một doanh nghiệp thực thụ phải đương đầu với những vấn đề thực tế như các hóa đơn và nợ phải trả. Một doanh nghiệp thực thụ lo nghĩ về lợi nhuận ngay từ ngày đầu tiên. Một doanh nghiệp thực thụ không che đậy những vấn đề sâu xa bằng cách nói: "Không sao đâu. Chúng ta là công ty mới thành lập mà." Hãy hành động như một doanh nghiệp thực thụ và bạn sẽ dễ tiến đến thành công hơn.

## Trốn chạy là hạ sách

Một điều mà bạn cũng rất hay nghe: "Chiến lược thoát thân của anh là gì?". Bạn nghe thấy câu này ngay khi bạn vừa bắt tay khởi sự. Điều này có ý nghĩa gì đối với những người thậm chí chưa làm mà đã nghĩ đến chuyện từ bỏ? Sự hấp tấp này là gì? Những việc ưu tiên cần làm sẽ trở nên rối rắm nếu bạn nghĩ đến chuyện thoát thân trước cả khi thực sự dấn thân vào công việc.

Liệu bạn có bắt đầu hẹn hò và đồng thời lên kế hoạch chia tay? Liệu bạn có viết bản hợp đồng hôn nhân vào lần đầu gặp gỡ? Liệu bạn có hẹn gặp luật sư giải quyết thủ tục ly hôn vào ngày cưới của mình? Điều đó thật nực cười, đúng không nào?

Bạn cần một chiến lược cam kết, chứ không phải một chiến lược thoát thân. Bạn nên nghĩ về cách làm cho dự án của mình phát triển và thành công, chứ không phải cách trốn chạy. Nếu toàn bộ chiến lược của bạn là về việc thoát thân, thì có nhiều khả năng là bạn sẽ không thể tiến xa ngay từ đầu.

Bạn thấy nhiều doanh nhân mong mỏi bán công ty của mình. Nhưng thực sự khả năng này rất nhỏ. Cơ hội bên mua định đúng giá rất mong manh. Có lẽ là 1/1.000 hoặc 1/10.000.

Thêm vào đó, khi bạn gầy dựng một công ty với ý định bán nó đi, bạn đã chọn nhầm đường. Thay vì chú tâm vào việc làm sao để khách hàng yêu mến mình, bạn lại lo lắng ai sẽ mua lại công ty của bạn. Bạn không nên để mình bị ám ảnh về điều đó.

Giờ thì cứ giả sử như bạn phớt lờ lời khuyên này và cứ làm theo cách của mình. Bạn gầy dựng một doanh nghiệp, bán nó đi, nhận một khoản hời. Rồi sao nữa? Chuyển đến một hòn đảo và nhấm nháp pina colada [[4]](#4) suốt cả ngày? Liệu điều đó có làm bạn thỏa mãn? Liệu chỉ có tiền thôi thì có thực sự làm bạn hạnh phúc? Bạn có chắc bạn thực sự thích điều đó hơn là điều hành một doanh nghiệp mà bạn thực sự tâm huyết và tin tưởng?

Đó là lý do tại sao bạn thường nghe nhiều chủ doanh nghiệp bán đi công ty của mình, rút lui trong vòng sáu tháng, rồi quay trở lại cuộc chơi. Họ nhớ thứ mà họ đã từ bỏ. Và thường thì việc kinh doanh sau của họ không tốt bằng công việc ban đầu.

Đừng bao giờ như thế. Nếu bạn đã xoay xở để thành lập được một doanh nghiệp đầy tiềm năng, hãy duy đến hai lần. Đừng để công việc kinh doanh của bạn đến rồi đi.

## Hạn chế quy mô lớn

Hãy tuân thủ nguyên tắc hạn chế quy mô lớn. Ngay lúc này, bạn là một công ty nhỏ và gọn nhất. Nhưng kể từ đây, bạn sẽ bắt đầu gia tăng quy mô. Một vật càng lớn thì tiêu thụ càng nhiều năng lượng khi vận động. Điều này đúng cho thế giới vật chất cũng như thế giới kinh doanh.

Quy mô ngày càng gia tăng bởi:
- Hợp đồng dài hạn
- Dư thừa nhân viên
- Các quyết định thường trực
- Họp hành
- Quy trình rắc rối
- Kiểm kê
- Sự lệ thuộc vào các phần cứng, phần mềm và công nghệ
- Kế hoạch dài hạn
- Chính sách công ty

Hãy hạn chế tối đa những việc này. Bằng cách đó, bạn sẽ có thể thay đổi phương hướng dễ dàng hơn.

Các tổ chức khổng lồ có thể mất nhiều năm trời để xoay chuyển. Họ chỉ nói thay vì hành động. Họ hội họp liên tục thay vì bắt tay vào làm. Nhưng nếu bạn giữ quy mô nhỏ, bạn có thể nhanh chóng thay đổi bất cứ thứ gì: toàn bộ mô hình kinh doanh, sản phẩm, đặc tính và thông điệp quảng cáo. Bạn có thể phạm sai lầm và nhanh chóng sửa chữa. Bạn có thể thay đổi các điều ưu tiên, tổ hợp sản phẩm hoặc trọng tâm mong muốn. Điều quan trọng nhất là bạn có thể thay đổi tư duy.


# Chương 4: Tiến triển

## Tận dụng sự hạn chế

"Tôi không có đủ thời gian/tiền bạc/nhân lực/kinh nghiệm". Hãy thôi than vãn những kiểu như vậy. Ít là tốt. Sự hạn chế là một lợi thế mà không phải ai cũng nhận ra. Nguồn lực hạn chế bắt buộc bạn phải tiến hành công việc với những gì bạn có. Không có chỗ cho sự lãng phí. Điều đó bắt buộc bạn phải trở nên sáng tạo.

Bạn có bao giờ thấy những món vũ khí do các tù nhân chế tạo từ xà phòng hoặc từ những chiếc muỗng chưa? Họ đã xoay xở làm tốt công việc với những gì mình có. Không phải chúng tôi đang khuyên bạn đi ra ngoài và cắt cổ ai, nhưng hãy sáng tạo và bạn sẽ bất ngờ trước những gì mình có thể làm với nguồn tài nguyên ít ỏi.

Các nhà thơ, nhà văn lỗi lạc luôn sử dụng sự hạn chế để thúc đẩy óc sáng tạo. Đại thi hào Shakespeare miệt mài với những niêm luật, vần điệu chặt chẽ của những bài sonnet (bài thơ mười bốn câu có khuôn vần đặc biệt). Thơ Haiku (thể thơ rất ngắn của Nhật) cũng có những quy luật khắt khe để cho ra những bài thơ đầy chất thơ. Đại văn hào Ernest Hemingway và nhà văn Raymond Carver đã nhận thấy việc ép buộc bản thân sử dụng ngôn ngữ súc tích và khúc chiết giúp họ tạo ra hiệu ứng tối đa.

The price is right(tạm dịch:Hãy chọn giá đúng), chương trình trò chơi truyền hình dài nhất trong lịch sử Hoa Kỳ, cũng là một minh chứng của sự sáng tạo nảy sinh từ sự hạn chế. Chương trình có hàng trăm trò chơi, mỗi trò dựa trên một câu hỏi: "Món hàng này giá bao nhiêu?". Công thức đơn giản này đã hấp dẫn được rất nhiều người xem trong hơn ba mươi năm qua.

Không giống như bao hãng hàng không khác, hãng Southwest chỉ sử dụng độc nhất loại máy bay Boeing 737. Kết quả là mỗi phi công, tiếp viên hàng không và nhân viên sân bay đều có thể điều hành được tất cả các chuyến bay. Ngoài ra, mọi cơ sở vật chất của Southwest đều tương thích với tất cả máy bay của họ. Những điều đó có nghĩa là chi phí sẽ thấp hơn và việc điều hành sẽ đơn giản hơn. Họ đã làm cho mọi thứ trở nên dễ dàng và thuận tiện hơn cho chính mình.

Khi xây dựng Basecamp, chúng tôi gặp rất nhiều hạn chế. Chúng tôi có một hãng thiết kế phải điều hành, múi giờ của những người quản lý cách nhau bảy tiếng đồng hồ (David lúc ấy đang làm công việc lập trình ở Đan Mạch, những người còn lại làm việc ở Mỹ), ít người và không có nguồn tài trợ từ bên ngoài. Những hạn chế này đã buộc chúng tôi phải làm sao cho sản phẩm của mình phải thật đơn giản.

Giờ đây, chúng tôi có nhiều nguồn tài nguyên và nhân lực hơn, nhưng chúng tôi vẫn đề cao sự hạn chế. Chúng tôi đảm bảo mỗi lúc chỉ có một hoặc hai người làm việc trên một sản phẩm. Và chúng tôi luôn giữ các tính năng ở mức tối thiểu. Tự uốn mình theo khuôn khổ như thế giúp chúng tôi "quên đi" những sản phẩm quá phức tạp.

Vì vậy, trước khi than vãn về sự thiếu thốn, hãy xem thử liệu bạn có thể làm được gì với những gì mình có.

## Ưu tiên chất lượng hơn số lượng

Nếu cố gắng thực hiện tất cả mọi việc cùng lúc, bạn có thể nhanh chóng biến bao ý tưởng tuyệt vời mà bạn ấp ủ thành công cốc. Bạn không thể làm tất cả mọi thứ mà bạn muốn và làm cho thật hoàn hảo được. Thời gian, nguồn lực, khả năng và sự tập trung của bạn là có hạn. Làm hoàn hảo một việc là đã khó khăn lắm rồi, cố gắng làm cả mười việc cùng một lúc à? Quên đi nhé!

Do vậy, hãy buông bỏ một vài việc để đạt được kết quả tốt hơn. Hãy giảm tham vọng của mình xuống bớt một nửa. Tốt hơn là bạn nên ưu tiên cho chất lượng thay vì số lượng.

Rất nhiều thứ trở nên tốt hơn khi được rút ngắn lại. Đạo diễn cắt riêng những phân cảnh hay để tạo nên một bộ phim ấn tượng. Nhạc sĩ chọn ra những bài hát hay để tạo nên một album tuyệt vời. Chúng tôi cũng đã rút gọn quyển sách này xuống một nửa, từ 57 ngàn từ xuống còn 27 ngàn từ. Hãy tin chúng tôi, làm thế tốt hơn nhiều.

Thế nên hãy bắt đầu cắt bớt. Hãy tạo ra những điều xuất sắc bằng cách cắt bớt những phần chỉ ở mức vừa hay.

## Hãy bắt đầu từ tâm chấn

Khi bạn bắt đầu một việc mới mẻ, có nhiều nguồn lực lôi kéo bạn theo nhiều hướng khác nhau. Có những việc bạn có thể làm, những việc bạn muốn làm và những việc bạn phải làm. Những việc bạn phải làm là nơi mà bạn nên bắt đầu. Hãy bắt đầu ngay từ tâm chấn.

Ví dụ, nếu mở một quầy bán hot-dog (một loại bánh mì kẹp xúc xích), bạn có thể lo nghĩ về các loại gia vị, xe đẩy, bảng hiệu, trang trí. Nhưng điều đầu tiên bạn nên nghĩ đến chính là chiếc bánh hot-dog. Những chiếc bánh hot-dog chính là tâm chấn. Mọi thứ khác chỉ là phần phụ mà thôi.

Cách tìm ra tâm chấn là hãy tự hỏi bản thân: "Nếu loại bỏ điều này thì liệu tôi có tiếp tục bán được hay không?". Một quầy bán hot- dog không còn là một quầy bán hot-dog nếu không có những chiếc bánh hot-dog. Bạn có thể bỏ bớt hành tây, các loại gia vị, mù tạc... Một số người có thể không thích những chiếc bánh nếu thiếu đi những thứ ấy, nhưng bạn vẫn có một quầy bán hot-dog. Còn nếu không có những chiếc bánh hot-dog thì bạn không thể nào có quầy bán hot-dog được.

Vì vậy, hãy tìm ra tâm chấn. Phần nào là không thể bỏ đi? Nếu bạn vẫn có thể tiếp tục mà không cần có thứ đó thì hẳn nó không phải là tâm chấn. Khi bạn tìm ra nó, bạn sẽ biết ngay thôi. Sau đó, hãy tập trung hết sức để tạo ra một sản phẩm hoàn hảo. Mọi việc khác mà bạn làm sẽ lệ thuộc vào phần cốt lõi ấy.

## Chi tiết để sau

Các kiến trúc sư chỉ bận tâm về việc lát đá loại nào trong buồng tắm hoặc lắp kệ bếp kiểu gìsau khihoàn thành bản thiết kế sàn nhà. Họ biết tốt hơn là nên quyết định các chi tiết ấy sau.

Bạn cần tiếp cận ý tưởng của mình theo cùng một cách như thế. Các chi tiết tạo nên sự khác biệt. Nhưng mải mê với các chi tiết quá sớm sẽ dẫn đến bất đồng và trì hoãn. Bạn sẽ lạc lối trong những thứ không thực sự quan trọng. Bạn lãng phí thời gian với những quyết định mà sau này cũng sẽ thay đổi. Vì vậy, hãy phớt lờ các chi tiết trong thời gian đầu. Hãy tập trung vào những điều cốt lõi trước, rồi hãy bận tâm đến các chi tiết sau.

Khi bắt đầu bản thiết kế, chúng tôi phác họa ý tưởng của mình bằng bút Sharpie, một loại bút đánh dấu to bản, thay vì bút bi ngòi nhỏ. Tại sao? Bút bi có nét quá mảnh, độ phân giải quá cao. Bút bi ngòi nhỏ khiến bạn bận tâm về những thứ mà bạn chưa nên bận tâm, chẳng hạn như hoàn thiện phần bóng hoặc nên sử dụng đường nhiều chấm hay đường thẳng nét liền. Cuối cùng, bạn lại đi tập trung vào những thứ chưa nên tập trung ở thời điểm hiện tại.

Bút Sharpie khiến bạn không thể đi sâu vào chi tiết như thế. Bạn chỉ có thể vẽ được các hình khối, đường thẳng và hình hộp. Điều đó rất tốt. Bức tranh tổng thể chính là tất cả những gì mà bạn nên bận tâm lúc mới bắt đầu.

Walt Stanchfield, chỉ đạo hình ảnh lừng danh của hãng Walt Disney, từng khuyến khích các nhân viên đồ họa của mình "quên đi chi tiết" khi bắt đầu vẽ. Ông đưa ra lý do: các chi tiết chẳng đem lại được gì trong giai đoạn đầu cả.

Ngoài ra, bạn thường không thể nhận ra các chi tiết quan trọng nhất cho đến tận sau khibạn bắt đầu tiến hành. Đó là lúc bạn nhìn thấy được cái mình cần phải chú trọng và cảm nhận được những gì đang thiếu. Đó chính là lúc bạn nên bận tâm đến chi tiết, chứ trước đó thì không nên.

## Ra quyết định là tiến triển

Khi bạn chần chừ chưa quyết định, các vấn đề sẽ chồng chất lên nhau. Và các vấn đề chồng chất như thế sẽ bị phớt lờ, giải quyết một cách hấp tấp hoặc quẳng sang một bên. Kết quả là từng vấn đề riêng lẻ trong đó sẽ không được giải quyết.

Bất cứ khi nào có thể, hãy thay đổi câu: "Chúng ta hãy suy nghĩ về việc này" thành câu "Hãy quyết định việc này". Hãy tuân thủ việc ra quyết định. Đừng chờ đợi giải pháp hoàn hảo. Hãy quyết định và tiến về phía trước.

Khi quen dần việc luân phiên thực hiện các quyết định, bạn sẽ tạo được đà và nâng cao tinh thần. Quyết định chính là sự tiến triển. Mỗi một quyết định mà bạn đưa ra chính là một viên gạch tạo nên nền móng vững chắc cho bạn. Bạn không thể nào xây xong một ngôi nhà dựa trên kiểu tư duy "cứ để sau". Hãy xây dựng nền tảng dựa trên những việc đã hoàn tất.

Bạn sẽ tạo thêm nhiều rắc rối khi chần chừ chờ đợi một giải pháp hoàn hảo. Nó sẽ chẳng đến đâu. Bạn hoàn toàn có thể đưa ra quyết định đúng đắn trong ngày hôm nay chứ không cớ gì phải đợi đến ngày mai.

Một điển hình từ thực tế công việc của chúng tôi: trong một thời gian dài, chúng tôi tránh tạo ra một chương trình liên kết cho các sản phẩm bởi giải pháp "hoàn hảo" có vẻ quá phức tạp. Chúng tôi phải tự động hóa việc thanh toán, gửi các chi phiếu qua bưu điện, tìm ra luật thuế áp dụng cho những chi nhánh ở nước ngoài... Bước đột phá xuất hiện khi chúng tôi đặt câu hỏi: "Chúng ta có thể làm gì ngay lúc này để có được kết quả tối ưu?". Và chúng tôi tìm ra câu trả lời: Hãy trả cho các chi nhánh bằng tín dụng thay vì tiền mặt. Thế là chúng tôi làm vậy.

Bài học từ việc này: bạn không cần phải sống với một lựa chọn suốt cả đời. Nếu bạn phạm sai lầm, bạn vẫn có thể sửa chữa sau đó.

Việc bạn lên bao nhiêu kế hoạch không quan trọng vì dù sao bạn sẽ vẫn có một số sai lầm. Đừng làm cho mọi việc tệ hại hơn bằng cách phân tích quá chi tiết và trì hoãn trước khi thực sự bắt đầu.

Các dự án dài hạn làm kiệt quệ tinh thần. Càng tốn nhiều thời gian để lên kế hoạch, khả năng thực hiện sẽ càng giảm. Hãy quyết định và bắt tay làm ngay khi bạn có đủ đà và động lực.

## Hãy là một giám tuyển

Không phải đặt tất cả các tác phẩm nghệ thuật trên thế giới vào một căn phòng thì sẽ có một viện bảo tàng vĩ đại. Đó đơn giản chỉ là một cái nhà kho. Cái làm cho một viện bảo tàng trở nên vĩ đại là những thứ không được treo trên tường.

Chính những thứ bạn loại ra mới quan trọng. Vì vậy, hãy liên tục tìm kiếm những thứ để chuyển đi, đơn giản hóa và sắp xếp cho hợp lý. Hãy là một giám tuyển [[5]](#5). Giám tuyển là người dồn hết tâm trí vào công việc, đưa ra những quyết định sáng suốt về những gì nên giữ và những gì nên bỏ đi. Hãy bám sát những gì tinh túy. Hãy xén bớt cho đến khi chỉ còn lại những thứ thực sự quan trọng.

Zingerman’s là một trong những cửa hàng bán đặc sản nổi tiếng nhất nước Mỹ. Zingerman’s được như thế bởi chủ cửa hàng nghĩ mình là giám tuyển. Họ không chất đầy các kệ hàng mà thực sự quan tâm đến những gì mình bán.

Đội bán hàng ở Zingerman’s đều tin mỗi chai dầu olive họ bán ra đều tuyệt diệu. Thường thì họ biết các nhà cung cấp trong nhiều năm. Họ đến tận nơi sản xuất và mang dầu về dùng thử. Đó là lý do vì sao họ có thể thẩm định hương vị thơm ngon đích thực của mỗi loại dầu.

Hãy xem cách mà chủ cửa hàng Zingerman’s mô tả loại dầu olive Pasolivo trên trang web của công ty:

Tôi thử dầu olive Pasolivo lần đầu tiên hồi vài năm trước trong một buổi giới thiệu sản phẩm. Có rất nhiều loại được đóng chai rất đẹp cùng những bài giới thiệu êm tai mà chất lượng thực sẽ không được như vậy, và quả thật, chẳng có loại nào thực sự nổi bật. Trái lại, Pasolivo khiến tôi chú ý ngay khi tôi nếm thử. Nó thật đậm đà và ngọt ngào. Nó hội tụ đủ tất cả những hương vị mà tôi ưa thích, chẳng có điều gì đáng chê cả. Pasolivo vẫn giữ vững vị trí là loại dầu số một ở Hoa Kỳ, ngang hàng với các loại dầu thô tuyệt hảo của vùng Tuscany [[6]](#6). Xin được trân trọng giới thiệu đến quý khách hàng.

Người chủ cửa hàng đã thực sự thử qua loại dầu ấy và chọn giới thiệu dựa trên hương vị của nó, chứ không phải dựa trên bao bì, quảng cáo, hay giá cả. Đó cũng chính là phương pháp mà bạn nên áp dụng.

## Đơn giản hóa vấn đề

Hãy xem chương trình Kitchen Nightmares (tạm dịch:Cơn ác mộng trong nhà bếp) của bếp trưởng Gordon Ramsay và bạn sẽ hiểu tại sao cần đơn giản hóa vấn đề. Những nhà hàng thất bại đưa ra thực đơn có quá nhiều món ăn. Các chủ nhà hàng cho là thực đơn đa dạng và phong phú sẽ thu hút thực khách, nhưng thật ra, nó khiến khách hàng nghĩ rằng quá nhiều món ăn thì chất lượng sẽ kém và không hấp dẫn.

Đó là lý do tại sao bước đầu tiên của Ramsay gần như luôn là cắt tỉa bớt thực đơn, thường là từ hơn ba mươi món xuống còn khoảng mười món. Việc cải thiện thực đơn hiện tại không phải là ưu tiên hàng đầu. Ưu tiên hàng đầu chính là giảm bớt số lượng món ăn trong thực đơn. Sau đó, ông sẽ làm cho những món còn lại trong thực đơn trông ngon mắt.

Khi mọi thứ diễn ra không suôn sẻ, chúng ta thường có xu hướng đổ dồn thêm các thứ vào vấn đề. Chúng ta muốn có nhiều người hơn, dành thêm nhiều thời gian hơn và tiêu tốn nhiều tiền bạc hơn. Tất cả những thứ đó rốt cuộc lại làm cho vấn đề thêm phức tạp. Con đường đúng đắn để đi thực ra là theo hướng ngược lại: giảm bớt đi.

Giá trị cốt lõi không bao giờ thay đổi
Rất nhiều công ty thường dốc toàn tâm toàn lực vào những sự kiện lớn sắp xảy ra. Họ đeo đuổi những thứ mới mẻ và nóng sốt. Họ đi theo xu hướng mới và công nghệ mới nhất.

Đó là con đường của kẻ ngốc. Bạn bắt đầu tập trung vào vẻ bề ngoài thay vì bản chất bên trong. Bạn bắt đầu chú tâm vào những thứ luôn thay đổi thay vì những thứ tồn tại lâu dài.

Phần cốt lõi trong việc kinh doanh của bạn nên được xây dựng dựa trên những thứ không đổi, những thứ mà mọi người muốn có hôm nay và trong vòng mười năm sau họ vẫn muốn có. Đó là những thứ mà bạn nên đầu tư vào.

Amazon.com tập trung vào dịch vụ chuyển phát nhanh (hoặc miễn phí) bằng đường thủy, chính sách lợi nhuận thân thiện và giá cả phải chăng. Nhu cầu về những thứ này ở người dùng luôn ở mức rất cao.

Các công ty sản xuất xe hơi của Nhật cũng tập trung vào các yếu tố cốt lõi không đổi: uy tín, giá cả phải chăng và tính thực tiễn. Người ta muốn những thứ ấy hồi ba mươi năm trước, hiện tại họ vẫn muốn và ba mươi năm nữa họ vẫn sẽ muốn như vậy.

Đối với 37signals, chúng tôi chú trọng những thứ như tốc độ, sự đơn giản, tính dễ sử dụng và sự rõ ràng. Đó là những nhu cầu bất biến qua thời gian. Vì trong vài chục năm tới, sẽ không ai bảo:

"Này anh, tôi ước gì các phần mềm khó sử dụng hơn". Họ sẽ không bảo: "Tôi ước gì phần mềm ứng dụng này chạy chậm hơn".

Hãy nhớ rằng mốt sẽ dần biến mất. Khi bạn tập trung vào những đặc tính vĩnh cửu, tức là bạn đã nắm bắt được những thứ không bao giờ bị lỗi mốt.

## Âm điệu nằm trên những ngón tay

Các nghệ sĩ guitar bậc thầy thường bảo: "Âm điệu nằm trên ngón tay bạn". Bạn có thể mua cùng một cây đàn guitar, bàn đạp tạo hiệu ứng và bộ khuếch đại mà tay guitar lừng danh Eddie Van Halen sử dụng. Nhưng khi bạn chơi các thiết bị ấy, nó vẫn sẽ mang phong cách của bạn.

Tương tự như thế, Eddie có thể sử dụng một bộ nhạc cụ rẻ tiền tại hiệu cầm đồ và bạn vẫn có thể nhận ra đó là Eddie Van Halen đang chơi. Những món đồ phụ tùng thật oách có thể hỗ trợ phần nào, nhưng sự thật là âm điệu của bạn xuất phát từ chính bạn.

Mọi người thường lưu tâm quá nhiều đến các dụng cụ hay thiết bị thay vì những gì họ sẽ làm. Bạn biết đó, các nhà thiết kế sử dụng hàng loạt các mẫu chữ và các bộ lọc thời thượng trên Photoshop nhưng lại chẳng có thông điệp gì để truyền tải. Các nhiếp ảnh gia nghiệp dư mải mê tranh luận giữa kỹ thuật chụp ảnh bằng phim và bằng máy kỹ thuật số thay vì tập trung vào những gì thực sự làm cho một bức ảnh trở nên giá trị.

Rất nhiều tay golf nghiệp dư nghĩ họ cần những chiếc gậy đắt đỏ. Nhưng chính động tác mới quan trọng, chứ không phải chiếc gậy. Hãy thử đưa cho tay golf nổi tiếng thế giới Tiger Woods một bộ gậy rẻ tiền xem, anh ta vẫn sẽ hạ gục bạn như thường.

Con người xem trang thiết bị như là chỗ dựa. Họ không muốn phải dành thời gian luyện tập vất vả, nên họ đầu tư nhiều cho các thiết bị. Họ muốn đi đường tắt. Nhưng rõ ràng là bạn không cần phải có thiết bị tốt nhất mới trở nên giỏi giang, và chắc hẳn là bạn không cần nó khi vừa bắt đầu làm một điều gì đó.

Trong kinh doanh, quá nhiều người bị ám ảnh bởi các thiết bị, các thủ thuật phần mềm, các vấn đề về quy mô, không gian văn phòng rộng lớn, đồ nội thất sang trọng và những thứ phù phiếm khác, thay vì những gì thực sự trọng yếu. Những gì thực sự trọng yếu chính là làm thế nào để thu hút được khách hàng và kiếm ra tiền.

Bạn cũng nhìn thấy điều này ở những người muốn tạo blog, web, hay quay phim để quảng bá việc kinh doanh của mình, nhưng lại bị mắc kẹt ở việc lựa chọn sử dụng công cụ. Nội dung mới là điều quan trọng. Bạn có thể đổ hàng núi tiền để mua những trang thiết bị hiện đại, nhưng nếu bạn không có thông điệp để truyền tải thì những trang thiết bị ấy cũng chẳng giúp ích được gì.

Hãy sử dụng bất cứ thứ gì mà bạn đã có hoặc những thứ mà bạn có thể chi trả. Cứ việc tiến lên. Công cụ chỉ đóng "vai phụ". Khả năng sử dụng tốt những gì bạn có mới là thứ chính yếu. Nên nhớ rằng các âm điệu nằm ở ngón tay bạn.

## Bán phụ phẩm

Khi sản xuất ra một sản phẩm, bạn luôn tạo ra vài thứ kèm theo. Không thể nào chỉ tạo ra mỗi một thứ. Bất cứ thứ gì cũng có sản phẩm phụ cả. Những bộ óc kinh doanh sáng tạo và tinh ý sẽ nhắm vào những phụ phẩm đó và nhìn thấy các cơ hội rộng mở.

Ngành công nghiệp gỗ hiện đang bán những thứ đã từng là phế liệu - mùn cưa, dăm bào và những mảnh gỗ vụn - và thu lại lợi nhuận đáng kể. Bạn sẽ tìm thấy những phụ phẩm này trong các loại gỗ tổng hợp dùng cho lò sưởi, bê tông, chất tăng cường độ đông kết của nước đá, lớp phủ dùng trong nông nghiệp, ván ghép, nhiên liệu và nhiều thứ khác.

Tuy nhiên, có thể bạn chẳng sản xuất thứ gì cả. Vậy làm sao để nhận ra đâu là phụ phẩm? Những người làm việc ở những xưởng gỗ nhìn thấy rõ phế liệu của họ, bởi họ không thể không nhìn thấy mùn cưa. Nhưng bạn không thấy phế liệu của mình. Có lẽ, bạn thậm chí chẳng hề nghĩ mình có tạo ra phụ phẩm. Nhưng đó là do bạn không có tầm nhìn xa.

Quyển sách trước của chúng tôi -Getting Real (tạm dịch:Hiện thực hóa) là một sản phẩm phụ. Chúng tôi viết sách mà thậm chí không hề biết. Kinh nghiệm từ việc gầy dựng công ty và thiết lập các phần mềm trước đây là phế liệu so với việc thực thi công việc. Chúng tôi phổ biến kiến thức trước tiên là trên blog của mình, rồi trong các buổi hội thảo, tiếp đó chúng tôi tạo một file PDF, và rồi viết thành sách. Sản phẩm phụ đã trực tiếp mang về cho 37signals hơn một triệu đô-la và gián tiếp mang về thêm một triệu đô-la nữa. Quyển sách mà hiện bạn đang đọc đây cũng là một phụ phẩm.

Ban nhạc rock Wilco phát hiện ra một sản phẩm phụ rất giá trị trong quá trình thu âm. Ban nhạc quay phim lại quá trình sáng tác album và phát hành như một cuốn phim tài liệu có tênI Am Trying to Break Your Heart (tạm dịch: Anh đang làm tan vỡ trái tim em). Cuốn phim mang đến những hình ảnh hấp dẫn, không bị cắt xén bớt về quá trình sáng tạo và tranh đấu của ban nhạc. Ban nhạc đã kiếm bộn tiền từ bộ phim và cũng sử dụng nó như những viên gạch lót đường để đến với đông đảo khán giả hơn.

Henry Ford đã học cách biến những vụn gỗ trong quá trình sản xuất dòng xe Model T thành than bánh. Ông cho xây dựng nhà máy than và thế là nhà máy than Ford ra đời (sau này được đặt tên lại là nhà máy than Kingsford). Ngày nay, Kingsford vẫn là nhà sản xuất than dẫn đầu ở Hoa Kỳ.

Các công ty phần mềm thường không nghĩ đến việc viết sách. Các ban nhạc cũng không nghĩ đến việc quay phim lại quá trình thu âm. Các nhà sản xuất xe hơi ít khi nghĩ đến việc bán than. Nên chắc là có thứ gì đó bạn có thể bán được mà bạn chưa nghĩ đến.

## Tung sản phẩm ra ngay

Khi nào sản phẩm hay dịch vụ của bạn hoàn thiện? Khi nào bạn nên tung nó ra thị trường? Sản phẩm nên đến tay người dùng vào lúc nào là tốt nhất? Bạn có thể đưa sản phẩm ra sớm hơn nhiều so với bạn nghĩ. Một khi sản phẩm của bạn đã đầy đủ tính năng, hãy tung ngay ra thị trường.

Khi bạn còn một danh sách những việc phải làm không có nghĩa là sản phẩm của bạn vẫn chưa hoàn thiện. Đừng kìm hãm tất cả những việc khác chỉ vì vài việc còn dang dở. Bạn có thể thực hiện chúng sau. Và như thế cũng có nghĩa là bạn có thể thực hiện tốt hơn.

Hãy nghĩ theo hướng này: nếu bạn cần khai trương doanh nghiệp trong vòng hai tuần, bạn sẽ cắt bớt những việc gì? Cách đặt câu hỏi như thế buộc bạn phải tập trung vào những gì thực sự quan trọng. Bạn đột nhiên nhận ra có rất nhiều thứ không cần thiết. Còn những gì bạn thực sự cần sẽ trở nên rất rõ ràng. Khi đặt ra kỳ hạn, bạn sẽ minh bạch mọi thứ. Đó là cách tốt nhất để trực giác mách bảo với bạn rằng "Chúng ta không cần cái này".

Hãy gác sang một bên bất cứ thứ gì mà bạn không cần đến cho lúc khai trương. Hãy tạo dựng những thứ cần thiết vào lúc này và bận tâm về những việc khác sau.

Khi tung Basecamp ra thị trường, chúng tôi thậm chí còn chẳng có khả năng ra hóa đơn cho khách hàng! Bởi vì sản phẩm có chu kỳ ra hóa đơn hàng tháng, nên chúng tôi biết chúng tôi có ba mươi ngày để tính cách. Vì vậy, chúng tôi sử dụng thời gian trước khi tung sản phẩm ra để giải quyết những vấn đề cấp bách, thực sự quan trọng vào ngày thứ nhất. Ngày ba mươi vẫn có thể chờ.

Camper, một nhãn hiệu giày, đã mở cửa hàng ở San Francisco trước cả khi hoàn tất việc xây dựng và gọi đó là Bước Tiến Triển. Khách hàng có thể vẽ lên tường lúc cửa hàng còn chưa hoàn thiện. Camper trưng giày trên những chiếc kệ bằng gỗ dán rẻ tiền. Thông điệp nổi tiếng nhất được khách hàng viết trên tường là: "Hãy giữ nguyên phong cách của cửa hàng".

Tương tự như thế, các nhà sáng lập của Crate & Barrel cũng không chờ đợi để xây dựng cơ ngơi trưng bày sản phẩm thật lộng lẫy khi họ khai trương cửa hàng đầu tiên của mình. Họ lật ngược những chiếc thùng chứa hàng hóa và chất sản phẩm lên trên.

Đừng nhầm lẫn giữa phương pháp này với việc bỏ bê chất lượng. Bạn vẫn muốn tạo ra sản phẩm thật tốt. Phương pháp này chỉ công nhận một điều: cách tốt nhất để tiếp cận với thị trường là cứ bắt tay vào thực hiện. Hãy thôi tưởng tượng cách nào sẽ hiệu quả. Hãy tìm ra điều đó trong thực tế.

# Chương 5: Năng suất

## Ảo tưởng về sự đồng thuận

Thế giới kinh doanh trở nên bừa bộn với các loại giấy tờ chết. Những thứ đó chẳng có tác dụng gì ngoài việc làm lãng phí thời gian của con người. Những bản báo cáo chẳng ai thèm đọc, những biểu đồ chẳng ai ngó ngàng tới và những bản mô tả chẳng bao giờ giống với thành phẩm. Những thứ này phải tốn rất nhiều thời gian để tạo ra, nhưng chỉ sau vài giây là bị quên lãng.

Nếu bạn cần phải giải thích một điều gì đó, hãy cố gắng hiện thực hóa nó. Thay vì mô tả vẻ bề ngoài, hãy vẽ nó ra. Thay vì giải thích âm thanh như thế nào, hãy ngâm nga nó. Hãy làm bất cứ điều gì có thể để loại bỏ các vỏ bọc trừu tượng.

Vấn đề của những thứ trừu tượng (như là các bản báo cáo, hay giấy tờ) đó là chúng tạo ra ảo tưởng về sự đồng thuận. Một trăm người có thể đọc những từ như nhau, nhưng trong đầu, họ lại đang tưởng tượng ra một trăm thứ khác nhau.

Đó là lý do tại sao bạn nên hiện thực hóa mọi việc ngay lập tức. Chỉ có như thế thì bạn mới thấu hiểu thực sự. Giống như khi chúng ta đọc về các mô tả đặc điểm ngoại hình trong một cuốn sách, mỗi người chúng ta sẽ có những hình dung khác nhau. Chỉ khi thực sự nhìn thấy người được mô tả ở ngoài, lúc đó tất cả chúng ta mới biết được chính xác người đó trông như thế nào.

Khi hãng hàng không Alaska muốn xây một sân bay mới, họ đã không dựa vào các bản vẽ và bản phác thảo. Họ dùng một nhà kho và dùng các hộp giấy cứng để bố trí mô hình các bục, ki-ốt và vành đai. Rồi đội ngũ nhân viên tiến hành xây một mô hình nhỏ ở hải cảng để kiểm tra các hệ thống với hành khách và nhân viên thật. Thiết kế được tạo ra từ quy trình thực tế giảm đáng kể thời gian chờ đợi và gia tăng năng suất của hãng.

Sam Maloof, một thợ thủ công nội thất lành nghề người Mỹ, cảm thấy không thể nào vẽ ra một bản vẽ thể hiện được hết các chi tiết tinh vi và phức tạp của một chiếc ghế dựa hay ghế đẩu. Ông bảo: "Rất nhiều lần tôi không biết chi tiết đó trông như thế nào cho đến khi tôi cầm cái đục trên tay".

Đó là con đường mà tất cả chúng ta nên đi. Hãy cầm lấy cây đục và bắt đầu tạo nên một thứ gì đó có thật. Những thứ khác chỉ tổ làm chúng ta sao lãng mà thôi.

## Lý do để từ bỏ

Cặm cụi thực hiện những việc mà bạn nghĩ phải làm thì rất dễ. Còn việc ngừng lại và hỏi tại sao mình làm thì lại khó khăn hơn rất nhiều. Bạn sẽ tìm ra đâu là những việc thực sự quan trọng qua những câu hỏi sau:

Tại sao bạn lại làm việc này? Đã bao giờ bạn nhận ra mình đang làm một điều gì đó mà không hề biết chính xác để làm gì? Bạn làm chỉ vì có người bảo bạn hãy làm thế. Việc này khá phổ biến. Đó là lý do tại sao việc tự hỏi bản thân mình làm việc đó để làm gì là rất quan trọng. Việc đó nhằm mục đích gì? Ai sẽ được hưởng lợi? Động cơ phía sau là gì? Biết được câu trả lời sẽ giúp bạn hiểu rõ hơn về bản chất công việc.

Bạn đang giải quyết vấn đề gì? Vấn đề ở đây là gì? Khách hàng gặp rắc rối? Bạn gặp rắc rối? Có điều gì không rõ ràng? Có điều gì trước đây là không thể, nhưng bây giờ lại có thể? Đôi khi, ngay lúc tự hỏi mình, bạn sẽ phát hiện ra mình đang giải quyết một vấn đề tưởng tượng. Đó là lúc bạn nên dừng và nhìn lại những việc đang làm.

Điều này có thực sự hữu ích? Bạn đang tạo ra một thứ gì đó hữu ích hay chỉ đơn thuần tạo ra thứ đó thôi? Đôi khi chơi đùa một chút và tạo ra một món hay hay cũng chẳng hại gì. Nhưng dần dần, bạn phải dừng lại và tự hỏi bản thân xem thứ bạn tạo ra có ích hay không. Những cái hay hay mà vô dụng rồi cũng lụi tàn. Còn những thứ hữu ích thì không bao giờ như thế.

Bạn có đang gia tăng giá trị? Thêm một thứ thì dễ nhưng thêm giá trị thì không. Liệu điều bạn đang thực hiện có thực sự làm tăng giá trị sản phẩm hơn đối với khách hàng? Họ có hưởng lợi nhiều hơn so với trước hay không? Đôi khi có những thứ bạn cho là làm gia tăng giá trị, nhưng thực ra chúng lại làm giảm giá trị. Quá nhiều nước xốt sẽ làm hỏng món chiên. Giá trị đòi hỏi sự cân bằng.

Điều này có làm thay đổi hành vi? Những gì bạn đang làm có thực sự thay đổi điều gì không? Đừng thêm vào thứ gì nếu nó không có tác động thực sự lên cách mà mọi người sử dụng sản phẩm của bạn.

Có cách nào dễ dàng hơn? Bất cứ khi nào bạn làm việc gì, hãy hỏi: "Có cách nào dễ hơn không?". Thường thì cách mà bạn vừa phát hiện ra sau khi đặt câu hỏi đó sẽ hiệu quả hơn cách bạn đang áp dụng. Các vấn đề thường khá đơn giản. Chúng rắc rối là do chúng ta tưởng tượng ra mà thôi.

Bạn có thể làm gì thay thế? Bạn không thể làm việc này bởi đang bận làm việc kia? Điều này đặc biệt quan trọng đối với các doanh nghiệp nhỏ có nguồn lực hạn chế khi mà việc sắp xếp thứ tự ưu tiên trở nên tối quan trọng. Nếu bạn làm việc A, liệu bạn có thể vẫn làm cả việc B và C kịp tiến độ không? Nếu không, bạn có muốn dành ưu tiên cho B và C, thay vì A không? Nếu bạn bị mắc kẹt việc gì đó trong một thời gian dài, nghĩa là bạn còn nhiều việc khác chưa thể hoàn thành.

Có đáng làm? Những gì bạn đang làm có thực sự xứng đáng? Cuộc họp có đáng để kéo sáu người ra khỏi công việc của họ trong vòng một tiếng đồng hồ? Việc này có đáng để bạn thức trắng đêm hay cứ để mai rồi giải quyết? Bài báo của đối thủ có đáng để bạn căng thẳng? Có đáng để chi tiền cho quảng cáo? Hãy quyết định giá trị thực của những gì bạn sắp làm trước khi quyết tâm hành động.

Hãy chất vấn bản thân (và người khác) những câu hỏi nêu trên. Bạn không cần phải biến nó thành một quy trình bắt buộc, nhưng cũng đừng hời hợt với nó.

Cũng đừng quá rụt rè với các kết luận của mình. Đôi khi từ bỏ những gì bạn đang thực hiện là một động thái đúng đắn, thậm chí dù bạn đã nỗ lực vì nó rất nhiều. Đừng tiếp tục lãng phí thời gian quý báu cho những việc không hiệu quả.

## Sự quấy rầy: kẻ thù của năng suất

Nếu bạn liên tục phải thức khuya và làm việc vào ngày cuối tuần, thì đó không phải là vì có quá nhiều công việc phải làm, mà là vì bạn chưa hoàn thành đủ công việc ở công sở. Và nguyên nhân của việc này chính là sự quấy rầy.

Hãy thử nghĩ xem: khi nào bạn hoàn thành nhiều việc nhất? Nếu bạn giống với đa số mọi người, thì đó là vào buổi tối hoặc lúc sáng sớm. Đây không phải là ngẫu nhiên, mà là vào những thời điểm này, không có ai khác ở quanh bạn.

Vào lúc hai giờ chiều, mọi người thường hội họp, hoặc trả lời e-mail, hoặc trò chuyện với đồng nghiệp. Những cuộc tán gẫu này trông có vẻ vô hại, nhưng thực ra chúng là chất ăn mòn năng suất làm việc. Quấy rầy không phải là sự cộng tác; nó chỉ là sự quấy rầy mà thôi. Và khi bạn bị quấy rầy, bạn không thể hoàn thành được công việc.

Sự quấy rầy chia nhỏ ngày làm việc của bạn thành những khoảng thời gian làm việc ngắn. Cứ bốn mươi lăm phút bạn lại có một cuộc điện thoại. Sau mười lăm phút, bạn đi ăn trưa.

Một giờ sau, bạn có một cuộc họp. Trước khi bạn kịp nhận biết thì đồng hồ đã chỉ năm giờ và bạn chỉ còn vài giờ đồng hồ để hoàn tất công việc. Bạn không thể làm được việc gì ra trò khi cứ liên tục bắt đầu, ngừng lại, bắt đầu, rồi lại ngừng lại.

Thay vì như thế, bạn nên lui vào chốn riêng tư một mình. Thời gian ở một mình chính là lúc bạn làm việc có hiệu quả nhất. Khi không phải nghĩ chuyện nọ xọ chuyện kia, bạn sẽ hoàn tất được cả núi việc.

Rút lui vào chốn riêng đòi hỏi thời gian và phải tránh mọi sự quấy rầy. Việc này giống như giấc ngủ đầy mộng mị vậy. Bạn không ngả lưng xuống là nằm mơ. Bạn ngủ trước và rồi chìm dần vào giấc mộng. Bất cứ sự quấy rầy nào cũng buộc bạn bắt đầu lại từ đầu. Và lúc bạn mơ là lúc đã thật sự say giấc. Chốn riêng là nơi mà bạn dồn hết tâm trí để làm việc và lúc đó, năng suất làm việc của bạn đạt đến đỉnh cao.

Bạn có thể lập ra nguyên tắc nơi công sở là bạn sẽ có riêng nửa ngày được ở một mình. Hãy dành buổi sáng hoặc buổi chiều làm thời gian riêng tư của bạn; hoặc ấn định một ngày nào đó trong tuần là ngày yên lặng. Hãy đảm bảo khoảng thời gian đó sẽ không bị phá vỡ để tránh những quấy rầy làm ảnh hưởng đến năng suất làm việc.

Và hãy tuân thủ những gì mình đề ra. Một khoảng thời gian riêng tư thành công có nghĩa là không có những giao tiếp không mong muốn. Trong suốt thời gian ở một mình, đừng xem tin nhắn, nghe điện thoại, hay họp hành. Hãy ngậm miệng lại và tập trung làm việc. Bạn sẽ ngạc nhiên khi thấy mình làm được nhiều việc hơn đến mức nào.

Nếu như bạn giao tiếp, hãy cố gắng sử dụng các công cụ giao tiếp thụ động, không đòi hỏi bạn phải trả lời ngay lập tức (như e-mail), thay vì những công cụ quấy rầy bạn (như điện thoại và các cuộc họp). Bằng cách đó, mọi người có thể trả lời vào những lúc thích hợp đối với họ, thay vì bị ép buộc phải dẹp bỏ tất cả mọi việc ngay lập tức.

Nếu ngày làm việc của bạn bị vây hãm bởi vô số sự quấy rầy, hãy kiên quyết đấu tranh chống lại chúng.

## Họp hành là độc dược

Họp hành là độc dược. Đây là lý do tại sao:

- Những cuộc họp thường bao gồm từ ngữ và các khái niệm trừu tượng chứ không phải người thật việc thật
- Các cuộc họp thường lái sang những chuyện ngoài lề
- Hội họp đòi hỏi sự chuẩn bị chu đáo mà hầu hết mọi người không có thời gian để thực hiện
- Có quá nhiều chương trình nhồi nhét trong cuộc họp đến nỗi không ai thực sự biết rõ về mục tiêu của cuộc họp
- Trong mỗi cuộc họp thường xuất hiện ít nhất một kẻ ngớ ngẩn, người chắc chắn gây lãng phí thời gian của mọi người bằng những lời phát biểu vô nghĩa
- Họp hội có khả năng sinh sôi. Một cuộc họp sẽ dẫn đến một cuộc họp khác, rồi một cuộc họp khác nữa, và cứ như thế...

Rủi thay, các cuộc họp thường được lên lịch như chương trình truyền hình vậy. Bạn dành riêng ba mươi phút để chuẩn bị cho cuộc họp chỉ vỏn vẹn mười phút. Quá tệ! Nếu chỉ cần tốn có mười phút để đạt được mục tiêu của cuộc họp thì tốt nhất là chỉ nên dành bấy nhiêu đó thời gian thôi. Đừng kéo lê cuộc họp mười phút thành ba mươi phút.

Khi nghĩ kỹ về việc này, bạn sẽ nhận thấy cái giá thực sự của việc họp hành thật đáng kinh ngạc. Giả sửlênbạn sắp lịch một cuộc họp kéo dài một tiếng đồng hồ và bạn mời mười người tham dự. Đó thực ra là một cuộc họp mất mười tiếng đồng hồ, chứ không phải một tiếng. Bạn đang đánh đổi mười giờ năng suất làm việc để lấy một giờ hội họp. Và có lẽ là khoảng thời gian thực sự tiêu tốn là mười lăm tiếng đồng hồ, bởi vì còn phải tính đến thời gian hao hụt khi mọi người phải chuyển tâm trí mình từ việc này sang việc khác: ngừng công việc đang làm, đến một nơi nào khác để họp, rồi quay trở về với công việc dang dở trước đó.

Như vậy, việc đánh đổi mười hoặc mười lăm giờ năng suất làm việc để lấy một giờ hội họp có gọi là ổn? Có lẽ cũng thỉnh thoảng. Nhưng đó là cái giá quá đắt. Đánh giá trên nền tảng chi phí đơn thuần, những cuộc họp ở quy mô như thế nhanh chóng trở thành những món tiêu sản chứ không phải tài sản. Hãy nghĩ đến thời gian mà bạn đang thực sự đánh mất và tự vấn bản thân xem liệu nó có thực sự xứng đáng.

Nếu bạn quyết định rằng các bạn chắc chắn phải hội họp, thì hãy cố gắng làm cho cuộc họp thật hiệu quả, chứ không phải họp là hành, bằng cách tuân theo những quy luật đơn giản:

- Hãy đặt đồng hồ báo giờ; chuông reo là kết thúc
- Mời càng ít người tham dự càng tốt
- Luôn có một chương trình làm việc rõ ràng
- Hãy bắt đầu với một vấn đề cụ thể
- Họp ngay tại nơi mà vấn đề nảy sinh thay vì trong một phòng họp. Hãy nhắm vào việc thật và đề nghị những thay đổi thật
- Kết thúc cuộc họp với một giải pháp và giao nhiệm vụ cho một người cụ thể để triển khai giải pháp đó

## Vừa tốt là được

Nhiều người rất thích giải quyết vấn đề bằng các giải pháp phức tạp. Việc uốn nắn "cơ bắp trí não" có thể khiến bạn say sưa, khiến mỗi lúc bạn càng tìm những thử thách lớn hơn, bất luận nó có hiệu quả hay không.

Sao bạn không thử tìm đến giải pháp judo [[7]](#7), một giải pháp giúp bạn đạt hiệu quả tối đa với nỗ lực tối thiểu? Bạn sẽ gặt hái thành quả cao nhất với ít công sức nhất. Bất cứ khi nào gặp trở ngại, hãy tìm cách đơn giản nhất và dễ dàng nhất để vượt qua.

Mấu chốt của việc này là bạn phải hiểu rằng hầu hết các vấn đề đều có giải pháp. Giả sử thử thách của bạn là có được tầm nhìn của loài chim, nếu vậy bạn có thể nghĩ đến việc leo lên đỉnh Everest. Đó là một giải pháp đầy tham vọng. Nhưng bạn cũng có thể đi thang máy lên một cao ốc. Đó chính là giải pháp judo.

Phần lớn các vấn đề thường có những giải pháp đơn giản. Điều đó có nghĩa là chẳng có kỳ công nào cả. Bạn không cần phải thể hiện các kỹ năng đáng kinh ngạc của mình. Bạn chỉ cần tạo dựng một điều gì đó để hoàn tất công việc và rồi tiếp tục tiến tới. Bạn có thể cảm thấy những phương pháp này không thú vị, nhưng nó giúp bạn đạt được kết quả tốt hơn.

Khi cái vừa đủ tốt có thể giúp bạn hoàn thành công việc, hãy thực hiện nó. Đó là cách khôn ngoan hơn nhiều so với việc lãng phí tài nguyên. Nếu bạn không kham nổi giải pháp phức tạp, kết quả còn tệ hại hơn vì bạn sẽ chỉ giậm chân tại chỗ. Hãy ghi nhớ, bạn thường có thể biến cái vừa tốt thành cái tuyệt vời sau này.

## Tốc chiến tốc thắng

Sự tăng trưởng tiếp sức động lực. Không lên được nấc thang kế tiếp, làm sao bạn có thể đứng trên nấc thang trên cùng. Không có sự khích lệ từ những việc mình đang làm, bạn khó mà làm tốt mọi thứ.

Cách tạo ra đà tăng trưởng là hoàn thành một việc rồi chuyển sang việc kế tiếp. Chẳng ai muốn bị mắc kẹt ở một dự án bất tận không có ngày kết thúc. Bị mắc kẹt trong một hào sâu suốt chín tháng trời mà chẳng làm nên trò trống gì thì quả là khổ hình. Dần dần, nó sẽ khiến bạn kiệt sức. Để giữ ổn định đà tăng trưởng và nguồn động lực của mình, hãy tập thói quen đạt được những chiến thắng nhỏ dọc đường; thậm chí chỉ một sự cải thiện tí hon cũng có thể tạo ra đà tăng trưởng tốt.

Một việc càng kéo dài, bạn sẽ càng khó lòng hoàn tất.

Sự hào hứng đến từ việc tạo ra một thứ và để cho khách hàng đánh giá nó ngay tắp lự. Lên thực đơn cho cả năm nghe quá nhàm chán. Đưa ra ngay một thực đơn mới, phục vụ thức ăn nóng sốt và nhận lại phản hồi sẽ thú vị hơn nhiều. Vì thế, đừng đợi quá lâu; nếu không bạn sẽ dập tắt ngọn lửa nhiệt huyết của mình nếu bạn làm như vậy.

Nếu bạn thực sự phải tiến hành một dự án dài hạn, hãy cố gắng dành ra một ngày trong tuần (hoặc ít nhất là một ngày trong hai tuần) để ăn mừng những chiến thắng nhỏ và tạo ra niềm hăng hái để bước vào trận đánh tiếp theo. Ăn mừng thành công nhỏ cũng là dịp bạn loan báo những thông tin tốt đẹp. Và tất nhiên, bạn luôn muốn có một dòng chảy liên tục những thông tin tốt đẹp như thế. Khi có việc gì mới để tuyên bố sau mỗi hai tuần, bạn sẽ tiếp thêm nghị lực cho các cộng sự và mang đến sự hào hứng cho khách hàng.

Vì vậy, hãy hỏi bản thân: "Chúng ta có thể làm được gì trong vòng hai tuần?". Rồi hãy thực hiện nó. Hãy đưa sản phẩm ra thị trường để mọi người dùng thử, nếm thử, hoặc chơi thử. Sản phẩm càng đến tay các khách hàng nhanh bao nhiêu, bạn sẽ càng phấn chấn bấy nhiêu.

## Đừng làm anh hùng

Nhiều lúc, thà làm kẻ bỏ cuộc còn tốt hơn là làm anh hùng.

Ví dụ, bạn cho là mình có thể hoàn thành một công việc nào đó trong vòng hai tiếng. Nhưng sau khi bỏ ra bốn tiếng đồng hồ, bạn vẫn chỉ mới làm xong một phần tư công việc. Theo bản năng tự nhiên, bạn sẽ nghĩ: "Mình không thể nào bỏ cuộc vào lúc này. Mình dành cả bốn tiếng đồng hồ cho việc này rồi!".

Thế là bạn ứng xử theo kiểu anh hùng. Bạn quyết tâm phải làm cho bằng được (và hơi xấu hổ vì thật ra mọi thứ chẳng tiến triển gì). Bạn giam mình trong một thế giới riêng, tách biệt hẳn với mọi người.

Đôi khi, kiểu nỗ lực ấy khiến bạn cảm thấy quá tải. Nhưng hãy nghĩ xem, liệu có đáng hay không? Có lẽ là không. Công việc đáng làm khi bạn nghĩ nó chỉ tốn có hai tiếng đồng hồ, chứ không phải mười sáu tiếng. Trong mười sáu tiếng đồng hồ, bạn có thể hoàn thành một mớ công việc khác. Hơn nữa, bạn tách mình khỏi mọi người - việc này có thể khiến bạn đi sai đường. Ngay cả các anh hùng đôi khi cũng cần nghe ý kiến phản hồi từ người khác.

Chúng tôi từng trực tiếp nếm trải chuyện này. Thế nên chúng tôi cùng thống nhất nếu phải mất trên hai tuần để thực hiện một việc nào đó, chúng tôi sẽ nhờ người khác xem giúp. Có thể họ chẳng nhúng tay vào công việc, nhưng ít nhất họ có thể nhanh chóng xem xét và góp ý giúp chúng tôi. Đôi khi, giải pháp hiển hiện ngay trước mắt song bạn chẳng hề trông thấy.

Hãy nhớ, bỏ cuộc đôi lúc trở thành giải pháp tối ưu. Mọi người thường đánh đồng từ bỏ là thất bại, song đôi lúc đó chính xác là những gì mà bạn nên làm. Nếu bạn đã tiêu tốn quá nhiều thời gian vào một việc không đáng, hãy từ bỏ nó. Đương nhiên bạn không thể lấy lại khoảng thời gian đã mất, nhưng nếu bạn tiếp tục lãng phí thêm thời gian vào việc mà bạn không thể đảm đương thì càng tệ hại hơn nhiều.

## Ngủ đủ giấc

Bỏ ngủ là một ý tồi. Dĩ nhiên là bạn sẽ có thêm nhiều thời gian trống vào ngay lúc này, nhưng bạn sẽ phải trả giá đắt về sau. Bạn hủy hoại sức sáng tạo, tinh thần và sự minh mẫn của chính mình.

Thỉnh thoảng, bạn có thể thức thâu đêm nếu cần thiết. Nhưng đừng biến việc đó thành thói quen. Nếu cứ "chong mắt" đêm này sang đêm khác, bạn sẽ phải gánh lấy hàng núi hậu quả:

- Lạc lối: Khi kiệt sức, bạn sẽ chọn "bừa" một hướng đi mà chẳng cần cân nhắc thiệt hơn. Đích đến lúc đó là một ảo ảnh và rốt cục bạn sẽ lang thang vô định trong sa mạc mà không tìm lối thoát.

- Thiếu sáng tạo: Sự sáng tạo là một trong những thứ đầu tiên nói lời từ giã khi bạn mất ngủ. Điểm khác biệt giữa những người làm việc hiệu quả gấp mười lần người thường không phải là họ làm việc chăm chỉ gấp mười lần, mà là họ sử dụng óc sáng tạo để tìm ra giải pháp chỉ tốn một phần mười công sức.

- Sụt giảm tinh thần: Khi không làm việc hết công suất, não bộ chỉ thích những công việc đơn giản, có tính chất giải trí như xem tivi hay đọc báo. Sự mệt mỏi khiến bạn mất đi động lực giải quyết những vấn đề lớn lao.

- Dễ cáu kỉnh: Một khi mất hết sức lực, bạn sẽ dễ đánh mất tính kiên nhẫn và lòng vị tha. Bạn cũng sẽ dễ bực dọc, cáu gắt và ứng xử thiếu khôn ngoan. Nếu bạn gặp phải một người hay xử sự nông nổi, rất có thể người đó đang chịu chứng thiếu ngủ.

Đây chỉ là một số hậu quả mà bạn phải gánh chịu khi không ngủ đủ giấc. Thế nhưng, nhiều người vẫn còn xem nhẹ chứng thiếu ngủ. Họ thậm chí còn đi khoe khoang là mình mệt mỏi như thế nào. Đừng ấn tượng về điều đó! Họ sẽ sớm lãnh hậu quả thôi.

## Những ước lượng của bạn trật lất

Tất cả chúng ta đều là những nhà ước lượng kinh khủng. Chúng ta cho là mình biết sẽ mất bao lâu để thực hiện một việc gì đó, trong khi thực sự chẳng biết gì. Chúng ta nhìn diễn biến của mọi việc dựa trên kịch bản tươi sáng nhất, mà không lường đến bất kỳ stựrì hoãn nào. Th ực tế chẳng bao giờ đúng như vậy cả.

Đó là lý do tại sao các ước lượng trong nhiều tuần, nhiều tháng và nhiều năm chỉ là chuyện tưởng tượng. Sự thật là bạn không biết trước điều gì sẽ xảy ra trong tương lai.

Đã bao nhiêu lần bạn nghĩ ghé ngang cửa hàng tạp hóa chỉ tốn vài phút, rồi hóa ra mất đứt cả tiếng đồng hồ? Rồi bạn có nhớ việc dọn dẹp nhà kho làm mất cả ngày trời chứ không phải chỉ vài giờ như bạn nghĩ? Hoặc đôi khi mọi việc diễn ra theo chiều ngược lại, giống như khi bạn dự định dành bốn tiếng đồng hồ để làm vườn, nhưng thực tế chỉ mất có ba mươi lăm phút thôi. Con người chúng ta rất dở ước lượng.

Đối với những công việc đơn giản như thế mà chúng ta còn ước lượng sai, vậy thì làm sao chúng ta lại kỳ vọng mình sẽ chính xác khi ước lượng thời hạn của một "dự án sáu tháng"?

Thời hạn hoàn thành công việc càng lâu thì khả năng dự đoán sai lệch càng lớn. Điều đó có nghĩa là nếu bạn đoán dự án kéo dài sáu tháng, chúng tôi e là nó sẽ kéo dài bảy tháng, tám tháng, hoặc thậm chí cả năm.

Đó là lý do tại sao dự án đường cao tốc "Big Dig" của thành phố Boston trễ tiến độ đến năm năm và vượt ngân sách đến hàng tỷ đô-la, hay sân bay quốc tế Denver khai trương trễ đến mười sáu tháng và vượt ngân sách đến hai tỷ đô-la.

Giải pháp: chia việc lớn thành những việc nhỏ hơn. Việc càng nhỏ thì càng dễ ước lượng. Có thể là bạn vẫn sẽ ước lượng sai, nhưng sự sai lệch sẽ giảm nhiều so với việc ước lượng cả một dự án lớn. Nếu việc gì đó tiêu tốn gấp đôi khoảng thời gian mà bạn dự đoán thì tốt hơn là bạn nên chia nhỏ nó ra.

Thay vì dự án mười hai tuần, hãy chia nó ra thành mười hai dự án một tuần. Thay vì ước đoán những công việc tốn đến ba mươi tiếng hoặc hơn, hãy chia nhỏ thành những công việc thực tế tốn từ sáu đến mười tiếng, rồi hãy tiến hành từng bước một.

Danh mục dài ngoằng, khó lòng hoàn tất
Hãy lập danh sách những việc cần làm ngăn ngắn thôi. Các danh mục dài ngoằng thường bị bỏ xó. Lần cuối cùng bạn hoàn tất một danh mục công việc dài lê thê là khi nào? Có thể bạn sẽ giải quyết một vài công việc đầu tiên, nhưng nhiều khả năng là bạn sẽ dần dần từ bỏ nó (hoặc mù quáng cho qua những công việc chưa được thực hiện đến nơi đến chốn).

Các danh mục dài khiến bạn có cảm giác có lỗi. Danh mục những việc chưa hoàn tất càng dài thì bạn sẽ càng cảm thấy day dứt. Rồi đến một lúc nào đó, bạn sẽ chẳng ngó ngàng đến nó nữa bởi nó khiến bạn thấy mình tệ hại. Thế là bạn bị căng thẳng và tất cả mọi việc biến thành một đống hỗn độn.

Có một cách tốt hơn. Đó là chia nhỏ danh mục dài ngoằng thành một nhóm danh mục ngắn hơn. Ví dụ, hãy chia nhỏ danh mục gồm một trăm công việc ra thành mười danh mục, mỗi danh mục gồm mười công việc. Điều đó có nghĩa là khi bạn hoàn tất một công việc là bạn đã hoàn tất mười phần trăm của danh mục, thay vì chỉ một phần trăm thôi.

Vâng, bạn vẫn có cùng một khối lượng công việc phải hoàn tất. Nhưng giờ thì bạn có thể nhìn vào bức tranh nho nhỏ và tìm thấy ở đó niềm vui và động lực để hoàn tất. Điều đó tốt hơn nhiều so với việc nhìn chằm chặp vào bức tranh khổng lồ và cảm thấy sợ hãi, mất hết nhuệ khí.

Bất cứ khi nào có thể, hãy chia nhỏ các vấn đề ra cho đến khi bạn có thể giải quyết chúng nhanh gọn và triệt để. Việc sắp xếp lại công việc theo phương thức này có thể tạo ra tác động bất ngờ đối với năng suất và động lực làm việc của bạn.

Một gợi ý nhanh về việc sắp xếp thứ tự ưu tiên: đừng phân thứ bậc bằng cách đánh số hay đặt tên. Hãy tránh nói những câu như: "Đây là công việc có mức ưu tiên cao, kia là công việc có mức ưu tiên thấp". Cũng đừng nói: "Đây là công việc số ba, đây là số hai, đây là số một...". Nếu làm như thế, cuối cùng bạn sẽ có cả khối công việc được xếp vào mức ưu tiên.

Thay vì như thế, hãy sắp xếp thứ tự ưu tiên một cách trực quan. Hãy đặt việc quan trọng nhất lên đầu. Khi hoàn tất việc này thì việc tiếp theo trong danh mục sẽ thành việc quan trọng nhất. Bằng cách đó, bạn sẽ chỉ có một việc quan trọng nhất để làm tại mỗi thời điểm. Thế là đủ.

## Ra những quyết định nhỏ

Những quyết định lớn rất khó đưa ra và rất khó thay đổi. Một khi đưa ra một quyết định lớn, bạn sẽ có khuynh hướng tin tưởng mình đã quyết định đúng đắn, dù bạn biết thực tế có thể không phải như thế. Bạn không còn khách quan nữa.

Một khi bản ngã và lòng kiêu hãnh chiếm ưu thế thì bạn không thể thay đổi quyết định một cách dễ dàng. Khao khát giữ thể diện đã lấn át khao khát đưa ra quyết định đúng đắn. Chưa kể bạn sẽ tạo ra một sự trì trệ không đáng có, vì càng quyết tâm đi theo một hướng, bạn sẽ càng khó lòng thay đổi diễn tiến sự việc.

Thay vì vậy, hãy đưa ra những lựa chọn nhỏ để nó chỉ có hiệu quả tạm thời. Khi đưa ra những quyết định nhỏ thì bạn không thể phạm sai lầm lớn, đồng thời bạn cũng dễ thay đổi nếu không phù hợp. Chẳng có hậu quả gì to tát nếu bạn làm sai. Bạn chỉ cần sửa chữa mà thôi.

Đưa ra những quyết định nhỏ không có nghĩa là bạn không thể thực hiện các kế hoạch lớn và nuôi chí lớn. Điều này chỉ có nghĩa là bạn tin tưởng rằng cách tốt nhất để đạt được những điều lớn lao là lần lượt thông qua từng quyết định nhỏ.

Nhà thám hiểm người Anh Ben Saunders cho biết trong suốt cuộc thám hiểm Bắc Cực độc hành của ông (31 cuộc chạy ma-ra-tông nối tiếp nhau, 72 ngày đơn độc), "quyết định khổng lồ" quá khó suy tính đến nỗi những quyết định hằng ngày của ông thường chỉ nhắm đến việc "tiến đến tảng băng ở vài thước trước mặt".

Những mục tiêu có thể đạt được là những mục tiêu mà chúng ta nên có. Đó là những mục tiêu mà bạn có thể thực sự đạt được và gầy dựng dựa trên đó. Bạn phải nói: "Chúng ta đạt được điều đó. Thế là xong!". Rồi hãy chuyển sang công việc tiếp theo. Như thế, bạn sẽ hài lòng hơn nhiều so với việc nhắm đến một mục tiêu cao tận trời xanh mà bạn sẽ không bao giờ với tới được.


# Chương 6: Đối thủ cạnh tranh

## Đừng sao chép

Đôi khi, sao chép là một phần của quá trình học hỏi, giống như một sinh viên ngành nghệ thuật đang họa lại bức tranh trong bảo tàng, hay một tay trống đang chơi nhại theo tiếng trống sô-lô của John Bonham trong bản "Moby Dick" của ban nhạc Led Zeppelin. Khi bạn còn đi học, kiểu bắt chước này có thể là một công cụ hữu ích trên con đường khám phá tiếng nói của chính mình.

Thật không may là việc sao chép trong giới kinh doanh thường hiểm ác hơn. Có lẽ đó là vì ngày nay chúng ta đang sống trong một "thế giới cắt dán". Bạn có thể đánh cắp lời nói hoặc hình ảnh của một ai đó ngay tức khắc. Và điều đó có nghĩa là gầy dựng công ty bằng cách mô phỏng theo một mô hình sẵn có mới dễ dàng và thật hấp dẫn làm sao.

Tuy nhiên, đó là công thức dẫn đến thất bại. Khi sao chép, bạn sẽ không có sự thấu hiểu, mà sự thấu hiểu chính là cách bạn tiến triển. Bạn cần phải hiểu tại sao việc này hiệu quả và tại sao việc kia lại diễn ra như thế. Khi chỉ đơn thuần cắt dán, bạn lỡ mất điều đó. Bạn chỉ điều chỉnh lớp mặt thay vì thấu hiểu tất cả những lớp nằm bên dưới.

Bạn không nhìn thấy được công sức của người tạo ra. Nó được chôn giấu sâu bên dưới lớp mặt. Kẻ bắt chước không thể nào biết được tại sao thứ đó có hình dáng như vậy và tại sao nó phải được cảm nhận theo cách thế này. Bản sao chỉ được cái mã bên ngoài. Nó chẳng chứa đựng nội dung, không có sự thấu hiểu và cũng chẳng có nền tảng để giúp bạn đưa ra các quyết định trong tương lai.

Thêm vào đó, nếu là một kẻ sao chép, bạn sẽ không bao giờ tiến bộ được. Bạn sẽ luôn ở thế bị động, không bao giờ dẫn đầu và luôn phải đi theo kẻ khác. Bạn tạo ra những thứ lỗi thời, chỉ là một thứ hàng nhái, kém phẩm chất hơn nguyên bản. Như thế, bạn sẽ không thể nào tồn tại được.

Làm sao bạn biết liệu mình có đang sao chép ai đó hay không? Nếu người khác đang nắm phần chính yếu của công việc, tức là bạn đang sao chép. Hãy chịu ảnh hưởng, nhưng đừng đánh cắp.

## Khác biệt hóa sản phẩm

Nếu bạn thành công, người ta sẽ bắt chước những gì bạn làm. Đó là sự thật hiển nhiên trong cuộc sống. Song có một cách rất hay để bảo vệ mình khỏi những kẻ sao chép: hãy biến mình thành một phần của sản phẩm hay dịch vụ mà bạn cung cấp. Hãy đưa vào sản phẩm của bạn những chi tiết độc đáo. Hãy khác biệt hóa sản phẩm của bạn. Hãy biến nó thành sản phẩm mà không ai khác có thể làm theo [[8]](#8).

Hãy nhìn Zappos.com, thương hiệu bán lẻ giày trên mạng trị giá hàng tỷ đô-la. Một đôi giày đế mềm của Zappos cũng giống với một đôi ở cửa hàng Foot Locker hay bất cứ nhà bán lẻ nào khác. Nhưng Giám đốc điều hành Tony Hsieh [[9]](#9) của Zappos đã khác biệt hóa thương hiệu của mình bằng cách tạo dấu ấn riêng ở khâu dịch vụ khách hàng.

Tại Zappos, các nhân viên dịch vụ khách hàng không sử dụng những đoạn trả lời được thu băng sẵn mà họ còn được phép tiếp chuyện lâu với khách hàng. Tổng đài và trụ sở công ty được đặt ở cùng một địa điểm chứ không cách xa nhau. Tất cả các nhân viên của Zappos, ngay cả những người không làm ở bộ phận dịch vụ khách hàng, đều bắt đầu sự nghiệp bằng cách trải qua bốn tuần trả lời điện thoại và làm việc trong nhà kho. Chính sự tận tâm đối với khách hàng đã tạo nên nét độc đáo của Zappos so với các nhà kinh doanh mặt hàng giày khác trên thị trường.

Một ví dụ khác nữa là Polyface, một nông trại thân thiện với môi trường, do Joel Salatin làm chủ. Salatin có một niềm tin mạnh mẽ và ông điều hành công việc kinh doanh của mình dựa trên niềm tin ấy. Đó là quan tâm đến môi trường và hướng đến chất lượng thực sự chứ không chạy theo số lượng. Mặc dù làm như thế tốn kém hơn rất nhiều nhưng doanh nghiệp này vẫn cho bò ăn cỏ, chứ không cho ăn bắp và không bao giờ tiêm kháng sinh cho bò. Polyface cũng không bao giờ nhập thực phẩm từ bên ngoài. Bạn sẽ luôn được chào đón khi đến thăm nông trại bất kể lúc nào. Và khách hàng yêu thích Polyface vì tất cả điều đó. Nhiều người trong số họ thường xuyên vượt đường xa để mua được thịt "sạch" cho gia đình mình.

Hãy in đậm dấu ấn cá nhân của chính bạn vào sản phẩm và mọi thứ liên quan như phương thức bán hàng, hậu mãi và cách thức giao hàng. Các đối thủ cạnh tranh không bao giờ có thể sao chép được điều đó trong sản phẩm của bạn.

## Tuyên chiến

Nếu bạn cho rằng đối thủ cạnh tranh rất tồi thì hãy cứ nói ra. Khi làm thế, bạn sẽ thấy những người chung ý kiến với bạn sẽ đứng về phía bạn. Chống đối là cách tuyệt vời để khác biệt hóa bản thân và thu hút những người ủng hộ mình.

Ví dụ, Dunkin’ Donuts thích đối đầu với Starbucks. Các quảng cáo của hãng này đã nhạo báng việc Starbucks sử dụng tiếng nước ngoài để chỉ kích cỡ của món đồ uống, thay vì đơn thuần dùng từ "nhỏ", "vừa" và "lớn". Ngoài ra, Dunkin còn thực hiện một chiến dịch quảng cáo cho khách hàng dùng thử sản phẩm và đã đánh bại Starbucks. Thậm chí Dunkin còn có cả trang web DunkinBeatStarbucks.com, nơi đây bạn có thể gửi những tấm thiệp điện tử với các câu khẩu hiệu đại loại như "Những người bạn đích thực không để cho bạn bè mình uống Starbucks".

Audi là một ví dụ khác. Audi tuyên chiến với những chàng vệ sĩ lâu đời trong ngành sản xuất xe hơi. Những thương hiệu sang trọng lâu đời như Rolls-Royce và Mercedes đã lọt vào "tầm ngắm" của Audi qua những mẩu quảng cáo mà trong đó Audi tuyên bố hãng chính là thương hiệu đẳng cấp thay thế. Audi hạ bệ hệ thống đỗ xe tự động của Lexus bằng mẩu quảng cáo những tài xế của Audi tự biết cách đỗ xe. Một mẩu quảng cáo khác so sánh chủ sở hữu một chiếc Audi và một chiếc BMW: chủ xe BMW sử dụng kính chiếu hậu để vuốt tóc, trong khi chủ xe Audi dùng để quan sát phía sau.

Apple tấn công Microsoft bằng các mẩu quảng cáo so sánh Mac và PC, còn 7UP thì giới thiệu mình là thức uống phi Cola. Under Amour tự tạo vị thế là Nike của thế hệ mới.

Tất cả những ví dụ này thể hiện sức mạnh và phương hướng của một thương hiệu bằng cách chọn ra đối thủ và thách đấu. Còn bạn, bạn muốn tuyên chiến với ai?

Bạn thậm chí có thể tự đặt mình vào vị thế đối lập với toàn ngành. Nhãn hàng máy hong khô tay Airblade của hãng Dyson đã bắt đầu với tiền đề rằng cả ngành công nghiệp sản xuất loại máy này đều thất bại và rồi sau đó họ đã tung ra loạt sản phẩm cung cấp giải pháp nhanh chóng và hợp vệ sinh hơn. Còn sản phẩm thay thế bơ I Can’t Believe It’s Not Butter đã lồng đối thủ vào ngay trong tên sản phẩm.

Việc chiếm lấy một chỗ đứng luôn khiến bạn được chú ý. Mọi người thường cảm thấy hào hứng trước sự đối chọi và có khuynh hướng lựa chọn đứng về một bên nào đó. Cảm xúc mạnh mẽ sẽ được nhen nhóm. Và đó là cách hay để khiến mọi người chú ý đến bạn.

## Lùi một bước trong cạnh tranh

Sách lược kinh doanh trước nay đều cho rằng để đánh bại đối thủ cạnh tranh, bạn cần phải cố gắng hơn họ một bậc. Nếu họ có bốn điểm mạnh, bạn cần phải có năm (hoặc mười lăm, hoặc hai mươi lăm). Nếu họ chi 20 ngàn đô-la, bạn cần phải chi 30 ngàn. Nếu họ có năm mươi nhân viên, bạn cần có một trăm.

Cạnh tranh theo kiểu chiến tranh lạnh như thế chính là một ngõ cụt. Khi cuốn vào cuộc chạy đua vũ trang, bạn sẽ vướng vào một cuộc chiến bất tận gây tốn kém tiền bạc, thời gian và sức lực. Việc đó cũng bắt buộc bạn phải luôn ở thế phòng thủ. Những công ty phòng thủ không thể đi trước đối thủ, họ luôn đi sau.

Thế thì bạn phải làm gì đây? Hãy lùi một bước so với đối thủ để đánh thắng họ. Hãy giải quyết các vấn đề đơn giản và dành phần khó khăn phức tạp cho các đối thủ của mình. Thay vì hơn họ một bậc, hãy cố gắng kém họ một bậc. Thay vì làm nhiều hơn, hãy làm ít hơn.

Ngành kinh doanh xe đạp đưa ra một ví dụ điển hình. Trong nhiều năm, các thương hiệu xe đạp nổi tiếng tập trung vào các thiết bị công nghệ cao: những chiếc xe đạp leo núi với hệ thống giảm xóc và thắng đĩa cực mạnh, hoặc xe đạp chạy đường trường làm bằng vật liệu titanium cực nhẹ. Và người ta cho rằng xe đạp nên có nhiều bánh răng: ba, mười, hoặc hai mươi mốt bánh răng.

Nhưng gần đây, những chiếc xe đạp có bánh răng cố định đã trở nên đặc biệt phổ biến, bất chấp đây là sản phẩm công nghệ thấp nhất hiện nay. Những chiếc xe đạp xuất hiện chỉ có một bánh răng. Một vài dòng xe không có thắng. Lợi thế: đơn giản hơn, nhẹ hơn, rẻ hơn và không đòi hỏi phải bảo dưỡng nhiều.

Một ví dụ khác là máy quay phim xách tay Flip, loại máy dạng nén cực kỳ đơn giản, dễ sử dụng, chiếm lĩnh thị phần đáng kể chỉ trong một thời gian ngắn. Hãy nhìn vào tất cả những thứ mà Flip không cung cấp:

- Không có màn hình lớn (và màn hình nhỏ cũng không có chức năng tự quay)
- Không có tính năng chụp ảnh
- Không có hộc băng hoặc đĩa (bạn phải chuyển phim quay được lên máy tính)
- Không có menu
- Không có tính năng cài đặt
- Không có đèn
- Không có kính ngắm
- Không có hiệu ứng đặc biệt
- Không có chỗ gắn tai nghe
- Không có nắp đậy ống kính
- Không có thẻ nhớ
- Không có zoom quang học

Máy quay phim Flip nhận được sự ưu ái của người tiêu dùng bởi nó chỉ thực hiện một số tính năng đơn giản, nhưng thực hiện một cách xuất sắc. Máy rất dễ sử dụng và tiện lợi, có thể quay ở nhiều góc mà loại máy quay phim lớn hơn không thể đến được. Chính vì thế, máy quay Flip được những người không bao giờ dùng đến các loại máy quay phim phức tạp ưa chuộng.

Đừng xấu hổ vì sản phẩm của bạn không có nhiều tính năng. Thay vào đó, hãy làm nổi bật các tính năng mà bạn có. Hãy tự hào về nó. Hãy tự tin bán sản phẩm ít tính năng của bạn như cách các đối thủ của mình tự hào bán những tính năng mở rộng của họ vậy.

## Ai quan tâm những gì họ đang làm chứ?

Dù sao đi nữa, các đối thủ cũng chẳng đáng cho bạn bận tâm đâu. Tại sao ư? Bởi vì việc lo cạnh tranh sẽ nhanh chóng trở thành nỗi ám ảnh. Bạn sẽ đặt ra trong đầu hàng tá câu hỏi: Hiện giờ họ đang làm gì? Tiếp theo họ sẽ đi theo hướng nào? Chúng ta nên có phản ứng ra sao?

Mỗi động thái nhỏ cũng sẽ trở thành một điều cần phải phân tích. Đó là một định kiến kinh khủng. Nó dẫn đến sự căng thẳng và lo lắng cực độ. Trạng thái tinh thần như thế là một mảnh đất cằn cỗi, không thể trồng trọt được gì.

Và đó là một việc làm không có mục đích. Môi trường cạnh tranh luôn thay đổi. Đối thủ ngày mai sẽ khác hẳn so với đối thủ hôm nay. Điều đó nằm ngoài tầm kiểm soát của bạn. Thế thì lo lắng về những điều bạn không thể kiểm soát được để làm gì kia chứ?

Thay vì như thế, hãy tập trung vào bản thân mình. Những điều đang diễn ra ở đây quan trọng hơn nhiều so với những điều đang diễn ra ngoài kia. Khi mải bận tâm về người khác, bạn không thể có thời gian để cải thiện bản thân mình.

Việc chú tâm quá nhiều vào đối thủ cạnh tranh sẽ khiến bạn làm nhòe tầm nhìn của chính bạn. Bạn sẽ đánh mất cơ hội tìm ra những gì mới lạ khi cứ nạp vào đầu ý tưởng của người khác. Thế nên thay vì là một người nhìn xa trông rộng, bạn lại trở thành một kẻ bảo thủ. Kết quả là bạn sẽ cung cấp sản phẩm của đối thủ cạnh tranh dưới một lớp vỏ khác.

Nếu bạn đang lên kế hoạch sản xuất "sát thủ iPod" hoặc "Pokemon thế hệ tiếp theo" thì coi như bạn chết chắc. Bạn đang để sự cạnh tranh thiết lập hệ thống thang đo. Bạn không thể loại trừ Apple bằng cách đưa ra sản phẩm na ná như họ. Họ đặt ra luật chơi và bạn không thể nào hạ gục được người đặt luật. Bạn cần phải đặt lại luật, chứ không chỉ đơn thuần tạo ra thứ gì đó hơi tốt hơn một chút.

Đừng tự hỏi liệu bạn có đang "đánh bại" Apple (hoặc bất kỳ ông lớn nào khác trong ngành của bạn) hay không. Đó là câu hỏi sai lầm. Đây không phải là vấn đề thắng bại. Lợi nhuận và chi phí của họ là của họ. Còn của bạn là của bạn.

Nếu bạn đi theo con đường của những kẻ khác thì bạn kinh doanh làm gì? Nếu bạn chỉ làm bản sao của các đối thủ cạnh tranh, thì chẳng có lý do gì để tồn tại cả. Thậm chí dù cầm chắc thất bại thì tốt hơn bạn vẫn nên chiến đấu cho niềm tin của mình, thay vì chỉ bắt chước.


# Chương 7: Sự tiến triển

## Cứng rắn nói không

> Nếu nghe theo khách hàng, tức là tôi đã dâng cho họ con tuấn mã phi nhanh hơn.

> Henry Ford

Nói "đồng ý" thì dễ như trở bàn tay, nào là đồng ý thêm một tính năng vào sản phẩm, đồng ý gia hạn hợp đồng, đồng ý với một thiết kế tầm thường... Và rồi những thứ mà bạn đồng ý sẽ nhanh chóng chất thành một đống cao đến nỗi bạn không thể nhìn thấy những gì mình sẽ phải làm.

Hãy bắt đầu tập thói quen nói "không", thậm chí nói không với cả những ý tưởng mà bạn cho là tuyệt vời nhất. Hãy sử dụng sức mạnh của từ "không" để thể hiện rõ thứ tự ưu tiên của mình. Thực tế là chúng ta ít khi hối tiếc vì đã nói "không", nhưng lại thường hối tiếc vì đã nói "đồng ý".

Nhiều người tránh nói "không" vì sự đối đầu khiến họ cảm thấy không thoải mái. Nhưng lựa chọn thứ hai thậm chí còn tệ hại hơn. Bạn làm cho mọi thứ kéo dài lê thê, khiến chúng trở nên phức tạp và bạn phải làm việc với những điều mà mình không tin tưởng.

Giống như trong chuyện tình cảm vậy: rất khó để nói lời chia tay, nhưng cứ tiếp tục ở lại chỉ vì bạn không đủ can đảm để dứt khoát thì tình hình sẽ ngày càng tệ hơn. Hãy thẳng thắn đương đầu để tránh phải hối tiếc về lâu dài.

Đừng tin vào chuyện "khách hàng luôn luôn đúng". Giả sử bạn là đầu bếp và nếu đa số thực khách bảo rằng thức ăn quá mặn hoặc quá cay, thì bạn nên thay đổi. Còn nếu chỉ một vài người khó tính bảo thêm thật nhiều ớt vào món gỏi, hãy phớt lờ ý kiến của họ để không ảnh hưởng đến số đông. Không đáng để làm hài lòng một số khách hàng thích phàn nàn nếu việc đó phá hỏng sản phẩm của bạn trong mắt nhiều người còn lại.

ING Direct đã gầy dựng nên ngân hàng có tốc độ phát triển nhanh nhất Hoa Kỳ bằng cách nói "không". Khi khách hàng yêu cầu cấp thẻ tín dụng, câu trả lời là "không". Khi họ yêu cầu dịch vụ môi giới trực tuyến, câu trả lời là "không". Khi họ hỏi liệu họ có thể mở một tài khoản trị giá một triệu đô- la hay không, câu trả lời là "không" (ngân hàng có quy định về tiền ký quỹ tối đa rất nghiêm ngặt). ING muốn giữ cho mọi thứ thật đơn giản. Đó là lý do tại sao ngân hàng chỉ cung cấp vài tài khoản tiết kiệm và chứng chỉ tiền gửi... Chỉ thế thôi.

Mặc dù vậy, đừng là một kẻ nói "không" ngớ ngẩn. Hãy thành thật. Nếu không sẵn lòng nhượng bộ trước yêu cầu của khách hàng, bạn cần lịch sự giải thích nguyên nhân tại sao. Mọi người sẽ hiểu vấn đề khi bạn dành thời gian giải thích với họ quan điểm của mình. Thậm chí bạn còn có thể thuyết phục được họ suy nghĩ theo cách của bạn. Nếu không, hãy đề nghị khách hàng tìm đến công ty khác nếu bạn biết họ sẽ tìm được giải pháp tốt hơn ở đó. Thà để cho khách hàng vui vẻ sử dụng sản phẩm của người khác, còn hơn buộc họ sử dụng sản phẩm của bạn trong sự bất bình.

## Để khách hàng vươn cao hơn bạn

Có lẽ cảnh này không còn xa lạ với bạn: công ty cố gắng làm hài lòng một khách hàng chủ chốt bằng bất cứ cách nào có thể, thay đổi sản phẩm theo yêu cầu của vị khách hàng đó và bắt đầu bỏ bê những người còn lại.

Đến một ngày nọ, vị khách hàng lớn dứt áo ra đi, bỏ lại công ty với sản phẩm chỉ phù hợp với mỗi một người đã không còn ở đó nữa. Sản phẩm giờ đây trở thành một sản phẩm tồi đối với tất cả những người khác.

Khi bạn bám quá chặt vào khách hàng hiện tại, bất chấp tất cả khó khăn, thì kết cục là bạn sẽ tự đánh mất cơ hội có được các khách hàng mới. Sản phẩm hay dịch vụ của bạn được thiết kế chỉ để dành riêng cho các khách hàng hiện tại nên nó chẳng còn hấp dẫn đối với các khách hàng mới. Điều đó chắc chắn khiến công ty của bạn đi vào con đường phá sản.

Sau khi tung sản phẩm đầu tay ra thị trường được một thời gian, chúng tôi bắt đầu nhận được phản hồi từ công chúng, những người đã quan tâm đến chúng tôi ngay từ lúc khởi đầu.

Họ bảo họ muốn phần mềm có nhiều ứng dụng hơn. Công việc kinh doanh của họ đang thay đổi và họ muốn chúng tôi thay đổi sản phẩm của mình để phù hợp với những yêu cầu phức tạp mới xuất hiện của họ.

Chúng tôi nói không. Nguyên nhân chính là đây: chúng tôi thà để khách hàng cũ từ bỏ sản phẩm của mình, còn hơn là không thể thu hút được những khách hàng mới. Thêm các tính năng dành cho người sử dụng thành thạo để thỏa mãn một vài khách hàng có thể khiến những người chưa từng dùng thử e ngại. Làm cho khách hàng mới sợ hãi mà bỏ chạy thì còn tệ hại hơn là mất khách hàng cũ.

Hãy để cho khách hàng vươn cao hơn mình. Chẳng sao cả. Những nhu cầu cơ bản và đơn giản thì lúc nào cũng có. Có một nguồn khách hàng vô tận luôn cần những thứ đó.

Số người không sử dụng sản phẩm của bạn luôn nhiều hơn số người đang sử dụng. Hãy đảm bảo bạn giữ cho mọi thứ thật đơn giản để nhóm đa số ấy bắt đầu sử dụng sản phẩm của bạn. Tiềm năng tiếp tục phát triển của bạn nằm ngay ở đó.

Con người và hoàn cảnh đều thay đổi. Bạn không thể nào biến hóa để phù hợp với tất cả mọi người. Các công ty cần phải trung thành với một đối tượng khách hàng nhất định, hơn là cố gắng uốn mình theo các khách hàng cá nhân với nhu cầu luôn thay đổi.

## Đừng để niềm háo hức lấn át thứ tự ưu tiên

Một ý tưởng mới bao giờ cũng tạo cho bạn cảm giác thôi thúc. Bạn bắt đầu tưởng tượng ra những khả năng và lợi ích từ ý tưởng đó. Và đương nhiên là bạn muốn có ngay tất cả những thứ ấy. Thế là bạn dẹp sang một bên những việc đang làm và bắt đầu đeo đuổi ý tưởng tuyệt vời vừa xuất hiện.

Đó là một động thái tồi. Lòng háo hức của bạn dành cho ý tưởng mới không phải là thước đo chính xác giá trị xứng đáng của ý tưởng. Những thứ ngay lúc này trông có vẻ như "cầm chắc thành công" thường tuột dốc xuống thành thứ "có cũng được, không có cũng không sao" chỉ sau một đêm. Và một thứ "có cũng được, không có cũng chẳng sao" thì không đáng để bạn hoãn tất cả mọi thứ lại vì nó.

Chúng tôi luôn có những ý tưởng mới về các tính năng. Trên hết, chúng tôi cũng nhận được hàng tá ý tưởng hấp dẫn từ các khách hàng của mình mỗi ngày. Chắc chắn việc đuổi theo những ý tưởng ấy ngay lập tức cũng có cái hay. Nhưng nếu làm vậy, chúng tôi thực sự sẽ chẳng đi đến đâu cả.

Vì vậy, hãy để những ý tưởng vĩ đại của bạn nguội lại trong giây lát. Bằng mọi cách, hãy có càng nhiều ý tưởng càng tốt. Hãy cứ hào hứng với chúng, nhưng đừng hành động ngay. Hãy viết ra những ý tưởng và để đó vài ngày, rồi hãy đánh giá mức độ ưu tiên thực sự của chúng bằng cái đầu minh mẫn.

## Mang về nhà vẫn dùng tốt

Có lẽ bạn cũng từng trải qua việc này. Bạn đến cửa hàng, so sánh các sản phẩm khác nhau và chọn mua một món mà bạn cho là tốt nhất. Nó có nhiều tính năng nhất và bắt mắt nhất. Bao bì cũng rất đẹp. Trên vỏ hộp có những hình ảnh gây ấn tượng. Tất cả mọi thứ đều tuyệt vời.

Nhưng rồi khi bạn mang món hàng về nhà, nó lại không như bạn mong đợi. Sản phẩm khó sử dụng và có nhiều tính năng không cần đến. Bạn sinh ra cảm giác thất vọng. Bạn không thật sự có được cái mình cần và nhận ra mình đã chi quá nhiều tiền cho nó.

Bạn vừa mua phải một món chỉ để trưng bày. Đó là một sản phẩm khiến bạn hào hứng ngay ở cửa hàng và hụt hẫng sau khi mang về nhà sử dụng.

Các công ty khôn ngoan thì làm điều ngược lại: họ tạo ra sản phẩm khiến bạn hài lòng khi đã đem về nhà. Khi mang món hàng về và dùng thử, bạn còn thấy ấn tượng hơn về nó so với lúc ở cửa hàng. Và càng sử dụng, bạn càng cảm thấy thích nó. Lúc đó, chính bạn sẽ chia sẻ với bạn bè về sản phẩm.

Khi tạo ra một sản phẩm mang về nhà vẫn dùng tốt, có thể bạn phải hy sinh một chút sự thu hút ở cửa hàng. Một sản phẩm tốt với các tính năng cơ bản có thể kém hấp dẫn so với các sản phẩm đa tính năng của các đối thủ cạnh tranh. Sản phẩm ít tính năng sẽ không bắt mắt ngay từ xa. Điều đó cũng tốt thôi. Bạn đang nhắm đến một mối quan hệ lâu dài, chứ không phải cuộc tình một đêm.

Điều này đúng cả trong việc quảng cáo lẫn việc đóng gói hoặc trưng bày sản phẩm. Tất cả chúng ta đều đã từng xem các mẩu quảng cáo trên ti-vi về những vật dụng "mang tính cách mạng" có khả năng thay đổi cuộc đời bạn. Nhưng khi sản phẩm thật đến tay, nó khiến bạn ngỡ ngàng và hụt hẫng. Những sản phẩm chỉ hào nhoáng trên phương tiện truyền thông hoàn toàn không đáng sử dụng. Bạn không thể nào dùng các chiêu thức quảng cáo và marketing để che giấu những trải nghiệm tồi mà khách hàng đã có với sản phẩm của bạn.

## Không cần phải ghi chép

Bạn thường ghi ghi chép chép những điều khách hàng mong muốn? Đừng làm vậy. Hãy lắng nghe, nhưng rồi hãy quên đi những gì họ nói. Chúng tôi nghiêm túc đấy.

Không cần phải có cơ sở dữ liệu hoặc hệ thống lưu trữ thông tin. Những yêu cầu thực sự cấp thiết là những điều mà bạn sẽ được nghe đi nghe lại. Sau một thời gian, bạn sẽ không thể nào quên được. Các khách hàng chính là bộ nhớ của bạn. Họ sẽ liên tục nhắc nhở bạn. Họ sẽ chỉ cho bạn thấy những việc bạn thực sự nên lo nghĩ đến.

Nếu có một yêu cầu mà bạn cứ liên tục quên mất thì yêu cầu đó không quan trọng lắm đâu. Những thứ thực sự cần sẽ không bao giờ bị bỏ lỡ.


# Chương 8: Chiêu thị

## Ẩn mặt

Hiện không ai biết bạn là ai. Và như thế rất đắc lợi. Nơi không ai biết là nơi an toàn. Hãy hạnh phúc vì bạn đang ở trong bóng tối.

Lúc này, bạn có thể thử nghiệm và phạm sai lầm mà chẳng ai hay. Hãy tiếp tục tu chỉnh. Hãy tạo ra các điểm nhấn. Hãy kiểm chứng các ý tưởng ngẫu nhiên. Hãy thử những điều mới lạ. Không ai biết bạn cả, vì thế sẽ chẳng có gì to tát nếu bạn làm hỏng mọi thứ. Sự ẩn mặt giúp bảo vệ cái tôi và duy trì lòng tự tin của bạn.

Vì lý do này mà các nhà bán lẻ luôn thử nghiệm sản phẩm của mình ở các thị trường thí điểm. Chẳng hạn như khi Dunkin’ Donuts nghĩ về việc bán bánh pizza, bánh mì kẹp xúc xích và các loại bánh mì sandwich, họ thử nghiệm kinh doanh sản phẩm chỉ ở vỏn vẹn mười địa điểm.

Các chương trình biểu diễn Broadway cũng là một ví dụ tuyệt vời khi họ quyết định kiểm chứng ý tưởng ở một sân khấu nhỏ trước. Họ có thông lệ làm chương trình thí điểm ở một thành phố nhỏ hơn trước khi tiến vào New York.

Việc này cho phép các diễn viên có cơ hội diễn tập thật sự vài lần trước khi trình diễn trước những khán giả khắt khe hơn.

Khi lần đầu tiên làm một việc gì đó, liệu bạn có muốn bị cả thế giới quan sát? Nếu chưa từng nói trước công chúng, vậy bạn muốn diễn thuyết trước mười ngàn người hay chỉ mười người thôi? Hẳn bạn không muốn tất cả mọi người quan sát bạn ngay khi bạn vừa khởi sự. Chẳng có lý do gì để bảo mọi người dõi theo bạn khi bạn chưa thật sự sẵn sàng.

Và hãy nhớ một khi đã lớn mạnh và nổi tiếng hơn thì tự nhiên bạn sẽ có khuynh hướng tránh mạo hiểm. Khi bạn thành công, áp lực duy trì sự ổn định và bền vững sẽ lớn dần. Bạn sẽ trở nên bảo thủ hơn và sợ mạo hiểm hơn. Đó là khi mọi thứ bắt đầu "hóa thạch" và rất khó biến chuyển.

Nếu có hàng triệu người đang sử dụng sản phẩm của bạn, thì mỗi thay đổi mà bạn thực hiện sẽ gây ra một tác động
rất lớn. Bạn có thể thuyết phục được một trăm người, nhưng hẳn bạn phải cần đến áo giáp sắt để đối phó với mười ngàn khách hàng đang giận dữ.

Rồi sau này, khi thực sự bị soi mói đến từng chân tơ kẽ tóc, bạn sẽ thấy tiếc nhớ những ngày đầu còn ẩn mặt vô danh này. Giờ chính là lúc để mạo hiểm mà không cần lo lắng hay ngại ngùng về việc tự bêu xấu mình.

## Xây dựng lực lượng khán thính giả

Tất cả các công ty đều có khách hàng. Nhiều công ty còn may mắn có người hâm mộ. Nhưng chỉ những công ty vận đỏ nhất mới có được khán thính giả. Khán thính giả có thể là vũ khí bí mật của bạn.

Nhiều doanh nghiệp phải đổ tiền để tiếp cận mọi người. Mỗi khi muốn gửi gắm một thông điệp, họ phải móc hầu bao để thực hiện các chương trình quảng cáo. Nhưng phương pháp này vừa tốn kém, lại vừa không đáng tin cậy. Nếu làm theo cách này thì thật sự bạn đang lãng phí một nửa ngân sách cho quảng cáo.

Ngày nay, các công ty khôn ngoan nhất đã tìm ra sách lược tối ưu. Thay vì tìm đến mọi người, họ để mọi người phải tìm đến họ. Khán thính giả sẽ tự tìm hiểu xem bạn có gì mới. Đây là nhóm khách hàng dễ tiếp thu nhất và cũng là nhóm khách hàng tiềm năng mà bạn có thể có.

Trong vòng mười năm qua, chúng tôi đã gầy dựng một lực lượng khán thính giả lên đến hàng trăm ngàn người đối với trang web Signal vs. Noise Blog của chúng tôi. Mỗi ngày họ đều lướt qua trang web để xem có gì mới. Chúng tôi có thể bàn về thiết kế, việc kinh doanh, phần mềm, tâm lý học, hoặc ngành công nghiệp nói chung. Bất luận chủ đề là gì thì những khán thính giả này cũng hào hứng theo dõi. Và khi đã thích những gì chúng tôi nói thì họ cũng tỏ ra thân thiện hơn với sản phẩm của chúng tôi.

Nếu làm theo cách cũ thì chúng tôi phải chi bao nhiêu để tiếp cận được với hàng trăm ngàn người như thế? Hàng trăm ngàn đô-la hay hàng triệu đô-la? Và chúng tôi sẽ làm việc đó bằng cách nào? Chạy quảng cáo? Mua các chương trình radio? Hay gửi thư trực tiếp?

Khi có khán thính giả, bạn không cần phải bỏ tiền mua sự lưu tâm của mọi người, mà họ sẽộntgự cđhú ý đế	n bạn. Đó là một lợi thế khổng lồ.

Vì thế, hãy nói, viết, tạo blog, làm video, bất cứ điều gì. Hãy chia sẻ những thông tin giá trị và bạn chắc chắn sẽ xây dựng được lực lượng khán thính giả trung thành, dù quá trình diễn ra hơi chậm. Rồi đến khi bạn cần gửi gắm một thông điệp hay muốn nói điều gì, thì đã có sẵn những người thích hợp để lắng nghe bạn.

## Chia sẻ kiến thức

Bạn có thể làm quảng cáo, thuê nhân viên bán hàng hoặc tài trợ cho các sự kiện. Nhưng đối thủ của bạn cũng làm những việc như thế, vậy thì làm sao bạn có thể nổi trội hơn đối thủ?

Thay vì cố gắng chi nhiều hơn, bán nhiều hơn hoặc tài trợ nhiều hơn đối thủ, hãy thử chia sẻ kiến thức nhiều hơn họ. Chia sẻ là việc mà đối thủ của bạn thậm chí không hề nghĩ đến. Hầu hết các công ty đều nghĩ đến việc bán hàng hoặc cung cấp dịch vụ, chứ chẳng hề nghĩ đến chuyện sẻ chia những gì mình biết.

Công ty thiết kế đồ họa Hoefler Type Foundry dạy các nhà thiết kế về mẫu chữ tại trang web Typography.com. Etsy - một cửa hàng trực tuyến chuyên bán các đồ thủ công, tổ chức những buổi hội thảo kinh doanh phân tích các "sách lược tối ưu" và các ý tưởng chiêu thị cho những ai bán hàng trên trang này. Gary Vaynerchuk, chủ sở hữu một tiệm rượu lớn, chia sẻ trực tuyến trên Wine Library TV những hiểu biết về rượu và có hàng chục ngàn người đón xem mỗi ngày.

Hãy chia sẻ kiến thức và bạn sẽ hình thành mối dây ràng buộc mà bạn không thể nào có được nếu theo đuổi các chiến thuật marketing truyền thống. Mua được sự lưu tâm của mọi người bằng một tờ tạp chí hoặc một mẩu quảng cáo trên mạng chỉ tạo ra mối gắn kết lỏng lẻo. Thay vào đó, nếu giành được sự trung thành của khách hàng bằng cách chia sẻ kiến thức, bạn sẽ tạo nên sự gắn kết mật thiết. Họ sẽ tin tưởng và tôn trọng bạn hơn. Thậm chí, tuy không sử dụng sản phẩm của bạn nhưng họ vẫn có thể là những người hâm mộ bạn.

Chia sẻ kiến thức là việc các công ty nhỏ có thể làm mà các công ty lớn không làm được. Các công ty lớn có khả năng thực hiện các chương trình quảng cáo "khủng", còn bạn thì không. Nhưng bạn có khả năng chia sẻ kiến thức và đó là điều mà họ sẽ chẳng bao giờ làm bởi họ luôn bị ám ảnh chuyện phải giữ bí mật. Chia sẻ kiến thức chính là cơ hội để bạn vượt trội hơn họ

## Noi gương bếp trưởng

Tuy có rất nhiều bếp trưởng tài ba, nhưng tại sao khi nhắc đến các tên tuổi vĩ đại thì người ta chỉ biết đến Emeril Lagasse, Mario Batali, Bobby Flay, Julia Child, Paula Deen, Rick Bayless, hay Jacques Pépin? Bởi vì họ chia sẻ tất cả những gì họ biết. Họ sẵn sàng trình bày cặn kẽ các công thức nấu ăn thành sách và trình diễn tài nghệ trên các chương trình ẩm thực.

Với tư cách là chủ doanh nghiệp, bạn cũng cần chia sẻ tất cả những gì bạn biết. Việc này thường bị bài xích trong giới kinh doanh. Các doanh nghiệp thường kín kẽ quá mức. Họ cho rằng họ có khuôn phép này và ưu thế cạnh tranh nọ. Thật ra thì cũng có một số điều được coi là quy tắc, nhưng nhìn chung không nghiêm trọng đến nỗi phải biến mọi thứ thành tuyệt mật như thế. Đừng sợ việc chia sẻ.

Công thức nấu ăn còn dễ bắt chước hơn nhiều so với công việc kinh doanh. Nhưng việc ấy có làm cho vị đầu bếp nổi danh thế giới Mario Batali e dè? Tại sao ông lại lên ti-vi và chỉ cho mọi người cách nấu nướng? Tại sao ông viết ra tất cả các công thức của mình trong những quyển sách dạy nấu ăn mà bất cứ ai cũng có thể mua và học theo? Bởi vì Batali biết các công thức và kỹ thuật nấu nướng ấy không đủ để hạ gục ông trong trò chơi của chính ông. Sẽ chẳng ai mua sách nấu ăn ông viết, rồi mở một nhà hàng sát bên để cạnh tranh với ông. Mọi việc không diễn ra như vậy. Thế nhưng, đây lại là những gì mà nhiều người trong thế giới kinh doanh cho là sẽ xảy ra nếu các đối thủ cạnh tranh học được cách làm việc của họ. Hãy nhận thức rõ và vượt qua điều này.

Hãy noi gương những vị bếp trưởng nổi tiếng. Họ nấu nướng. Họ viết sách dạy nấu ăn. Còn bạn làm gì? Các "công thức nấu nướng" của bạn là gì? "Sách dạy nấu ăn" của bạn là gì? Bạn có thể nói gì với thế giới về cách bạn điều hành doanh nghiệp? Quyển sách này chính là "sách dạy nấu ăn" của chúng tôi. Còn của bạn đâu?

## Vén màn hậu trường

Giả như bạn sắp phải nói về cách bạn điều hành công việc trong một chương trình thực tế, bạn sẽ chia sẻ những gì?

Đừng nghĩ là chẳng ai thèm quan tâm. Hãy vén màn hậu trường để mọi người biết cách bạn điều hành công việc. Thậm chí, những công việc chán ngắt cũng trở nên cuốn hút nếu biết cách khai thác. Nghề đánh cá và lái xe tải thì có gì thú vị? Thế mà kênh truyền hình Discovery & History lại biến hai nghề này thành các chương trình truyền hình ăn khách mang tên Deadliest Catch (Chuyến ra khơi tử thần) và Ice Road Truckers (Những tay lái đường băng).

Người ta thích khám phá tất tần tật mọi thứ, cả những bí mật kinh doanh nho nhỏ, chẳng hạn bí quyết làm ra những viên kẹo dẻo be bé xinh xinh. Đó là lý do tại sao chương trình Unwrapped (Cùng khám phá) của kênh Food Network, một chương trình tiết lộ các bí mật sau những hộp thức ăn trưa, lon nước ngọt có ga, các loại kẹo và nhiều thứ khác, lại thu hút đến như vậy.

Nhiều người tò mò muốn khám phá mọi thứ. Thế nên họ háo hức muốn tham quan nhà máy hoặc thích xem đĩa quay cảnh hậu trường. Họ muốn tìm hiểu cách dàn dựng một bộ phim, cách ghi hình và chỉ đạo diễn xuất... Họ muốn biết vì sao phải làm như vậy và làm như thế nào.

Khi biết bạn đã đổ bao mồ hôi nước mắt và công sức vào sản phẩm, họ sẽ thấu hiểu và trân trọng hơn những gì bạn đang làm.

## Ai mà thích hoa nhựa chứ

Thế giới kinh doanh đầy rẫy những "chuyên gia" ăn vận bóng bẩy và cố khoác vẻ hoàn hảo. Kỳ thực, trông họ cứng nhắc và nhàm chán. Chẳng ai thích kết giao với những người như vậy.

Đừng ngại để lộ những khiếm khuyết. Có khiếm khuyết là lẽ tự nhiên và con người có khuynh hướng tương tác với những gì tự nhiên. Đó là lý do tại sao chúng ta thích những bông hoa thật dù chúng mau úa tàn, chứ không thích những bông hoa nhựa mãi mãi thắm tươi. Đừng lo lắng về lời ăn tiếng nói hay cách cư xử của mình. Hãy cho thế giới thấy bạn thực sự thích gì dù những gì bạn thích có khiếm khuyết đi chăng nữa.

Vẻ đẹp tiềm ẩn trong sự khiếm khuyết. Đây là tinh hoa trong triết lý wabi-sabi [[10]](#10) của người Nhật. Wabi-sabi đánh giá cao đặc tính và sự độc đáo vượt khỏi vẻ ngoài hào nhoáng. Triết lý này chỉ ra rằng nên trân trọng những vết nứt và vết xước trên các vật dụng. Wabi-sabi cũng nói về sự giản dị. Hãy lột bớt những gì rườm rà và sử dụng cái đang có. Leonard Koren, tác giả của một quyển sách viết về wabi-sabi, đưa ra lời khuyên: hãy tinh giảm mọi thứ cho đến khi chỉ còn phần cốt lõi, nhưng chớ lấy đi tính thơ. Hãy giữ cho mọi thứ sạch sẽ, gọn gàng nhưng đừng thanh tẩy. Nói cách khác, hãy giữ lại tính thơ trong những gì bạn làm. Khi một thứ quá bóng bẩy, nó đánh mất phần hồn và hóa cứng nhắc.

Vậy thì hãy nói những gì bạn thực sự muốn nói. Hãy khơi mở những vấn đề mà người khác không sẵn lòng thảo luận. Hãy đương đầu với những điểm yếu của mình. Hãy chia sẻ những gì bạn làm, dù còn đang dang dở. Mọi việc vẫn ổn dù chưa hoàn hảo. Có thể như thế sẽ khiến bạn rông thiếu chuyên nghiệp, nhưng bạn sẽ gây được cảm tình vì sự chân thành của chính bạn.

## Thông cáo báo chí là thư rác

Thư rác là thư được gửi hàng loạt đến nhiều người xa lạ. Thông cáo báo chí cũng giống vậy: đó là những thông tin chung chung mà bạn gửi đến hàng trăm nhà báo không quen biết. Bạn muốn cánh nhà báo chú ý đến công ty, sản phẩm, dịch vụ, thông báo mới, hay bất cứ thứ gì về công ty bạn. Bạn muốn qua thông cáo báo chí, họ thấy hứng thú viết về bạn hoặc sản phẩm của bạn.

Tuy nhiên, đăng báo bằng thông cáo báo chí là hạ sách. Nó khuôn sáo và nặng nề. Các nhà báo nhận được hàng trăm thông tin như thế mỗi ngày. Kết cục là họ bị vùi lấp bởi vô vàn những dòng tít giật gân và những lời khoa trương của các Giám đốc điều hành. Tất cả mọi thứ đều được gán cho những mỹ từ như gây xúc động mạnh, mang tính cách mạng, đột phá và gây kinh ngạc. Thật vô vị và chẳng có gì mới lạ.

Nếu bạn muốn gây chú ý thì việc hành xử giống y những kẻ khác thật ngớ ngẩn. Bạn cần phải nổi trội. Thế thì tại sao lại đăng thông cáo báo chí như cách bao người vẫn làm? Tại sao lại gửi thư rác cho các nhà báo khi mà hộp thư của họ đã ngập ngụa các loại đó rồi?

Hơn thế nữa, thông cáo báo chí có gì đặc biệt đâu. Bạn viết ra một lần và gửi hàng loạt đến vô số các phóng viên, những người bạn không quen biết và họ cũng chẳng hề biết bạn. Lời giới thiệu mở đầu của bạn cũng mập mờ và bạn sẽ đề Kính gửi Quý ông/bà một cách chung chung trong phầnKính gửi? Liệu đó có phải là ấn tượng mà bạn muốn có? Liệu bạn có tạo nên một câu chuyện thực sự hay ho và cuốn hút?

Thay vì thế, hãy gọi điện thoại hoặc viết thư riêng. Nếu bạn đọc được một bài báo về một công ty hay sản phẩm na ná như của bạn, hãy liên hệ với tác giả bài báo. Hãy lay động nhà báo ấy bằng niềm đam mê, sự hào hứng và lòng chân thành. Hãy làm một điều gì đó có ý nghĩa. Hãy tạo ra sự nổi bật và khác biệt mà người khác không thể nào quên. Đó là cách bạn gây chú ý hiệu quả nhất.

## Hãy quên tờ Wall Street

Hãy quên các tạp chí Time, Forbes, Newsweek, BusinessWeek, New York Times và Wall Street đi. Được phóng viên ở một trong những tờ báo này để mắt đến là chuyện không tưởng. May mắn lắm mới tiếp cận được họ. Mà cho dù bạn có may mắn như thế thì họ cũng chẳng ngó ngàng đến bạn. Bạn chưa đủ tầm để họ lưu tâm.

Tốt hơn là bạn nên đăng bài trên một trang tin thương mại hoặc trang blog có tiếng tăm. Như thế, rào cản bạn phải vượt qua sẽ dễ hơn nhiều. Bạn có thể gửi e-mail và nhận được phản hồi (thậm chí có thể là được đăng) vào cùng một ngày. Ở đó không có ban biên tập. Vì thế, thông điệp của bạn sẽ không phải đi qua kênh sàng lọc nào nữa.

Những anh chàng này rất đói tin nóng. Họ thích khơi dòng thị hiếu, tìm ra những điều mới lạ và khởi đầu mọi việc. Đó là lý do tại sao nhiều phóng viên kỳ cựu ngày nay lại sử dụng những trang tin nhỏ để tìm kiếm ý tưởng đề tài. Những bài viết xuất hiện ở ngoài rìa có thể đi vào dòng chảy chính rất nhanh chóng.

Chúng tôi đang đưa tin trên tờ báo lớn Wired and Time, nhưng chúng tôi phát hiện ra mình được nhiều người chú ý qua trang Daring Fireball,một trang sử dụng hệ điều hành Mac nerds, hoặc Lifehacker. Lượng truy cập đẩy vọt chủ yếu từ đường dẫn của trang này và kết quả là, doanh số của chúng tôi tăng lên một cách đáng kể. Đăng báo trên những tờ lớn thì cũng tốt thôi, nhưng kết quả không trực tiếp và nhanh chóng như thế.

## Dùng thử trước khi mua

Hãy làm cho chất lượng sản phẩm của bạn tốt đến nỗi người ta phải ước ao có nó ngay khi dùng lần đầu. Khách hàng sẽ quay lại tìm bạn với tiền cầm sẵn trên tay.

Việc này buộc bạn phải đầu tư kỹ càng về tính năng và kích cỡ thích hợp cho sản phẩm. Tiếp đó, bạn sẽ tổ chức một buổi giới thiệu dùng thử để khách hàng có cơ hội tự kiểm định sản phẩm mà không cần phải đầu tư nhiều tiền bạc hoặc thời gian.

Các hiệu bánh, nhà hàng và quán kem đã thực hiện thành công việc này từ lâu. Những tay buôn xe hơi cũng để bạn lái thử trước khi mua. Các hãng sản xuất phần mềm cũng vào cuộc với những phiên bản dùng thử miễn phí hoặc có giới hạn thời gian sử dụng. Biết bao nhiêu ngành nghề có thể hưởng lợi từ phương thức kinh doanh dùng thử trước khi mua?

Đừng e ngại việc cho không vào lúc đầu, miễn là bạn có những cái khác biệt hơn để bán. Hãy tự tin với những gì bạn cung cấp. Bạn nên biết rằng người ta sẽ quay lại để mua thêm. Nếu bạn không tự tin về điều đó tức là bạn chưa tạo ra sản phẩm đủ sức lôi cuốn.

## Không có phòng marketing

Công ty bạn có phòng marketing không? Nếu không, tốt đấy. Nếu có thì đừng nghĩ chỉ những người thuộc bộ phận này mới có trách nhiệm đối với hoạt động marketing của công ty. Phòng kế toán là một bộ phận, nhưng còn phòng marketing thì không. Marketing là việc mà tất cả mọi người thực hiện suốt hai mươi bốn giờ mỗi ngày, bảy ngày trong tuần và bốn mươi tám tuần trong năm.
Chỉ khi nào bạn không thể giao tiếp, bạn mới không thể làm marketing.

- Mỗi khi bạn trả lời điện thoại, đó là marketing.
- Mỗi khi bạn gửi e-mail, đó là marketing.
- Mỗi khi có ai sử dụng sản phẩm của bạn, đó là marketing.
- Nếu bạn viết ra các phần mềm, mỗi lời báo lỗi chính là marketing.
- Nếu bạn kinh doanh nhà hàng, viên kẹobạc hà sau bữa tối chính là marketing.
- Nếu bạn hoạt động trong ngành bán lẻ, quầy thu ngân chính là marketing.
- Nếu bạn kinh doanh dịch vụ, hóa đơn của bạn chính là marketing.

Hãy nhận ra rằng mỗi điều nhỏ bé này đều quan trọng hơn so với việc lựa chọn hàng mẫu để gói tặng phẩm. Marketing không chỉ là một vài sự kiện đơn lẻ; nó là tổng hợp tất cả những gì bạn làm.

## Thành công trong một sớm một chiều là điều không tưởng

Bạn không thể trở nên lớn mạnh ngay tức thì. Bạn không thể giàu lên nhanh chóng. Bạn không đủ đặc biệt để gây sự chú ý ngay lập tức. Chẳng ai quan tâm đến bạn cả. Ít nhất là chưa phải bây giờ. Hãy làm quen với việc ấy.

Bạn từng nghe nhiều câu chuyện thành công trong một sớm một chiều? Sự thật không hẳn là thế đâu. Hãy đào sâu vấn đề và bạn sẽ phát hiện ra người ta phải "đổ mồ hôi sôi nước mắt" trong nhiều năm trời để tìm được bãi đáp an toàn. Đối với những trường hợp hiếm hoi mà thành công đến ngay tắp lự thì thường cũng không dài lâu bởi chẳng có nền tảng nào để chống đỡ cho nó cả.

Hãy đổi ước mơ đứng trên bục vinh quang chỉ sau một đêm để lấy sự tăng trưởng chậm nhưng bền vững. Việc này rất khó, nhưng bạn phải kiên nhẫn. Bạn cần phải mài giũa công việc kinh doanh của mình và việc này mất một thời gian dài trước khi mọi người chú ý đến bạn.

Có lẽ bạn nghĩ rằng mình có thể rút ngắn thời gian bằng cách thuê một công ty PR. Đừng bận tâm làm chi. Đơn giản là bạn chưa sẵn sàng để làm thế đâu. Thứ nhất là nó quá đắt đỏ. Những công ty PR lớn có thể tốn chi phí lên đến 10.000 đô-la mỗi tháng. Đừng nên ném tiền qua cửa sổ.

Thêm nữa, giờ đây bạn vẫn còn là một công ty vô danh với sản phẩm mà chẳng ai biết đến. Ai mà quan tâm những điều đó? Một khi bạn có khách hàng và bề dày kinh doanh, bạn sẽ có khối thứ để kể, chứ còn việc mới khai trương thì chắc chắn không phải là một câu chuyện thú vị rồi.

Và hãy nhớ những thương hiệu lớn đã bắt đầu việc kinh doanh của họ mà không cần phải có các chiến dịch PR. Tất cả những tên tuổi như Starbucks, Apple, Nike, Amazon, Google và Snapple trường tồn qua năm tháng đều không phải nhờ vào một chiến dịch PR rầm rộ ngay từ đầu.

Hãy gầy dựng lực lượng khán giả của mình ngay từ hôm nay. Hãy bắt đầu khiến mọi người quan tâm đến những gì bạn nói và duy trì điều đó. Trong vòng vài năm, bản thân bạn cũng sẽ cười thầm khi mọi người bàn tán về thành công đến chỉ sau "một đêm" của bạn.


# Chương 9: Thuê mướn

## DIY (Do It Yourself): Hãy tự làm trước

Đừng bao giờ thuê người làm bất cứ việc gì mà bạn chưa thử tự làm trước. Chỉ khi tự làm, bạn mới hiểu được bản chất của công việc. Bạn sẽ nắm rõ quá trình hoàn thành tốt một việc ra sao. Bạn sẽ biết cách viết bản mô tả công việc thiết thực và nên hỏi gì trong buổi phỏng vấn. Bạn sẽ biết liệu nên thuê người làm việc toàn thời gian hay bán thời gian, nên thuê ngoài hay tự mình thực hiện (nếu có thể thì phương cách sau cùng nên được ưu tiên).

Bạn cũng sẽ trở thành một nhà quản lý giỏi bởi bạn giám sát mọi người thực hiện những việc mà bạn từng làm trước đây. Bạn sẽ biết khi nào nên phê bình và khi nào nên hỗ trợ.

Tại công ty 37signals, khi đã thiết lập hệ thống máy chủ, chúng tôi mới thuê nhân viên quản trị hệ thống. Trong ba năm đầu, chúng tôi phụ trách toàn bộ mảng hỗ trợ khách hàng, rồi sau đó mới thuê nhân viên hỗ trợ khách hàng tận tụy. Chúng tôi cố gắng hết sức theo đuổi công việc cho đến khi trao nó cho người khác. Qua đó, chúng tôi biết rõ mình cần tìm kiếm điều gì một khi quyết định thuê nhân sự.

Có thể đôi lúc bạn cảm thấy bực bội khi làm hỏng việc. Chẳng sao cả. Bạn có thể thuê người làm giúp. Song, trước khi thuê người, hãy thử học cách làm đã. Những gì bạn phải từ bỏ trong những ngày đầu sẽ được đền đáp gấp nhiều lần bởi sự khôn ngoan mà bạn có được sau quá trình rèn luyện.

Hơn nữa, hẳn bạn muốn nắm rõ tất cả các lĩnh vực trong công việc kinh doanh của mình. Không thì cuối cùng bạn sẽ phải quờ quạng trong bóng tối và đặt vận mệnh của mình vào tay kẻ khác. Điều đó vô cùng nguy hiểm.

## Thuê người khi thực sự cần

Đừng thuê người chỉ để cho vui; hãy thuê người để giải quyết khó khăn. Hãy luôn hỏi bản thân: Sẽ thế nào nếu chúng ta không phải thuê? Chúng ta có thể thay đổi phương thức làm việc hay sử dụng phần mềm để giải　quyết vấn đề? Sẽ thế nào nếu chúng ta không làm việc đó nữa?

Cũng tương tự như thế, nếu nhân viên nghỉ việc, đừng lấp đầy vị trí ngay. Hãy xem thử liệu bạn có thể duy trì được bao lâu mà không cần đến anh ta. Thường thì bạn sẽ phát hiện thật ra bạn không cần nhiều người như bạn nghĩ.

Thời điểm thích hợp thuê người là khi bạn có quá nhiều việc không thể thu xếp trong một thời gian dài hoặc có những việc mà bạn không thể nào tiếp tục đảm đương. Bạn cũng nên để ý đến chất lượng công việc. Khi chất lượng tụt dốc, đó chính là khi bạn không còn khả năng làm công việc đó nữa. Và đây là thời điểm để thuê người.

## Đừng cố giữ nhân tài một cách mù quáng

Một số công ty bị nghiện việc thuê mướn. Nhiều nơi thậm chí còn tuyển người dù không có nhu cầu. Họ nghe nói về một ai đó rất có tài và cố tạo ra một vị trí hoặc chức vụ chỉ để thuyết phục người ấy về công ty mình. Rồi vị này sẽ cứ ngồi ì ở một vị trí chẳng mấy cần thiết và làm những việc cũng chẳng mấy quan trọng cho công ty.

Đừng thuê những người mà bạn không cần, dù bạn có cho anh ta là một nhân tài hiếm có. Bạn sẽ gây hại cho công ty hơn là hỗ trợ nếu bạn nhận vào một người tài mà chẳng dùng đến.

Khi thuê dư người, rắc rối sẽ nảy sinh. Bạn bắt đầu tạo ra việc này việc kia để mọi người có việc để làm. Công việc giả tạo sẽ dẫn đến những kế hoạch giả tạo. Nhưng những kế hoạch giả tạo thì mang đến chi phí thực và sự phức tạp.

Đừng lo lắng về "những người dứt áo ra đi". Việc thuê những người không làm được việc gì thậm chí còn khiến mọi việc tồi tệ hơn nhiều. Có vô số nhân tài ở ngoài kia. Khi có nhu cầu thực sự, bạn sẽ tìm được người phù hợp.

Tài năng sẽ phí phạm nếu không dùng đến. Nếu bạn không cần người, tức là bạn không cần người, thế thôi.

## Người lạ tại tiệc rượu

Nếu bạn đến dự tiệc rượu, nơi tất cả mọi người đều không quen biết nhau, cuộc trò chuyện sẽ thật buồn tẻ và mất tự nhiên. Bạn sẽ chỉ xã giao về vấn đề thời tiết, thể thao, chương trình truyền hình... Bạn ngại đề cập đến những vấn đề nghiêm túc và các quan điểm gây tranh cãi.

Thế nhưng, một bữa tối thân mật giữa bạn bè cũ với nhau lại là chuyện hoàn toàn khác. Sẽ có những trao đổi nhiều thú vị và các cuộc tranh cãi nóng bỏng. Đến cuối buổi, bạn cảm thấy mình học hỏi được nhiều điều.

Việc nhanh chóng thuê mướn hàng tá người cùng lúc chắc chắn sẽ tạo ra "những người lạ tại tiệc rượu". Luôn luôn có những gương mặt mới, nên mọi người luôn luôn phải tỏ ra lịch sự. Ai cũng đều cố gắng tránh mâu thuẫn hoặc các cuộc tranh cãi đầy kịch tính. Chẳng ai bảo rằng "Ý tưởng này thật tồi." Mọi người đều nhân nhượng thay vì tạo ra những thử thách đề cùng vượt qua.

Và chính sự nhân nhượng đó là điều khiến cho công ty gặp rắc rối. Bạn cần phải có khả năng nói cho người khác biết khi nào thì họ tầm phào. Nếu bạn chẳng khi nào nói như vậy thì có nghĩa là những gì bạn nói không "động chạm" đến ai, nhưng đồng thời cũng không gợi được xúc cảm của ai cả.

Bạn cần có một môi trường, nơi mọi người thấy dễ dàng chia sẻ khi mọi việc trở nên căng thẳng. Bạn cần phải biết ý kiến và quan điểm của từng người.

Vì vậy, hãy cứ thảnh thơi mà thuê người. Đó là cách duy nhất để tránh bữa tiệc rượu toàn những gương mặt xa lạ.

## Sơ yếu lý lịch thật nực cười

Tất cả chúng ta đều biết các bản sơ yếu lý lịch không đáng tin cậy. Mọi thứ trong đó được cường điệu quá mức và đầy những "động từ chỉ hành động" vô nghĩa. Chúng liệt kê vô vàn những chức danh và trách nhiệm không bao giờ chính xác một trăm phần trăm. Và không có cách kiểm chứng hầu hết các thông tin đó. Tất cả đều được thêm mắm dặm muối.

Tệ hại nhất là việc tạo được một sơ yếu lý lịch như thế hết sức dễ dàng. Bất cứ ai cũng có thể tạo cho mình một bản sơ yếu lý lịch tươm tất. Đó là lý do tại sao các ứng viên hời hợt thích sơ yếu lý lịch đến thế. Họ có thể hí hoáy hàng trăm bản hệt nhau để gửi đến nhiều nhà tuyển dụng cùng một lúc. Đó là một dạng khác của thư rác. Họ không hề thực sự quan tâm đến công việc của bạn; công việc nào đối với họ cũng đều như nhau.

Nếu một người đồng thời gửi sơ yếu lý lịch đến hàng chục công ty, thì bạn nên loại anh ta ngay. Hẳn ứng viên ấy không thể nào tìm hiểu về bạn. Đối với anh ta, không có sự khác biệt giữa công ty bạn và các công ty khác.

Nếu tuyển dụng dựa trên những thứ vớ vẩn này, bạn sẽ đánh mất ý nghĩa thực sự của việc tuyển dụng. Điều bạn muốn có là một ứng viên cụ thể đặc biệt quan tâm đến công ty của bạn, sản phẩm của bạn, khách hàng của bạn và công việc của bạn.

Thế thì làm cách nào để bạn tìm ra những ứng viên này? Bước đầu tiên: kiểm tra thư xin việc. Qua thư xin việc, bạn có được cuộc trao đổi thật sự thay vì danh sách liệt kê các kỹ năng, động từ và những khoảng thời gian đâu đâu. Một ứng viên không thể nhào nặn ra hàng trăm lá thư xin việc cùng một lúc. Đó là lý do tại sao thư xin việc là một công cụ kiểm tra tốt hơn so với sơ yếu lý lịch. Bạn có thể nghe được giọng điệu thực sự của một người và có khả năng nhận ra liệu người đó có phù hợp với bạn và công ty của bạn không.

Hãy tin vào trực giác của mình. Nếu đoạn đầu tiên tệ hại, đoạn thứ hai phải bù đắp lại. Nếu cả ba đoạn đầu trong thư xin việc đều chẳng có gì nổi bật thì có lẽ đó không phải là người thích hợp. Ngược lại, nếu trực giác bảo với bạn có khả năng đây sẽ là một ứng viên đầy tiềm năng thì hãy chuyển sang giai đoạn phỏng vấn.

## Những năm tháng không liên quan

Tất cả chúng ta đều từng nhìn thấy các mẩu quảng cáo tuyển dụng ghi: "Yêu cầu 5 năm kinh nghiệm". Con số đó chẳng nói lên ý nghĩa gì cả.

Đương nhiên, yêu cầu kinh nghiệm là một ý hay. Việc ưu tiên cho những ứng viên có kinh nghiệm từ sáu tháng đến một năm rất có ý nghĩa. Đây là khoảng thời gian đủ để ứng viên tiếp thu các thuật ngữ, biết quy trình công việc, hiểu được cách sử dụng các công cụ có liên quan...

Song, thật đáng ngạc nhiên là chẳng có nhiều khác biệt giữa một ứng viên có sáu tháng kinh nghiệm và một ứng viên đã làm việc sáu năm. Điều khác biệt thực sự đến từ sự cống hiến, tính cách và trí thông minh của mỗi cá nhân.

Thế nhưng bạn đo lường tất cả mọi thứ bằng cách nào? 5 năm kinh nghiệm có ý nghĩa gì? Công ty có thể kiểm chứng những gì ứng viên hùng hồn tuyên bố? Hoàn toàn không dễ.

Khoảng thời gian người ta dành để làm một việc gì đó đã được đánh giá quá cao. Thật ra, điều quan trọng là một người đã thực hiện công việc tốt đến mức nào.

## Bớt quan tâm đến trình độ học vấn

> Đừng để trường lớp cản trở con đường giáo dục của bạn.

> Mark Twain

Có rất nhiều công ty đòi hỏi trình độ học vấn. Họ chỉ thuê những người có trình độ Đại học (đôi khi là chuyên ngành), một tấm bằng hạng ưu, điểm số trung bình nhất định, một loại chứng chỉ, hoặc một số yêu cầu khác.

Thôi nào. Có biết bao người tuy học lực không xuất sắc nhưng lại rất thông minh. Đừng để mình rơi vào cái bẫy cho rằng bạn phải tuyển được nhân sự từ những trường "điểm" thì mới chất lượng. Chín mươi phần trăm các vị Giám đốc điều hành hiện tại của năm trăm doanh nghiệp đứng đầu Hoa Kỳ không tốt nghiệp từ các trường danh tiếng. Thực ra là có nhiều người tốt nghiệp Đại học Wisconsin hơn là Harvard.

Việc cứ cắm đầu vào học có thể gây hại cho bạn. Hãy lấy kỹ năng viết làm ví dụ. Khi ra trường, bạn phải học cách gột bỏ bớt những thứ mà thầy cô dạy. Một trong những bài học sai lầm mà bạn học ở trường là:

- Văn bản càng dài càng tốt
- Văn phong trang trọng hay hơn văn phong bình dị
- Sử dụng những từ "đao to búa lớn" sẽ gây được nhiều ấn tượng
- Phải viết dài mới làm rõ được luận điểm muốn trình bày
- Hình thức của văn bản cũng quan trọng như (hoặc thậm chí còn quan trọng hơn) nội dung

Vì thế, chẳng có gì ngạc nhiên khi xuất hiện ngày càng nhiều các văn bản kinh doanh khô khan, dài ngoằng mà vô nghĩa. Mọi người cứ phát huy những thói quen xấu mà họ học được ở trường. Nhưng không chỉ có mỗi kỹ năng viết thôi đâu. Còn có rất nhiều những kỹ năng khác, tuy hữu dụng ở giảng đường nhưng lại chẳng ứng dụng được trong thực tế.

Cần biết rằng thế giới của các ứng viên tài năng rất rộng, chứ không chỉ giới hạn trong số những người tốt nghiệp đại học với điểm số trung bình hạng ưu. Hãy xét đến cả những người bỏ học giữa chừng, những người có điểm số trung bình thấp, các sinh viên tốt nghiệp cao đẳng, trung cấp và thậm chí cả những người chỉ tốt nghiệp phổ thông trung học.

## Ai cũng phải làm

Với một nhóm nhỏ thì bạn cần những người sẽ làm việc, chứ không phải những kẻ đùn đẩy công việc cho người khác. Tất cả mọi người đều phải làm việc. Không ai là ngoại lệ cả.

Điều đó có nghĩa là bạn tránh thuê những kẻ đùn đẩy, những kẻ thích sai bảo người khác. Những kẻ đó chính là gánh nặng cho nhóm. Họ cản trở những người khác bằng cách đẩy việc cho mọi người. Và khi chẳng còn gì để đùn đẩy nữa, họ sẽ tạo ra thêm, bất luận công việc có cần thiết hay không.

Những kẻ đùn việc rất thích hội họp. Những cuộc họp chính là "thiên đường" của họ, nơi mà họ cảm thấy mình là người quan trọng. Trong khi đó, công việc của những người khác bị gián đoạn khi phải họp hành liên miên như thế.

## Thuê nhà quản lý giỏi

Những nhà quản lý giỏi là những người có mục tiêu riêng và biết cách hiện thực hóa những mục tiêu đó. Họ không cần ai hướng dẫn. Họ không cần phải ngày ngày đến công ty trình diện. Họ làm những gì mà một viên quản lý sẽ làm: kiểm soát tình hình, phân công công việc, quyết định những việc cần làm..., nhưng họ tự thực hiện tất cả mọi việc với tinh thần trách nhiệm cao.

Những người như thế sẽ giúp được bạn rất nhiều. Họ định hướng cho chính bản thân họ. Khi bạn để họ làm việc một mình, họ sẽ khiến bạn ngạc nhiên với khối lượng công việc mà họ hoàn thành. Những người này không cần bạn phải cầm tay chỉ việc hay giám sát từng bước một.

Làm thế nào để định vị những người như vậy? Hãy nhìn vào những gì họ từng làm. Họ kiểm soát được tình hình ở những công việc cũ. Họ tự điều hành công việc hoặc tiến hành một dự án nào đó.

Bạn cần một người có khả năng gầy dựng nên một thứ đáng giá từ đống tạp nham và kiểm soát công việc từ đầu đến cuối. Việc tìm ra những người như thế sẽ giúp cho những người còn lại trong nhóm của bạn ít bị chi phối hơn và tập trung làm nhiều việc hơn.

## Thuê người văn hay chữ tốt

Nếu bạn đang cân nhắc không biết chọn ai giữa nhóm người có cùng khả năng, hãy chọn người có khiếu viết nhất. Bất luận chức vụ có là nhân viên marketing, nhân viên bán hàng, nhân viên thiết kế, lập trình viên, hoặc bất cứ vị trí gì đi nữa thì kỹ năng viết lách của họ cũng sẽ phát huy công dụng.

Đó là bởi kỹ năng viết tốt không chỉ gói gọn ở việc viết lách. Kỹ năng soạn thảo một văn bản rõ ràng là dấu hiệu của khả năng tư duy logic. Những người văn hay chữ tốt sẽ biết cách giao tiếp. Họ khiến cho mọi thứ trở nên dễ hiểu. Họ có thể đặt mình vào vị thế của người khác. Họ biết nên bỏ qua cái gì. Và đó chính là những phẩm chất mà bạn muốn ở một ứng viên.

Hình thức giao tiếp dạng viết đã trở lại trong xã hội của chúng ta. Hãy xem, ngày nay mọi người sử dụng e-mail và tin nhắn còn nhiều hơn nói chuyện qua điện thoại. Giao tiếp thông qua các công cụ tán gẫu, blog và các trang mạng xã hội đã trở nên phổ biến. Khả năng viết ngày nay chính là phương tiện để có được những ý tưởng hay.

## Người giỏi ở khắp nơi

Thật dại dột nếu không thuê người giỏi chỉ vì họ sống ở xa, nhất là giờ đây mọi người có thể dễ dàng đến gần nhau hơn nhờ các phương tiện công nghệ.

Trụ sở công ty của chúng tôi đặt tại Chicago, nhưng hơn một nửa đội ngũ nhân viên của chúng tôi không sống ở đó. Chúng tôi có nhân viên ở Tây Ban Nha, Canada, Idaho, Oklahoma và nhiều nơi khác. Nếu giới hạn việc tìm kiếm nhân tài chỉ ở Chicago thôi thì hẳn chúng tôi đã để mất hơn một nửa nhân viên mà chúng tôi có.

Để nhóm làm việc từ xa có thể tiện liên lạc với nhau, hãy sắp xếp một số giờ làm việc trùng nhau trong ngày. Tuy nhiên, việc này sẽ gặp đôi chút khó khăn do sự khác biệt múi giờ. Nếu rơi vào tình huống này, bạn nên thay đổi thời gian làm việc của vài người trong nhóm. Họ có thể bắt đầu làm sớm hơn hoặc muộn hơn để tất cả có một khoảng thời gian làm việc cùng lúc. Dù thế, không nhất thiết thời gian làm việc phải trùng nhau đến tám tiếng đồng hồ. Thời gian làm việc trùng nhau từ hai đến bốn giờ đã là tốt lắm rồi.

Đồng thời, thỉnh thoảng hãy tổ chức những buổi họp mặt để mọi người có cơ hội gặp nhau trực tiếp. Chúng tôi đảm bảo cho đội ngũ nhân viên của mình gặp mặt vài lần trong năm. Đây là cơ hội để kiểm tra tiến độ, thảo luận xem việc gì tốt và chưa tốt, lên kế hoạch cho tương lai, làm quen và kết thân.

Giờ thì khoảng cách địa lý đã không còn là vấn đề nữa. Hãy thuê những người giỏi nhất, bất luận họ ở nơi đâu.

## Thử việc nhân viên

Các cuộc phỏng vấn chỉ có một số giá trị nhất định mà thôi. Một số người thể hiện rất tốt ở buổi phỏng vấn, nhưng khi bắt tay vào làm thì lại không giỏi. Bạn cần phải đánh giá công việc mà họ có thể làm ở hiện tại, chứ không phải những việc mà họ đã làm trong quá khứ.

Cách đánh giá tốt nhất là quan sát họ làm việc. Hãy thuê họ làm một dự án nhỏ, thậm chí dù dự án chỉ mất hai mươi hay bốn mươi giờ. Bạn sẽ thấy cách họ ra quyết định. Bạn sẽ thấy liệu họ có hợp với bạn hay không. Bạn sẽ thấy những kiểu câu hỏi mà họ đưa ra. Bạn sẽ phải đánh giá họ dựa trên hành động, chứ không chỉ dựa trên lời nói.

Thậm chí bạn có thể tạo ra một dự án giả. Tại một nhà máy ở bang Nam Carolina, hãng BMW đã xây một dây chuyền lắp ráp mô phỏng, nơi mà các ứng viên có chín mươi giờ để thực hiện các công việc khác nhau có liên quan đến vị trí mà họ ứng tuyển.

Hãng sản xuất máy bay Cessna có một bài thực tập mô phỏng một ngày làm việc của giám đốc điều hành dành cho những vị giám đốc tương lai. Các ứng viên đàm phán, ký kết hợp đồng, đối phó với các khách hàng giận dữ và giải quyết các vấn đề khác. Cessna đã tuyển hàng trăm nhân viên bằng phương thức trên.

Những công ty này nhận ra rằng khi người ta bước vào môi trường làm việc thực tế thì sự thật sẽ phơi bày. Việc kiểm tra hồ sơ, đọc sơ yếu lý lịch hoặc tiến hành phỏng vấn là một chuyện, còn thực sự làm việc với nhau có hiệu quả hay không lại là chuyện khác.


# Chương 10: Kiểm soát thiệt hại

## Làm chủ tình huống xấu

Tốt hơn là bạn nên trình bày ngay nếu mọi việc diễn ra theo chiều hướng xấu. Nếu không, những tin đồn và các thông tin sai lệch sẽ lan truyền nhanh chóng.

Khi có điều gì tồi tệ xảy ra, hãy báo cho khách hàng của mình (thậm chí dù họ chưa hề nhận thấy). Đừng nghĩ bạn có thể che giấu. Bạn không làm thế được đâu. Nếu bạn không tự giác nói ra thì cũng sẽ có người làm việc đó thôi. Họ sẽ đưa thông tin lên mạng và rốt cục, tất cả mọi người đều biết. Chẳng còn gì là bí mật nữa cả.

Mọi người sẽ tôn trọng bạn hơn nếu bạn cởi mở, chân thật, công khai và có tinh thần trách nhiệm trong cơn khủng hoảng. Đừng cố biện minh hoặc giấu giếm thông tin bất lợi. Bạn nên để khách hàng biết đầy đủ thông tin.

Hồi năm 1989, tàu chở dầu của hãng Exxon Valdez đã làm tràn hơn 41 triệu lít dầu ra vùng biển Prince William Sound thuộc Alaska. Hãng Exxon đã phạm phải sai lầm khi chậm trễ nhận lãnh trách nhiệm và gửi các đội hỗ trợ đến Alaska. Mãi hai tuần sau vụ việc, chủ tịch hãng Exxon mới đến đó. Exxon đã tổ chức buổi họp báo ở tận Valdez, một tỉnh thuộc vùng sâu vùng xa của Alaska, khiến các nhà báo khó lòng đến tham dự. Sự kiện đã trở thành một thảm họa về quan hệ công chúng khi dư luận đều cho rằng hãng đang che đậy chuyện xấu, đồng thời chẳng hề quan tâm đến những gì mình đã gây ra.

Hãy thử so sánh câu chuyện của Exxon với vụ rò rỉ bể chứa dầu của hãng Ashland làm tràn dầu ra con sông gần Pittsburgh cũng vào cùng thời điểm. Chủ tịch hãng Ashland, John Hall, đã đến tận hiện trường để phụ trách giải quyết sự cố. Ông đảm bảo sẽ dọn sạch tất cả mọi thứ. Ông đến từng tòa soạn báo để giãi bày những gì công ty sẽ làm và trả lời các câu chất vấn. Chỉ trong vòng một ngày, ông đã xoay chuyển được tình thế, biến câu chuyện về một công ty gây hại môi trường thành câu chuyện về một công ty đang cố gắng làm sạch môi trường.
Một vài bí quyết về cách làm chủ tình huống:

- Thông điệp phải đến từ ban lãnh đạo.
Người có vị trí cao nhất phải kiểm soát tình huống một cách thuyết phục.
- Hãy để thông điệp lan truyền đi xa. Hãy sử dụng bất cứ phương tiện truyền thông nào có thể. Đừng cố che giấu sự việc.
- "Miễn bình luận" không phải là một lựa chọn.
- Hãy xin lỗi chân thành và giải thích thật chi tiết những gì đã xảy ra.
- Hãy thành thật quan tâm đến khách hàng và hãy chứng minh điều đó.

## Tốc độ thay đổi mọi thứ

"Cuộc gọi của quý khách rất quan trọng đối với chúng tôi. Chúng tôi hết sức trân trọng sự kiên nhẫn của quý khách. Thời gian chờ máy lúc này là mười sáu phút." Xin làm ơn tha cho tôi.

Khi nói đến dịch vụ khách hàng thì việc nhanh chóng tiếp chuyện với khách hàng có lẽ là việc quan trọng nhất mà bạn có thể làm. Bạn sẽ ngạc nhiên khi biết rằng việc này có thể xoa dịu một tình huống bất lợi và biến nó thành một tình huống có lợi như thế nào đâu.

Bạn đã bao giờ gửi một e-mail và phải chờ mấy ngày trời hoặc thậm chí là mấy tuần lễ mới nhận được thư trả lời của công ty? Việc đó khiến bạn cảm thấy thế nào? Giờ đây mọi người đều đã quen dần chuyện đó. Họ đã quen với việc phải chờ đợi. Họ đã quen với những lời cam kết "chăm sóc" suông một cách tẻ nhạt.

Đó là lý do tại sao quá nhiều người yêu cầu giúp đỡ lại bắt đầu bằng giọng cự nự. Một số thậm chí còn đe dọa hoặc xưng hô thiếu nhã nhặn. Họ không có ác ý gì đâu, họ chỉ đơn thuần nghĩ như vậy họ sẽ được lắng nghe. Họ chỉ đang cố tình gắt gỏng một chút với hy vọng sẽ khiến cho công việc trôi chảy hơn.

Một khi bạn giải đáp nhanh chóng, họ sẽ thay đổi 180 độ. Họ vui vẻ trở lại và trở nên cực kỳ lịch sự. Thường thì họ sẽ không ngớt lời cảm ơn bạn.

Điều này đặc biệt đúng khi bạn giải đáp thắc mắc của họ bằng lòng nhiệt thành của chính bạn. Khách hàng đã quá quen với những câu trả lời khuôn sáo, vì vậy bạn có thể thực sự khác biệt hóa bản thân bằng cách trả lời họ một cách cặn kẽ và thể hiện bạn đang lắng nghe. Và thậm chí dù bạn không có câu trả lời hoàn hảo thì cũng hãy nói một điều gì đó. Câu "Hãy để tôi tìm hiểu thêm và gọi lại cho anh sau" là một câu trả lời tuyệt vời.

# Nói lời xin lỗi

Một trong những cách xin lỗi tệ hại nhất là xin lỗi mà không nhận lỗi. Cách xin lỗi này nghe giống như một lời xin lỗi nhưng thật ra lại không nhận lãnh trách nhiệm, ví dụ: "Chúng tôi xin lỗi nếu việc này làm quý vị bực mình." hoặc "Chúng tôi xin lỗi nếu chúng tôi không được như mong đợi của quý vị".

Một lời xin lỗi đúng nghĩa phải bao gồm cả việc nhận lãnh trách nhiệm. Nó không chứa mệnh đề "nếu...". Nó phải thể hiện cho mọi người thấy bạn hoàn toàn nhận trách nhiệm, và rồi cung cấp thật chi tiết về những gì đã xảy ra và những gì bạn đã làm, cam kết không để sự việc tái diễn. Lời xin lỗi tìm kiếm một phương cách để sửa chữa sai lầm.

Đây là một lời xin lỗi tệ hại khác: "Chúng tôi xin lỗi vì sự bất tiện mà việc này có thể đã gây ra." Chúng ta hãy cùng làm rõ tại sao nó lại tệ hại.

"Chúng tôi xin lỗi..." Nếu bạn làm đổ cà phê lên người khác trong xe buýt thì liệu bạn có thốt ra chỉ vỏn vẹn ba từ "Tôi xin lỗi"? Không, bạn sẽ phải nói: "Tôi vô cùng xin lỗi! Tôi vô ý quá." Nếu dịch vụ của bạn cực kỳ quan trọng đối với khách hàng thì việc gián đoạn trong cung cấp dịch vụ cũng giống như làm đổ cà phê lên người khác vậy. Cho nên hãy dùng giọng điệu và lời lẽ thích hợp để thể hiện bạn thật sự thấu hiểu mức độ nghiêm trọng của những gì vừa diễn ra. Đồng thời, người phụ trách phải nhận lãnh trách nhiệm cá nhân. Câu "Tôi xin lỗi" có tác động mạnh hơn nhiều so với câu "Chúng tôi xin lỗi." 

"... sự bất tiện..." Nếu khách hàng phụ thuộc vào dịch vụ của bạn và không thể sử dụng dịch vụ thì đó không chỉ là sự bất tiện. Đó là một cơn khủng hoảng. Sự bất tiện có thể là việc phải xếp hàng dài dằng dặc ở siêu thị. Còn ở đây rõ ràng không phải là sự bất tiện.

"... mà việc này có thể đã gây ra". Chữ "có thể" ở đây hàm ý cũng có khả năng là chẳng có gì bất ổn cả. Đó là một động thái xin lỗi mà không nhận lỗi. Nó coi nhẹ vấn đề thực sự mà khách hàng đang gặp phải. Nếu như sự việc không ảnh hưởng đến họ, bạn ắt hẳn không cần phải nói gì. Còn nếu có thì không cần phải sử dụng từ "có thể" ở đây. Hãy bỏ ngay kiểu nói nước đôi ấy đi.

Thế thì đâu là cách hoàn hảo để nói lời xin lỗi? Chẳng có đâu. Bất cứ câu trả lời khuôn sáo nào nghe cũng thật nhàm chán và rỗng tuếch. Bạn sẽ phải xem xét từng trường hợp để ứng xử cho phù hợp.

Nguyên tắc cần ghi nhớ khi nói lời xin lỗi: nếu bạn là người nhận lời xin lỗi, bạn sẽ cảm thấy thế nào về lời xin lỗi mình sắp nói? Nếu ai đó nói những lời ấy với bạn thì liệu bạn có tin họ không?

Hãy nhớ, bạn không thể xin lỗi để lấp liếm lỗi lầm của mình. Thậm chí dù lời xin lỗi hay ho nhất cũng sẽ không cứu được bạn nếu bạn không giành được lòng tin của mọi người. Tuy nhiên, nếu bạn xây dựng quan hệ tốt đẹp với khách hàng, họ sẽ bớt khắt hơn với bạn và tin tưởng bạn khi bạn nói lời xin lỗi.

## Đặt mọi người ở đầu chiến tuyến

Trong kinh doanh ẩm thực, có sự khác biệt rất lớn giữa khâu nấu nướng và khâu phục vụ. Các trường dạy nấu ăn và chủ nhà hàng biết việc nhân viên của cả hai khâu thấu hiểu và thông cảm lẫn nhau là điều cực kỳ quan trọng. Đó là lý do tại sao họ thường để các bếp trưởng ra phục vụ bàn để củng cố việc nấu nướng. Bằng cách đó, các nhân viên làm việc trong bếp có thể trao đổi với thực khách và hiểu được mọi việc thực sự diễn ra như thế nào ở đầu chiến tuyến ngoài kia.

Tương tự, rất nhiều công ty có sự phân chia rõ ràng giữa "tiền tuyến" và "hậu phương". Những người tạo ra sản phẩm thì làm việc "trong bếp", trong khi các bộ phận hỗ trợ kinh doanh thì làm việc với khách hàng. Thật không may, điều đó có nghĩa là vị "bếp trưởng" sẽ chẳng bao giờ có cơ hội trực tiếp lắng nghe những gì mà khách hàng nói. Như thế thật chẳng hay chút nào. Lắng nghe khách hàng là cách tốt nhất để nắm bắt được thế mạnh điểm yếu của sản phẩm.

Hãy nghĩ đến một trò chơi kiểu "tam sao thất bản" của trẻ con. Có mười đứa bé ngồi thành một vòng tròn. Bé thứ nhất sẽ thầm thì một câu vào tai bé ngồi cạnh và bé này sẽ lặp lại câu đó với bé tiếp theo. Câu nói cứ thế truyền đến bé cuối cùng. Lúc này, thông tin đã hoàn toàn bị bóp méo đến nỗi nghe rất tức cười. Một câu vốn dĩ ban đầu có nghĩa, khi đi hết vòng tròn lại thành ra "Mì ống dưa đỏ biết về tương lai". Và càng có nhiều người chơi thì thông điệp sẽ càng bị nhiễu.

Mọi việc cũng tương tự như thế đối với công ty của bạn. Càng thông qua nhiều trung gian thì thông tin truyền từ khách hàng càng bị sai lệch.

Tất cả mọi người trong nhóm của bạn nên được kết nối với khách hàng, có thể là không phải hàng ngày, nhưng ít nhất cũng phải vài lần mỗi năm. Đó là cách duy nhất để tất cả các thành viên có thể cảm nhận được những điều phiền lòng của khách hàng. Việc cảm nhận đó sẽ thúc đẩy mọi người tiến hành giải quyết vấn đề. Và việc này có tác dụng hai chiều: sự hài lòng của khách hàng trở thành nguồn động lực mạnh mẽ cho đội ngũ nhân viên.

Do vậy, đừng cản trở những người trực tiếp làm ra sản phẩm nhận phản hồi từ khách hàng. Đừng nên che chắn ai trước sự phê bình.

Có thể bạn cho là mình không có thời gian để trao đổi với khách hàng. Thế thì hãy tạo ra thời gian. Nhà sáng lập trang Craigslist, Craig Newmark, hiện nay vẫn trả lời các e- mail hỗ trợ khách hàng (thường chỉ trong vài phút). Ông cũng xóa những lời bình mang tính phân biệt chủng tộc trên các diễn đàn thảo luận của trang web và phản đối các công ty địa ốc ở thành phố New York vì đã đưa thông tin ảo về các căn hộ cho thuê. Nếu ông có thể cống hiến hết lòng cho dịch vụ khách hàng như thế thì bạn cũng có thể.

## Tĩnh tâm

Mặt hồ bị đánh động đương nhiên sẽ dậy sóng. Sau khi bạn giới thiệu một tính năng mới, thay đổi một chính sách hoặc loại bỏ một điều gì đó, ít nhiều sẽ xuất hiện phản ứng. Hãy cố gắng bình tâm để tránh cảm giác hoảng loạn vừa xuất hiện. Những cảm xúc mạnh mẽ luôn lóe sáng vào lúc đầu. Đó là điều hoàn toàn bình thường. Nếu bạn vượt qua tuần lễ đầu tiên đầy sóng gió, mọi việc thường sẽ dần bình ổn trở lại.

Con người là sinh vật có thói quen. Đó là lý do tại sao mọi người thường phản ứng tiêu cực trước sự thay đổi. Bất cứ sự thay đổi nào cũng làm rối trật tự vốn có của mọi thứ.

Chính vì thế mà họ kháng cự. Họ phàn nàn. Họ yêu cầu bạn phải trả lại tình trạng nguyên sơ của những thứ mà họ đã quen dùng.

Nhưng điều đó không có nghĩa là bạn phải làm theo những gì họ muốn. Đôi khi, bạn phải tiếp tục kiên trì với quyết định mà bạn tin tưởng, thậm chí dù quyết định này không được số đông ủng hộ lúc ban đầu.

Phản ứng tiêu cực tức thì là phản ứng cơ bản. Và thường các phản ứng tiêu cực mạnh mẽ hơn so với các phản ứng tích cực. Nhưng bạn đừng quá quan tâm đến việc này bởi thông thường, bạn chỉ để ý đến những lời phàn nàn nên không nghe thấy những tiếng hân hoan trước sự thay đổi của bạn. Đừng khờ dại rút lui khỏi một quyết định gây tranh cãi nhưng rất cần thiết.

Hãy để cho mọi thứ có thời gian lắng dịu lại. Hãy cho mọi người biết bạn đang lắng nghe họ. Hãy cho thấy bạn đồng cảm những gì họ đang nói. Hãy để họ biết bạn thấu hiểu sự bất mãn của họ. Nhưng hãy giải thích với họ rằng bạn sẽ để cho mọi việc tiếp tục diễn ra để xem tình hình như thế nào. Có thể bạn sẽ nhận ra mọi người dần dần cũng thích nghi được. Và một khi đã quen với cách mới, họ thậm chí có thể thích hơn cả cách cũ.


# Chương 11: Văn hóa

## Bạn không tạo ra văn hóa

Văn hóa được hình thành ngay tức thời là nền văn hóa nhân tạo. Văn hóa nhân tạo được tạo ra từ những lời tuyên bố và các luật lệ rạch ròi, ngớ ngẩn và sáo rỗng. Văn hóa nhân tạo chỉ là lớp sơn. Văn hóa thật mới là bề mặt sáng bóng của gỗ.

Bạn không tạo ra văn hóa. Nó tự hình thành. Đây là lý do tại sao các công ty mới chưa tạo dựng được văn hóa riêng. Văn hóa là phụ phẩm của hành vi nhất quán. Nếu bạn khuyến khích mọi người chia sẻ thì sự chia sẻ được hình thành trong nền văn hóa của công ty bạn. Nếu bạn đánh giá cao sự tin tưởng và đối xử với khách hàng đúng mực thì cả hai sẽ trở thành một phần trong nền văn hóa của bạn.

Văn hóa không phải là trò Trust Fall [[11]](#11). Nó không phải là một chính sách. Nó cũng không phải là bữa tiệc Giáng sinh, hay là chuyến dã ngoại của công ty. Những thứ ấy là các vật thể và sự kiện, không phải là văn hóa. Và văn hóa cũng chẳng phải là câu khẩu hiệu. Văn hóa là hành động chứ không phải những lời hô hào.

Vì vậy, đừng quá lo lắng về nó. Đừng cố bức ép nó. Bạn không thể cài đặt một nền văn hóa. Giống như loại rượu vang thượng hạng vậy, bạn phải cho nó thời gian để lên men đúng độ.

## Quyết định chỉ là tạm thời

"Nhưng sẽ thế nào nếu...?" "Điều gì sẽ xảy ra khi...?" "Liệu chúng ta có cần lên kế hoạch cho...?"

Đừng tạo ra vấn đề mà bạn còn chưa thấy. Chẳng có gì là vấn đề cho đến khi chúng thực sự hiện hữu. Dù sao thì hầu hết những điều mà bạn lo lắng cũng chẳng bao giờ xảy ra.

Bên cạnh đó, những quyết định mà bạn đưa ra hôm nay cũng không nhất thiết phải tồn tại mãi. Bạn rất dễ để	lỡ
mất những ý tưởng hay, các chính sách thú vị hoặc các trải nghiệm đáng giá khi cho rằng bất cứ điều gì mà bạn quyết định hôm nay sẽ tiếp tục đúng đắn trong nhiều năm liền. Sự thật không phải thế đâu, nhất là đối với một doanh nghiệp nhỏ. Các quyết định chỉ là tạm thời mà thôi.

Ở giai đoạn này, thật ngốc nghếch khi bỏ công lo lắng liệu việc kinh doanh của bạn có gia tăng quy mô từ năm người lên năm ngàn người hay không (hoặc từ một trăm ngàn lên một trăm triệu người). Đưa một sản phẩm hay dịch vụ ra thị trường thành công đã đủ khó khăn lắm rồi, không cần phải tạo ra nhiều trở ngại hơn.

Khả năng thay đổi phương hướng là một trong những ưu thế của doanh nghiệp nhỏ. So với các đối thủ cạnh tranh lớn hơn, bạn có nhiều khả năng hơn trong việc tạo nên những thay đổi nhanh chóng, có ảnh hưởng sâu rộng. Các công ty lớn không thể chuyển mình nhanh như thế. Vì vậy, hãy tập trung vào hiện tại trước và lo lắng về tương lai sau. Nếu không thì bạn sẽ lãng phí năng lượng, thời gian và tiền của để bận tâm về những vấn đề phi thực tế.

## Hãy bỏ qua các "ông sao"

Rất nhiều công ty đăng quảng cáo tìm kiếm các "ngôi sao nhạc rock" hay "ninjas" (chỉ những tay lập trình viên tài năng). Thật chẳng ra làm sao cả. Trừ phi công ty của bạn đầy những kẻ hâm mộ cuồng nhiệt hoặc đầy rẫy phi tiêu, còn nếu không thì những từ ấy chẳng liên quan gì đến việc kinh doanh của bạn.

Thay vì nghĩ về việc làm thế nào thu hút những lập trình viên tài năng về công ty mình, bạn hãy nghĩ về môi trường làm việc trước đã. Môi trường có liên quan đến kết quả công việc nhiều hơn là mọi người vẫn tưởng.

Nói như thế không có nghĩa là khẳng định tất cả mọi người sinh ra đều giống nhau và bạn sẽ khai mở được tài năng tiềm ẩn ở họ bằng môi trường làm việc tốt. Nhưng có rất nhiều những tiềm năng còn mắc kẹt giữa đống chính sách vớ vẩn, phương hướng lệch lạc và các thủ tục ngột ngạt. Hãy bỏ bớt những thứ vớ vẩn và ban sẽ phát hiện ra rằng mọi người đang chờ thể hiện khả năng hoàn thành xuất sắc công việc. Họ chỉ cần được cho cơ hội mà thôi.

Việc này không phải là về ngày thứ Sáu ăn mặc tự do, hay một ngày mà bạn có thể thoải mái mang thú cưng đến sở làm. (Nếu đó là những ý tưởng tuyệt vời thì tại sao bạn lại không thực hiện mỗi ngày, đúng không nào?)
Môi trường làm việc tuyệt vời, trong đó mọi người có sự riêng tư, không gian thoải mái và các công cụ thiết bị mà họ xứng đáng có được, sẽ giúp phát triển lòng tin, sự tự chủ và trách nhiệm của từng người. Tạo dựng một môi trường làm việc tuyệt vời là thể hiện sự tôn trọng đối với những người đang làm việc và cách họ thực hiện công việc.

## Họ đâu phải là con nít

Khi đối xử và xem mọi người như trẻ con, bạn sẽ nhận được kết quả làm việc của trẻ con. Thế nhưng đó chính xác là cách mà nhiều công ty và nhiều vị giám đốc đối xử với nhân viên của mình. Các nhân viên cần phải xin phép trước khi họ làm bất cứ điều gì. Họ cần phải được sự đồng ý của cấp trên đối với bất cứ khoản chi tiêu nhỏ nhặt nào. Cũng may là họ không cần phải xin giấy phép mỗi lần đi vệ sinh.

Khi mọi thứ luôn cần phải được phê duyệt, bạn tạo ra một nền văn hóa mà trong đó nhân viên không cần phải động não. Bạn tạo ra một mối quan hệ đối đầu giữa sếp và nhân viên với thông điệp hiện lên rành rành: "Tôi không tin tưởng các người".

Bạn sẽ đạt được điều gì nếu cấm nhân viên truy cập vào các trang mạng xã hội hoặc xem YouTube trong giờ làm việc? Bạn chẳng đạt được gì cả. Khoảng thời gian đó sẽ không hóa thành thời gian làm việc đâu. Rồi họ sẽ tìm được trò tiêu khiển khác thôi.

Và hãy xem nào, bạn sẽ chẳng thể nào lấy được trọn vẹn tám giờ đồng hồ mỗi ngày từ mọi người. Đó là chuyện hoang đường. Họ có thể ngồi ở văn phòng tám giờ đồng hồ, nhưng họ đâu thực sự làm việc suốt tám giờ. Mọi người cần được giải trí. Thư giãn và giải trí giúp phá vỡ sự đơn điệu của một ngày làm việc. Chút ít thời gian lướt qua các trang như YouTube hay Facebook chẳng hại gì đến ai.

Chưa kể đến việc bạn phải tiêu tốn cả đống tiền bạc và thời gian để đưa ra chính sách cho việc này. Tốn bao nhiêu chi phí để cài đặt phần mềm giám sát? Tốn bao nhiêu thời gian để các nhân viên IT giám sát lẫn nhau thay vì tập trung vào một dự án thực sự có giá trị? Bạn lãng phí bao nhiêu thời gian để viết ra những quyển luật lệ của công ty mà chẳng ai thèm đọc? Hãy nhìn vào những khoản phí tổn đó và bạn sẽ nhanh chóng nhận ra việc không tin tưởng nhân viên sẽ gây tổn thất lớn.

Để mọi người về nhà lúc năm giờ chiều
Mẫu nhân viên mơ ước của nhiều công ty là người ở độ tuổi hơn đôi mươi và có cuộc sống riêng tư ngoài công việc càng ít càng tốt, người có thể làm việc mười bốn giờ mỗi ngày và ngủ ngay tại bàn làm việc.

Nhưng việc tuyển toàn những người có thể chong đèn thức thâu đêm để làm việc như thế thật ra có gì hay đâu. Nó khiến chất lượng công việc sa sút. Nó duy trì những chuyện hoang đường kiểu như: "Đây là cách duy nhất để chúng ta có thể cạnh tranh với các đối thủ lớn". Thật ra, bạn không cần phải làm việc nhiều giờ, mà cái bạn cần là giờ làm việc có chất lượng.

Khi bận bịu việc nhà, mọi người sẽ tập trung giải quyết nhanh công việc để được về sớm. Họ tìm ra những cách làm việc hiệu quả bởi họ bắt buộc phải như vậy. Sau khi tan sở, họ cần phải đi đón con hoặc gặp gỡ bạn bè. Thế nên họ sử dụng thời gian một cách khôn ngoan để có thể làm được nhiều chuyện.

Giống như câu ngạn ngữ: "Nếu bạn muốn hoàn tất nhanh một việc, hãy hỏi người bận rộn nhất". Bạn cần những người bận rộn, những người có cuộc sống cá nhân ngoài công việc, những người quan tâm đến nhiều việc khác nhau. Bạn không nên trông mong công việc là tất cả cuộc sống của một ai đó, ít nhất là đừng làm vậy nếu bạn muốn giữ họ ở lại công ty lâu dài.

## Đừng tạo sẹo ngay từ vết cắt đầu tiên

Ngay khi xảy ra việc ngoài ý muốn, khuynh hướng tự nhiên là lập ra chính sách. "Nếu ai đó mặc quần soóc đi làm, chúng ta cần có quy định về trang phục!" Không, bạn không cần phải thế. Bạn chỉ cần bảo nhân viên ấy đừng mặc quần soóc nữa là xong.
Các chính sách là những vết sẹo của tổ chức. Chúng hệ thống hóa thái quá những tình huống hiếm khi lặp lại. Đó là hình phạt tập thể dành cho hành động không phải của một cá nhân.

Chẳng ai có nhiệm vụ tạo ra chính sách cả. Từng quy định một từ từ tiến vào các công ty và dần hình thành nên chính sách. Mỗi một chính sách là một vết sẹo.

Thế nên đừng tạo sẹo ngay từ vết cắt đầu tiên. Đừng tạo ra chính sách bởi lỗi lầm của một người vừa sai phạm lần đầu. Các chính sách chỉ có ý nghĩa đối với những tình huống cứ lặp đi lặp lại.

## Hãy là mình

Những doanh nhân ra vẻ đường bệ trông như thế nào nhỉ? Ngôn ngữ cứng nhắc, những bài phát biểu nghiêm trang, sự thân thiện giả tạo, những thuật ngữ sặc mùi pháp lý và nhiều thứ khác. Bạn đọc và nghe những thứ ấy rồi cảm thấy có vẻ như một con rô-bốt đã viết ra những điều đó vậy. Những công ty này đang phát ra rả vào mặt bạn, chứ đâu phải đang giao tiếp với bạn.

Tất cả chỉ là chiếc mặt nạ. Thế nhưng các công ty nhỏ vẫn cố gắng bắt chước theo. Họ cho rằng khoác vẻ đường bệ sẽ giúp gia tăng tầm cỡ và trông chuyên nghiệp hơn. Nhưng thật ra nó chỉ khiến họ trông kỳ cục mà thôi. Thêm nữa, bạn còn đánh mất tài sản quý giá nhất của một công ty nhỏ: khả năng giao tiếp đơn giản, nhanh chóng, không cần phải sàng lọc từng lời thông qua bộ phận PR hay tư vấn pháp lý.

Chẳng có gì là bất ổn khi thể hiện đúng tầm mức của mình. Thành thật về bản thân là điều khôn ngoan. Thường thì ngôn ngữ chính là ấn tượng đầu tiên của bạn - tại sao lại đi bắt đầu bằng một lời nói dối? Chớ nên e ngại khi là chính mình.

Hãy nhất quán với ngôn từ mà bạn sử dụng ở bất cứ nơi đâu, trong e-mail, bao bì hàng hóa, các buổi phỏng vấn, những bài viết trên blog, các bài thuyết trình... Hãy nói chuyện với khách hàng như cách bạn nói với bạn bè của mình. Hãy giải thích mọi việc như thể bạn đang ngồi cạnh họ. Hãy tránh sử dụng các thuật ngữ khó hiểu, hoặc bất cứ dạng ngôn ngữ khuôn sáo nào. Hãy sử dụng các từ đơn giản và dễ hiểu. Đừng nói về "tiền tệ hóa" hay "sự minh bạch"; hãy nói về việc làm ra tiền và sống chân thật. Đừng dùng đến bảy từ trong khi bốn từ đã diễn đủ ý.

Và đừng buộc các nhân viên phải kết thúc e-mail với những câu sặc mùi pháp lý như: "Thư này chỉ dành riêng cho người được chỉ định và có thể chứa các thông tin mật". Điều đó cũng như đang bảo: "Chúng tôi không tin tưởng anh và chúng tôi sẵn sàng chứng minh điều đó ở tòa án".

Khi viết thư, hãy nghĩ đến người đọc, chứ đừng chỉ viết cho xong. Bất cứ khi nào bạn viết một điều gì, hãy đọc thật to lên. Liệu có nghe như bạn đang thực sự nói chuyện hay không? Nếu không, bạn có thể làm cho nó gần gũi hơn bằng cách nào?

Ai bảo thư từ thì cần phải nghiêm trang? Ai bảo bạn phải xóa sạch cá tính của mình khi soạn thảo văn bản? Hãy quên luật lệ đi! Hãy giao tiếp!

Và khi bạn đang viết, đừng nghĩ đến tất cả những người có thể đọc nó. Chỉ nên nghĩ đến một người mà thôi. Rồi hãy viết cho người đó. Viết cho nhiều người sẽ dẫn đến sự chung chung và khó xử. Khi bạn viết riêng cho một người cụ thể, bạn sẽ diễn đạt thành công hơn.

## Những từ có bốn chữ cái

Có những từ có bốn chữ cái mà bạn không nên dùng trong kinh doanh. Chúng không phải là "fuck" hay "shit" (những từ chửi thề trong tiếng Anh), mà chúng là "need" (cần), "must" (phải), "can’t" (không thể), "easy" (dễ dàng), "just" (chỉ), "only" (duy nhất) và "fast" (nhanh). Những từ này, khi chen vào giữa các cuộc giao tiếp lành mạnh, sẽ trở thành những ngọn cờ cảnh báo, phá vỡ cuộc đàm phán và làm trì hoãn các dự án.

Khi sử dụng những từ có bốn chữ cái, bạn tạo ra một tình huống khó phân đen trắng (nhưng sự thật thì phải có sự rạch ròi giữa trắng và đen). Vì vậy mà mọi người dễ trở nên căng thẳng và tức giận. Các vấn đề cũng nảy sinh từ đó.
Đây là mặt bất ổn của những từ được xem là cấm kỵ.

- Need (cần): Thực sự chỉ có vài việc cần được hoàn tất. Thay vì nói "cần", tốt hơn là bạn nên dùng từ "có thể".

- Can’t (không thể): Khi bạn nói "không thể", rất có khả năng là bạn có thể. Đôi khi thậm chí còn có những từ "không thể" hoàn toàn mâu thuẫn nhau: "Chúng ta không thể đưa sản phẩm ra thị trường như thế này vì nó vẫn chưa hoàn chỉnh lắm" và "Chúng ta không thể dành thêm thời gian để hoàn thiện sản phẩm này vì chúng ta phải đưa nó ra thị trường".

- Easy (dễ dàng): "Dễ dàng" là từ được dùng để mô tả công việc của người khác. "Việc đó anh làm dễ thôi mà, đúng không?" Nhưng hãy để ý xem mọi người rất hiếm khi mô tả công việc của chính họ là dễ dàng. Đối với bản thân thì bạn hay nói "Để tôi nghiên cứu lại đã", nhưng đối với người khác thì bạn lại nói: "Hãy hoàn thành việc ấy đi".

Những từ trên thường xuất hiện trong những buổi tranh luận (bạn cũng nên cẩn trọng với những từ có bà con với chúng: everyone (mọi người),no one (không ai cả), always (luôn luôn) và never (không bao giờ). Một khi bạn nói ra những từ ấy thì việc tìm kiếm giải pháp trở nên khó hơn nhiều. Chúng sẽ dồn bạn vào chân tường bằng cách tách bạch hai thái cực đối kháng lẫn nhau. Đó chính là lúc xảy ra đối đầu. Bạn bóp nghẹt bất cứ thỏa hiệp nào.

Và những từ trên đặc biệt nguy hiểm khi bạn xâu chuỗi chúng lại với nhau: "Chúng ta cần phải thêm tính năng này ngay. Chúng ta không thểđưa sản phẩm ra thị trường mà thiếu đi tính năng này. Tất cả mọi người đều muốn có nó. Đó chỉ là việc nhỏ, nên làm dễ thôi. Anh phải đưa nó vào sản phẩm thật nhanh!". Chỉ có năm mươi mốt từ thôi, nhưng lại chứa hàng trăm giả định. Đây chính là "bí quyết" tạo ra thảm họa.

## Càng sớm càng tốt là liều độc dược

Đừng có dùng cụm từ "càng sớm càng tốt". Tất cả chúng ta đều biết hàm ý của nó. Có ai muốn trễ nải công việc đâu.

Khi bạn luôn chốt câu bằng cụm từ "càng sớm càng tốt", tức là bạn ám chỉ tất cả mọi thứ đều được ưu tiên hàng đầu. Và khi mà tất cả mọi thứ đều nằm ở vị trí ưu tiên hàng đầu thì thật ra chẳng có gì là được ưu tiên cả (thật là buồn cười, mọi thứ đều chiếm vị trí ưu tiên hàng đầu cho đến khi bạn thực sự phải sắp xếp thứ tự ưu tiên).

"Càng sớm càng tốt" giống như lạm phát vậy. Nó hạ thấp giá trị của tất cả những yêu cầu không chứa cụm từ này. Và rồi trước khi bạn kịp nhận ra thì cách duy nhất để yêu cầu mọi người hoàn tất công việc là bạn phải đính cụm từ "càng sớm càng tốt" lên yêu cầu ấy.

Không cần kiểu kích động như thế. Nếu một công việc chưa được hoàn thành ngay tức khắc thì cũng chẳng có ai chết. Chẳng có ai bị mất việc. Nó cũng chẳng làm tổn hao cả núi tiền của công ty. Kiểu kích động này chỉ tạo ra sự căng thẳng, gây kiệt sức và thậm chí dẫn đến hậu quả tồi tệ.

Vì vậy hãy để dành những từ khẩn cấp cho những tình huống khẩn cấp thực sự. Những tình huống mà việc trì trệ sẽ dẫn đến hậu quả trực tiếp, có thể đo lường được. Còn đối với tất cả những tình huống khác, hãy thoải mái thư giãn đi.

Chương 12: Kết thúc

## Nguồn cảm hứng có thể lụi tàn

Tất cả chúng ta đều có ý tưởng. Các ý tưởng là bất tử. Các ý tưởng luôn trường tồn.

Thứ không tồn tại mãi chính là nguồn cảm hứng. Nguồn cảm hứng giống như trái cây mới hái hoặc sữa tươi vậy: đều có ngày hết hạn.

Nếu muốn làm một việc gì đó, bạn phải thực hiện ngay. Bạn không thể đặt nó lên kệ rồi đợi hai tháng sau mới quay lại làm. Bạn không thể bảo bạn sẽ thực hiện nó sau. Nếu chần chừ, bạn sẽ mất đi hứng thú.

Nếu bạn có cảm hứng vào ngày thứ Sáu, hãy dành những ngày cuối tuần để chiêm nghiệm dự án ấy. Khi bạn đang ngập tràn cảm hứng, bạn có thể hoàn tất khối lượng công việc của hai tuần chỉ trong vòng hai mươi bốn giờ. Nguồn cảm hứng chính là cỗ máy thời gian.

Nguồn cảm hứng là một điều kỳ diệu, một cỗ máy khuếch đại năng suất làm việc, một nguồn động lực. Nhưng nguồn cảm hứng không chờ đợi bạn đâu. Nó là một thứ tức thời.

Nếu nó xuất hiện, hãy nắm chặt ngay lập tức và bắt tay vào làm.

***

### [1] 
Tiếng Anh là entrepreneur. Thuật ngữ này có gốc là một từ tiếng Pháp, entreprendre, do nhà kinh tế học người Pháp Jean-Baptiste Say đặt ra vào khoảng năm 1800, để chỉ người điều hành một doanh nghiệp. Nhà kinh tế học người Ai-len Richard Cantillon đã đưa từ này vào tiếng Anh.

### [2]
 Craigslist là trang mạng rao vặt do Craig Newmark và Jim Buckmaster sáng lập. Craigslist miễn phí gần như tất cả các mẩu quảng cáo đăng trên đó.

### [3] 
Quicken: Phần mềm quản lý tài chính kế toán.

### [4] 
Pina Colada: Một loại cocktail gồm rượu rhum pha với nước ép từ quả dứa và quả dừa.

### [5] 
Giám tuyển (còn gọi là curator) là chuyên gia tư vấn chuyên môn nghệ thuật cho việc mua và quản lý tác phẩm của các bảo tàng, đồng thời là nhà tổ chức sự kiện nghệ thuật, thiết kế tổ chức triển lãm.

### [6] 
Tuscany: Một địa danh ở miền trung nước Ý, nổi tiếng với nghề sản xuất dầu olive.

### [7] 
Judo: môn võ của Nhật. Lý thuyết chủ đạo của môn võ này là dùng nhu chế cương, dùng thế và lực bẩy để quật ngã được đối phương to khỏe hơn mà không cần tốn nhiều sức lực.

### [8] 
Foot Locker: Cửa hàng bán lẻ chuyên bán các loại giày có thương hiệu.

### [9] 
Tony Hsieh: Giám đốc điều hành đời thứ hai của Zappos.com, một công ty thương mại điện tử có trụ sở tại Nevada (Mỹ). Zappos là từ phái sinh của zapatos trong tiếng Tây Ban Nha, có nghĩa là giày.

### [10] 
Wabi-sabi trong tiếng Nhật có nghĩa là tìm kiếm vẻ đẹp và sự tinh tế trong những điều giản dị của cuộc sống.

### [11] 
Trust Fall là một trò chơi tập thể nhằm xây dựng lòng tin, trong đó một người sẽ cố ý để mình rơi tự do, hoàn toàn tin tưởng các thành viên còn lại sẽ đỡ lấy mình.
